import { RunTimeDriver } from "@mongez/cache";
import Endpoint, { setCurrentEndpoint } from "@mongez/http";
import { currentRoute, navigateTo } from "@mongez/react-router";
import { userAtom } from "apps/front-office/account/atoms";
import user from "apps/front-office/account/user";
import { cartAtom } from "apps/front-office/cart/atoms";
import {
  categoriesAtom,
  metaAtom,
  settingsAtom,
} from "apps/front-office/common/atoms";
import { breadcrumbAtom } from "apps/front-office/design-system/atoms/breadcrumb-atom";
import { currentLocaleCode } from "apps/front-office/utils/helpers";
import URLS from "apps/front-office/utils/urls";
import { AxiosResponse } from "axios";
import { apiBaseUrl, apiOS } from "./flags";

const endpoint = new Endpoint({
  baseURL: apiBaseUrl,
  cache: false,
  cacheOptions: {
    driver: new RunTimeDriver(),
    expiresAfter: 60 * 60 * 24 * 7, // 1 week, but because it is a runtime driver, it will be cleared when the page is refreshed
  },
  setAuthorizationHeader: () => {
    if (user.isLoggedIn()) {
      return `Bearer ${user.getAccessToken()}`;
    }
  },
});

const endpointEvents = endpoint.events;

endpointEvents.beforeSending(config => {
  const headers: any = config.headers;
  headers["os"] = apiOS;
  headers["locale-code"] = currentLocaleCode();

  if (settingsAtom.get("state") === "initial") {
    settingsAtom.change("state", "loading");

    headers["ws"] = true; // ws -> with settings

    if (categoriesAtom.get("state") === "initial") {
      categoriesAtom.change("state", "loading");
      headers["wc"] = true;
    }
  }
});

endpointEvents.onSuccess((response: AxiosResponse) => {
  if (response.data?.meta) {
    metaAtom.update(response.data.meta);
  }

  if (response?.data?.settings) {
    settingsAtom.update(response.data.settings);
  }

  if (response.data?.breadcrumbs) {
    breadcrumbAtom.update(response.data.breadcrumbs);
  }

  if (response.data?.menuCategories) {
    categoriesAtom.update({
      state: "loaded",
      categories: response.data.menuCategories,
    });
  }

  if (response.data?.cart !== undefined) {
    cartAtom.update(response.data.cart);
  }
});

endpointEvents.onComplete((response: AxiosResponse) => {
  if (!response) return;

  if (response.data?.data) {
    response.data = response.data.data;
  }

  if (response.data?.user) {
    userAtom.update(response.data.user);
  }
});

endpointEvents.onError(response => {
  if (!response) return;

  if (response.data?.data) {
    response.data = response.data.data;
  }

  if (response.status === 401 && currentRoute() !== URLS.auth.login) {
    user.logout();
    navigateTo(URLS.auth.login);
  }
});

setCurrentEndpoint(endpoint);

export default endpoint;
