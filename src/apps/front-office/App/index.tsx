import "@mantine/carousel/styles.css";
import { DirectionProvider, MantineProvider } from "@mantine/core";
import "@mantine/core/styles.css";
import { Notifications } from "@mantine/notifications";
import "@mantine/notifications/styles.css";
import { useEvent, useOnce } from "@mongez/react-hooks";
import { routerEvents } from "@mongez/react-router";
import { getGuestToken } from "apps/front-office/account/service/auth";
import user from "apps/front-office/account/user";
import React, { useState } from "react";
import { theme } from "../design-system";
import { cacheLTR, cacheRTL, cacheValue } from "./LayoutSettings";

export type AppProps = {
  children: React.ReactNode;
};

export function App({ children }: AppProps) {
  const [cacheProvider, setCacheProvider] = useState(cacheValue);

  // if you're using hard reload, remove the following hook
  useEvent(() =>
    routerEvents.onLocaleChanging(localeCode => {
      setCacheProvider(localeCode === "ar" ? cacheRTL : cacheLTR);
    }),
  );

  return (
    <DirectionProvider>
      <MantineProvider forceColorScheme="light" theme={theme}>
        <Notifications
          position="top-right"
          zIndex={1000}
          w="fit-content"
          limit={10}
        />
        {children}
      </MantineProvider>
    </DirectionProvider>
  );
}

/**
 * If the project requires guest token to be loaded if the user is not logged in,
 * OR, if the project requires user data to be loaded if the user is logged in,
 * then Use this component instead of TopRoot
 * Make this the default export instead of App
 */
export default function AppWithUser({ children }: any) {
  const [canPass, setCanPass] = useState(user.isLoggedIn());

  useOnce(() => {
    if (user.isLoggedIn()) return;

    getGuestToken()
      .then(response => {
        user.login(response.data.user);
        setCanPass(true);
      })
      .catch(error => error);
  });

  if (!canPass) return;

  return <App>{children}</App>;
}
