import endpoint from "shared/endpoint";

export function joinAffiliate() {
  return endpoint.patch("/join-affiliate-program");
}
