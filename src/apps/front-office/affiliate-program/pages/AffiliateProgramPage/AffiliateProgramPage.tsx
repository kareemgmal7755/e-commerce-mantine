import { Button, Flex, List, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import user from "apps/front-office/account/user";
import Container from "apps/front-office/design-system/components/Container";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import {
  toastError,
  toastSuccess,
} from "apps/front-office/design-system/utils/toast";
import parseError from "apps/front-office/utils/parse-error";
import URLS from "apps/front-office/utils/urls";
import { useEffect, useState } from "react";
import { applyAffiliate, partnerWithUsData } from "../../data/affiliate-data";
import { joinAffiliate } from "../../services/affiliate-services";

export default function AffiliateProgramPage() {
  const [disabledButton, setDisabledButton] = useState(false);

  const joinAffiliateProgram = () => {
    joinAffiliate()
      .then(() => toastSuccess(trans("successfullyJoined")))
      .catch(error => toastError(parseError(error)));
  };

  useEffect(() => {
    if (!user.get("email") || !user.get("city") || !user.get("phoneNumber")) {
      setDisabledButton(true);
    }
  }, []);

  return (
    <Container>
      <Flex direction="column" gap="1.2rem" my="lg">
        <Title order={2} ta="center" c="rhino.9">
          {trans("joinOurAffiliateProgram")}
        </Title>
        <Text span fz="1rem" fw={500}>
          {trans("passionateAboutOurProducts")}
        </Text>
        <Flex direction="column" gap="1rem">
          <Title order={4} c="rhino.7">
            - {trans("whatIsAffiliateMarketing")}
          </Title>
          <Text span fz="1rem" fw={500}>
            {trans("affiliateMarketing")}
          </Text>
        </Flex>
        <Flex direction="column" gap="1rem">
          <Title order={4} c="rhino.7">
            - {trans("whyPartnerWithUs")}
          </Title>
          <List w="100%">
            {partnerWithUsData.map((item, index) => (
              <List.Item key={index} fz="0.9rem" fw={500} w="98%">
                {trans(item.content)}
              </List.Item>
            ))}
          </List>
        </Flex>
        <Flex direction="column" gap="1rem">
          <Title order={4} c="rhino.7">
            - {trans("howCanYouJoin")}
          </Title>
          <List type="ordered">
            {applyAffiliate.map((item, index) => (
              <List.Item key={index} fz="0.9rem" fw={500}>
                {trans(item.content)}
              </List.Item>
            ))}
          </List>
        </Flex>
        {disabledButton && (
          <>
            <Flex direction="column" gap="0.7rem">
              <Flex align="baseline" gap="0.1rem">
                <Text c="red.7" fz="1.3rem" fw={800}>
                  {trans("note")}:
                </Text>
                <Text span fw={600}>
                  {trans("addToJoin")}
                </Text>
                <UnStyledLink
                  c="rhino.8"
                  to={URLS.account.editProfile}
                  td="underline"
                  fw={700}>
                  {trans("editProfile")}
                </UnStyledLink>
              </Flex>
            </Flex>
          </>
        )}
        {user.get("affiliate")?.status === "pending" && (
          <Text component="p" fw={700} c="red.6" fz="1.6rem" ta="center" m={0}>
            {trans("successfullyJoined")}
          </Text>
        )}
        {user.get("affiliate")?.status === "active" && (
          <Text component="p" fw={700} c="red.6" fz="1.6rem" ta="center" m={0}>
            {trans("alreadyJoined")}
          </Text>
        )}
        {!user.get("affiliate") && (
          <Button
            fw={700}
            fz="1.2rem"
            w="fit-content"
            size="md"
            onClick={joinAffiliateProgram}
            variant="light"
            disabled={disabledButton ? true : false}>
            {trans("join")}
          </Button>
        )}
      </Flex>
    </Container>
  );
}
