export const partnerWithUsData = [
  {
    content: "highQualityProducts",
  },
  {
    content: "competitiveCommissions",
  },
  {
    content: "easySetup",
  },
  {
    content: "trackingAndReporting",
  },
  {
    content: "marketingSupport",
  },
  {
    content: "timelyPayouts",
  },
];

export const applyAffiliate = [
  {
    content: "clickJoin",
  },
  {
    content: "affiliatePromote",
  },
  {
    content: "earnCommissions",
  },
  {
    content: "trackAndGetPaid",
  },
];
