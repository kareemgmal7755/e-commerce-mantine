import { guardedRoutes } from "../utils/router";
import URLS from "../utils/urls";
import AffiliateProgramPage from "./pages/AffiliateProgramPage";

guardedRoutes([
  {
    path: URLS.affiliate,
    component: AffiliateProgramPage,
  },
]);
