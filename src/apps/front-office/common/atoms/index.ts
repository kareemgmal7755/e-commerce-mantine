import cache from "@mongez/cache";
import { setDescription, setFavIcon, setImage, setTitle } from "@mongez/dom";
import { atom } from "@mongez/react-atom";
import { HelmetProps, setHelmetConfigurations } from "@mongez/react-helmet";
import { Attachment, Category } from "apps/front-office/store/utils/types";

export type CategoriesAtomOptions = {
  state: "initial" | "loading" | "loaded";
  categories: Category[];
};

export type SettingsState = {
  state: "initial" | "loading" | "loaded";
  general: {
    appName: string;
    description: string;
    primaryColor: string;
    about: string;
    brief: string;
    defaultCurrency: {
      symbol: string;
      code: string;
      id: number;
      value: number;
      published: boolean;
      name: string;
    };
    maintenanceMode: boolean;
    watermarkPosition: string;
    watermarkOpacity: number;
    logo: Attachment;
    watermarkImage: Attachment;
    favIcon: Attachment;
    placeholderImage: Attachment;
  };
  contact: {
    address: string;
    email: string;
    phoneNumber: string;
    whatsappNumber: string;
    workingDays: string;
    location: {
      lat: number;
      lng: number;
    };
  };
  social: {
    facebook: string;
    instagram: string;
    twitter: string;
  };
};

function setupSettings(settings: SettingsState) {
  if (settings.general?.description) {
    setDescription(settings.general.description);
  }

  if (settings.general?.appName) {
    setHelmetConfigurations({
      appendAppName: true,
      appNameSeparator: " | ",
      translatable: false,
      translateAppName: false,
      appName: settings.general.appName,
    });
  }

  if (settings.general?.appName) {
    setTitle(settings.general.appName);
  }

  if (settings.general?.favIcon) {
    setFavIcon(settings.general.favIcon.url);
  }

  if (settings.general?.logo?.url) {
    setImage(settings.general.logo.url);
  }

  return settings;
}

export const settingsAtom = atom<SettingsState>({
  key: "settings",
  beforeUpdate(settings) {
    settings.state = "loaded";
    setupSettings(settings);
    cache.set("ss", settings);
    return settings;
  },
  default: {
    ...setupSettings(
      cache.get("ss", {
        state: "initial",
        general: {} as any,
      }),
    ),
    state: "initial", // force get updated settings
  },
});

export const categoriesAtom = atom<CategoriesAtomOptions>({
  key: "categories",
  default: {
    state: "initial",
    categories: [],
  },
});

export type Meta = HelmetProps;

// Metadata is fetched from the api using the onComplete event
// @see src/shared/endpoint.ts onComplete event
export const metaAtom = atom<Meta>({
  key: "meta",
  default: {},
});

