import { trans } from "@mongez/localization";
import {
  toastError,
  toastSuccess,
} from "apps/front-office/design-system/utils/toast";
import parseError from "apps/front-office/utils/parse-error";
import { subscribeToNewsletter } from "../services/newsletter";

export function useNewsletterSubscription() {
  return ({ values, form }) => {
    subscribeToNewsletter(values)
      .then(() => {
        toastSuccess(trans("successfullySubscribed"));
        form.reset();
        form.submitting(true);
      })
      .catch(error => {
        toastError(parseError(error));
        toastError(parseError(error));
        form.submitting(false);
      });
  };
}
