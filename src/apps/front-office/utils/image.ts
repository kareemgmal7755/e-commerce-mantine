import { Attachment } from "../design-system/components/Attachments";

export default function image(
  file: Attachment,
  {
    size,
    w,
    h,
    q,
  }: {
    size?: number;
    w?: number;
    h?: number;
    q?: number;
  } = {},
) {
  if (!file) return "";

  const params = {};

  if (size) {
    params["w"] = size;
    params["h"] = size;
  }

  if (w) {
    params["w"] = w;
  }

  if (h) {
    params["h"] = h;
  }

  if (q) {
    params["q"] = q;
  }

  const url = new URL(file.url);

  url.search = new URLSearchParams(params).toString();

  return url.toString();
}
