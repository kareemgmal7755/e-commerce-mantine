import endpoint from "shared/endpoint";

export function getCities() {
  return endpoint.get("/cities");
}

export function getDistricts() {
  return endpoint.get("/districts");
}
