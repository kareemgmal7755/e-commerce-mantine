import { MantineColor } from "@mantine/core";
import { OrderStatus } from "../store/utils/types";

const statusMap: Record<OrderStatus, MantineColor> = {
  pending: "rhino.9",
  processing: "blue",
  shipped: "grape",
  delivered: "green",
  canceled: "red",
};

export function getOrderStatusColor(status: OrderStatus): MantineColor {
  return statusMap[status];
}
