// append urls here, DO NOT remove this line

const URLS = {
  home: "/",
  notFound: "/404",
  affiliate: "/affiliate",
  maintenance: "/maintenance",
  compare: "/compare",
  cart: "/cart",
  checkout: "/checkout",
  orderCompleted: "/order-completed",
  successPayment: "/success-payment/:id", // For test
  successCheckout: (order: any) => `/checkout/success/${order.id}`,
  successCheckoutRoute: "/checkout/success/:id",
  failedPayment: "/failed-payment", // For test
  blog: {
    root: "/blog",
    viewRoute: "/blog/:id/:slug",
    view: (post: any) => `/blog/${post.id}/${post.slug}`,
  },
  faq: "/faq",
  auth: {
    login: "/login",
    forgetPassword: "/forget-password",
    resetPassword: "/reset-password",
    register: "/register",
    verifyForgetPassword: "/verify-password",
    registerVerify: "/register/verify",
  },
  brands: "/brands",
  account: {
    settings: "/settings",
    dashboard: "/account",
    address: "/address-book",
    devices: "/account/my-devices",
    notifications: "/account/notifications",
    editProfile: "/account/edit-profile",
    updatePassword: "/update-password",
    orders: "/account/orders",
    viewOrderRoute: "/orders/:id",
    viewOrder: (order: any) => `/orders/${order.id}`,
    wishlist: "/account/wishlist",
  },
  shop: {
    list: "/shop",
    search: "/search",
    offers: "/offers",
    viewCategoryRoute: "/categories/:id/:slug",
    viewProductRoute: "/products/:id/:slug",
    viewProduct: (product: any) => `/products/${product?.id}/${product?.slug}`,
    viewBrandRoute: "/brands/:id/:slug",
    viewCategory: (category: any) =>
      `/categories/${category?.id}/${category?.slug}`,
    viewBrand: (brand: any) => `/brands/${brand?.id}/${brand?.slug}`,
  },
  contactUs: "/contact-us",
  wishlist: "/wishlist",
  pages: {
    aboutUs: "/about-us",
    termsConditions: "/terms-conditions",
    privacyPolicy: "/privacy-policy",
    viewRoute: "/pages/:slug/:id",
    view: (page: any) => `/pages/${page.slug}/${page.id}`,
  },
  orderDetails: "/order-details", // For test
  search: {
    list: "search",
  },
};

export default URLS;
