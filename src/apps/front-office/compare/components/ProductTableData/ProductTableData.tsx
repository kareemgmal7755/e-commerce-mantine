import { Box, Button, Flex, Table, Text, Tooltip } from "@mantine/core";
import { trans } from "@mongez/localization";
import { IconX } from "@tabler/icons-react";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import { Product } from "apps/front-office/home/utils/types";
import QuickViewButton from "apps/front-office/store/components/QuickViewButton";
import { useCompare } from "apps/front-office/store/hooks";
import { price } from "apps/front-office/store/utils/price";
import URLS from "apps/front-office/utils/urls";
import React from "react";
import style from "../style.module.scss";

export default function ProductTableData({
  products,
  setProducts,
}: {
  products: Product[];
  setProducts: React.Dispatch<React.SetStateAction<Product[]>>;
}) {
  const { removeProductFromCompare } = useCompare();

  const removeFromCompare = async (productId, index) => {
    await removeProductFromCompare(productId);
    products.splice(index, 1);
    setProducts([...products]);
  };

  return (
    <Table.Tr>
      <Table.Td tt="uppercase" fw={500} ta="end">
        {trans("products")}
      </Table.Td>
      {products.map((product, index) => (
        <Table.Td
          key={product.id}
          className={style.product_table_wrapper}
          pos="relative">
          <Tooltip
            label={trans("removeFromCompare")}
            fw={600}
            tt="uppercase"
            withArrow
            fz="0.7rem">
            <Button
              onClick={() => removeFromCompare(product.id, index)}
              bg="rhino.9"
              c="gray.0"
              size="xs"
              p={7}
              radius="50%"
              right={5}
              style={{ zIndex: 3 }}
              top={5}
              pos="absolute">
              <IconX size={14} />
            </Button>
          </Tooltip>
          <Flex
            direction="column"
            w="fit-content"
            m="auto"
            align="center"
            ta="center"
            pos="relative">
            <Box pos="absolute" className={style.quick_view_button_wrapper}>
              <QuickViewButton product={product} type="grid"/>
            </Box>
            <UnStyledLink
              w="10rem"
              h="10rem"
              to={URLS.shop.viewProduct(product)}>
              <img
                width="100%"
                height="100%"
                src={product.images[0].url}
                style={{ objectFit: "contain" }}
              />
            </UnStyledLink>
            <UnStyledLink
              to={URLS.shop.viewProduct(product)}
              c="dark.9"
              fw={600}
              fz="0.85rem">
              {product.name}
            </UnStyledLink>
            {product.discount ? (
              <Flex gap="0.3rem" align="center">
                <Text span fz="1rem" fw={600} c="gray.3" td="line-through">
                  {price(product.price)}
                </Text>
                <Text span c="rhino.9" fz="1rem" fw={600}>
                  {price(product.salePrice)}
                </Text>
              </Flex>
            ) : (
              <Text span fz="1rem" fw={600} c="rhino.9">
                {price(product.price)}
              </Text>
            )}
          </Flex>
        </Table.Td>
      ))}
    </Table.Tr>
  );
}
