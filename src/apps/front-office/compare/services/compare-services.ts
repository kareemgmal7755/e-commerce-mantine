import endpoint from "shared/endpoint";

export function getCompare() {
  return endpoint.get("/compare");
}

export function addToCompare(productId: number) {
  return endpoint.post(`/compare/${productId}`);
}


export function removeFromCompare(productId: number) {
  return endpoint.delete(`/compare/${productId}`);
}

