import { publicRoutes } from "../utils/router";
import URLS from "../utils/urls";
import ComparePage from "./pages/ComparePage";

publicRoutes([
  {
    path: URLS.compare,
    component: ComparePage,
  },
]);
