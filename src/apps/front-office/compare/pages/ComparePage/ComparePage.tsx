import { Flex, Rating, Table, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import { preload } from "@mongez/react-utils";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { compareItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import Container from "apps/front-office/design-system/components/Container";
import HTML from "apps/front-office/design-system/components/HTML";
import { Product } from "apps/front-office/home/utils/types";
import AddToCartButton from "apps/front-office/store/components/AddToCartButton";
import { useState } from "react";
import ProductTableData from "../../components/ProductTableData";
import { getCompare } from "../../services/compare-services";

function _ComparePage({ response }) {
  const [products, setProducts] = useState<Product[]>(response.data.products);

  return (
    <>
      <Helmet title={trans("compare")} />
      <Breadcrumb title="compare" items={compareItems} />
      <Container>
        <Table.ScrollContainer minWidth={800} my="4rem">
          <Table withRowBorders={true} withTableBorder withColumnBorders>
            <Table.Tbody>
              <ProductTableData products={products} setProducts={setProducts} />
              <Table.Tr>
                <Table.Td tt="uppercase" fw={500} ta="end"></Table.Td>
                {products.map(product => (
                  <Table.Td key={product.id} p="1rem" ta="center">
                    <AddToCartButton product={product} />
                  </Table.Td>
                ))}
              </Table.Tr>
              <Table.Tr>
                <Table.Td tt="uppercase" fw={500} ta="end">
                  {trans("availability")}
                </Table.Td>
                {products.map(product => (
                  <Table.Td key={product.id} ta="center">
                    <Text
                      tt="uppercase"
                      fw={600}
                      span
                      c={product.inStock ? "grassGreen.9" : "red.9"}
                      fz="0.85rem">
                      {!product.inStock
                        ? trans("outOfStock")
                        : trans("inStock")}
                    </Text>
                  </Table.Td>
                ))}
              </Table.Tr>
              <Table.Tr>
                <Table.Td tt="uppercase" fw={500} ta="end">
                  {trans("rating")}
                </Table.Td>
                {products.map(product => (
                  <Table.Td key={product.id}>
                    <Flex align="center" gap="0.5rem" justify="center">
                      <Rating value={product.rating} readOnly size="xs" />
                      <Text span c="gray.6" fz="0.9rem" fw={500}>
                        ({product.rating} {trans("reviews")})
                      </Text>
                    </Flex>
                  </Table.Td>
                ))}
              </Table.Tr>
              <Table.Tr>
                <Table.Td tt="uppercase" fw={500} ta="end">
                  {trans("sku")}
                </Table.Td>
                {products.map(product => (
                  <Table.Td key={product.id} ta="center">
                    <Text tt="uppercase" fw={500} span fz="0.85rem">
                      {product.sku}
                    </Text>
                  </Table.Td>
                ))}
              </Table.Tr>
              <Table.Tr>
                <Table.Td tt="uppercase" fw={500} ta="end">
                  {trans("brands")}
                </Table.Td>
                {products.map(product => (
                  <Table.Td key={product.id} ta="center">
                    <Text tt="uppercase" fw={500} span fz="0.85rem">
                      {product.brand.name}
                    </Text>
                  </Table.Td>
                ))}
              </Table.Tr>
              <Table.Tr>
                <Table.Td tt="uppercase" fw={500} ta="end">
                  {trans("description")}
                </Table.Td>
                {products.map(product => (
                  <Table.Td key={product.id}>
                    <Text span c="gray.6" fz="0.9rem" fw={500}>
                      <HTML html={product.shortDescription} />
                    </Text>
                  </Table.Td>
                ))}
              </Table.Tr>
            </Table.Tbody>
          </Table>
        </Table.ScrollContainer>
      </Container>
    </>
  );
}

const ComparePage = preload(_ComparePage, getCompare);
export default ComparePage;
