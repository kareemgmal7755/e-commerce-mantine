import { accountRoutes, reverseGuardedRoutes } from "../utils/router";
import URLS from "../utils/urls";
import AccountWishlistPage from "./pages/AccountWishlistPage";
import EditProfilePage from "./pages/EditProfilePage";
import ForgetPassword from "./pages/ForgetPassword";
import LoginPage from "./pages/LoginPage";
import OrderDetailsPage from "./pages/OrderDetailsPage";
import OrdersPage from "./pages/OrdersPage";
import RegisterPage from "./pages/RegisterPage";
import ResetPasswordPage from "./pages/ResetPasswordPage";
import UpdatePasswordPage from "./pages/UpdatePasswordPage";
import VerifyCodePage from "./pages/VerifyCodePage";

reverseGuardedRoutes([
  {
    path: URLS.auth.login,
    component: LoginPage,
  },
  {
    path: URLS.auth.register,
    component: RegisterPage,
  },
  {
    path: URLS.auth.forgetPassword,
    component: ForgetPassword,
  },
  {
    path: URLS.auth.verifyForgetPassword,
    component: VerifyCodePage,
  },
  {
    path: URLS.auth.resetPassword,
    component: ResetPasswordPage,
  },
]);

accountRoutes([
  {
    path: URLS.account.updatePassword,
    component: UpdatePasswordPage,
  },
  {
    path: URLS.account.editProfile,
    component: EditProfilePage,
  },
  {
    path: URLS.account.orders,
    component: OrdersPage,
  },
  {
    path: URLS.account.viewOrderRoute,
    component: OrderDetailsPage,
  },
  {
    path: URLS.account.wishlist,
    component: AccountWishlistPage,
  },
]);
