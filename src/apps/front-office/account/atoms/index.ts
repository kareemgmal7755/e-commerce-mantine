import { atom } from "@mongez/react-atom";
import { Order } from "apps/front-office/store/utils/types";
import user from "../user";

type User = {
  totalCart: number;
  totalCompare: number;
  totalWishlist: number;
  type?: string;
};

export const registerVerifyAtom = atom({
  key: "register",
  default: {
    email: "",
    code: "",
  },
});

export const resetPasswordAtom = atom({
  key: "resetPassword",
  default: {
    code: "",
    email: "",
    phoneNumber: "",
    // Used for sending the verify code to api
    // if valid, then it will be cleared
    // code must be used instead in the reset password
    tempOTP: "",
  },
});

export const userAtom = atom<User>({
  key: "user",
  default: user.all() as User,
  beforeUpdate: data => {
    user.login({
      ...data,
      type: data.type || user.get("type") || "customer",
    });

    return data;
  },
});

export const orderAtom = atom<Order>({
  key: "order",
  default: {},
});
