import { Button, Flex, Text } from "@mantine/core";
import cache from "@mongez/cache";
import { trans } from "@mongez/localization";
import { useOnce } from "@mongez/react-hooks";
import { IconBrandGoogleFilled } from "@tabler/icons-react";
import { useSocialLogin } from "../../hooks";
import { loginByGoogle } from "../../service/auth";

export default function GoogleLogin() {
  const { googleLogin } = useSocialLogin();

  useOnce(() => {
    const googleTokenAuth = cache.get("gtoken");

    if (!googleTokenAuth) return;

    loginByGoogle(googleTokenAuth);
  });

  return (
    <Button size="md" onClick={googleLogin}>
      <Flex gap="0.2rem" align="center">
        <IconBrandGoogleFilled size={18} />
        <Text span fw={600}>
          {trans("google")}
        </Text>
      </Flex>
    </Button>
  );
}
