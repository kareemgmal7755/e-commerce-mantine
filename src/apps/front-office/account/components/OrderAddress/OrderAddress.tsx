import { Table, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { orderAtom } from "../../atoms";

export default function OrderAddress() {
  const order = orderAtom.useValue();

  return (
    <>
      <Text fz="xl" fw="bold">
        {trans("shippingAddress")}
      </Text>
      <Table.ScrollContainer minWidth={600}>
        <Table striped highlightOnHover>
          <Table.Tbody>
            <Table.Tr>
              <Table.Td fw={500}>{trans("name")}</Table.Td>
              <Table.Td fw={600}>{order.shippingAddress.name}</Table.Td>
              <Table.Td fw={500}>{trans("phoneNumber")}</Table.Td>
              <Table.Td fw={600}>{order.shippingAddress.phoneNumber}</Table.Td>
            </Table.Tr>
            <Table.Tr>
              <Table.Td fw={500}>{trans("email")}</Table.Td>
              <Table.Td fw={600}>{order.shippingAddress.email}</Table.Td>
              {order.shippingAddress.district && (
                <>
                  <Table.Td fw={500}>{trans("country")}</Table.Td>
                  <Table.Td fw={600}>
                    {order.shippingAddress.district.city?.region.country.name}
                  </Table.Td>
                </>
              )}
            </Table.Tr>
            <Table.Tr>
                {order.shippingAddress.district && (
                  <>
                    <Table.Td fw={500}>{trans("city")}</Table.Td>
                    <Table.Td fw={600}>
                      {order.shippingAddress.district.city?.name}
                    </Table.Td>
                  </>
                ) }
              <Table.Td fw={500}>{trans("address")}</Table.Td>
              <Table.Td fw={600}>
                {order.shippingAddress.address}
              </Table.Td>
            </Table.Tr>
          </Table.Tbody>
        </Table>
      </Table.ScrollContainer>
      <Text fz="xl" fw="bold">
        {trans("billingAddress")}
      </Text>
      <Table.ScrollContainer minWidth={600}>
        <Table striped highlightOnHover>
          <Table.Tbody>
            <Table.Tr>
              <Table.Td fw={500}>{trans("name")}</Table.Td>
              <Table.Td fw={600}>{order.billingAddress.name}</Table.Td>
              <Table.Td fw={500}>{trans("phoneNumber")}</Table.Td>
              <Table.Td fw={600}>{order.billingAddress.phoneNumber}</Table.Td>
            </Table.Tr>
            <Table.Tr>
              <Table.Td fw={500}>{trans("email")}</Table.Td>
              <Table.Td fw={600}>{order.billingAddress.email}</Table.Td>
              {order.billingAddress.district && (
                <>
                  <Table.Td fw={500}>{trans("country")}</Table.Td>
                  <Table.Td fw={600}>
                    {order.billingAddress.district.city?.region.country.name}
                  </Table.Td>
                </>
              )}
            </Table.Tr>
            <Table.Tr>
              {order.billingAddress.district && (
                <>
                  <Table.Td fw={500}>{trans("city")}</Table.Td>
                  <Table.Td fw={600}>
                    {order.billingAddress.district.city?.name}
                  </Table.Td>
                  <Table.Td fw={500}>{trans("address")}</Table.Td>
                </>
              )}
              <Table.Td fw={600} colSpan={3}>
                {order.billingAddress.address}
              </Table.Td>
            </Table.Tr>
          </Table.Tbody>
        </Table>
      </Table.ScrollContainer>
    </>
  );
}
