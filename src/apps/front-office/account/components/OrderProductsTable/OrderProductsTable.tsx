import { Flex, Table, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { SingleCartItem } from "apps/front-office/cart/atoms";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import { price } from "apps/front-office/store/utils/price";
import URLS from "apps/front-office/utils/urls";

export default function OrderProductsTable({
  items,
}: {
  items: SingleCartItem[];
}) {
  const tableHeads = ["product", "price", "quantity", "totalPrice"];

  return (
    <Table.ScrollContainer minWidth={600}>
      <Table withTableBorder>
        <Table.Thead>
          <Table.Tr>
            {tableHeads.map((head, index) => (
              <Table.Th tt="uppercase" fz="0.8rem" ta="center" key={index}>
                {trans(head)}
              </Table.Th>
            ))}
          </Table.Tr>
        </Table.Thead>
        <Table.Tbody>
          {items.map(item => (
            <Table.Tr key={item.id}>
              <Table.Td>
                <Flex
                  gap="0.8rem"
                  align="center"
                  direction="column"
                  justify="center">
                  <UnStyledLink to={URLS.shop.viewProduct(item.product)}>
                    <img
                      src={item.product.images[0].url}
                      alt={item.product.name}
                      width={100}
                      height={100}
                      style={{ objectFit: "contain" }}
                    />
                  </UnStyledLink>
                  <UnStyledLink
                    to={URLS.shop.viewProduct(item.product)}
                    fz="1rem"
                    fw={600}
                    c="dark.9">
                    {item.product.name}
                  </UnStyledLink>
                  <Flex gap="0.1rem">
                    <Text span fz="0.8rem">
                      {trans("brand")}:
                    </Text>
                    <Text span fz="0.8rem" fw={500}>
                      {item.product.brand.name}
                    </Text>
                  </Flex>
                </Flex>
              </Table.Td>
              <Table.Td>
                {item.product.discount ? (
                  <Flex gap="0.3rem" align="center" justify="center">
                    <Text span c="dark.9" fz="1rem" fw={600}>
                      {price(item.product.salePrice)}
                    </Text>
                    <Text
                      span
                      fz="0.8rem"
                      fw={600}
                      c="gray.3"
                      td="line-through">
                      {price(item.product.price)}
                    </Text>
                  </Flex>
                ) : (
                  <Flex align="center" justify="center">
                    <Text span fz="1rem" fw={600} c="dark.9">
                      {price(item.product.price)}
                    </Text>
                  </Flex>
                )}
              </Table.Td>
              <Table.Td ta="center">
                <Text span c="dark.9" fz="1rem" fw={600}>
                  {item.quantity}
                </Text>
              </Table.Td>
              <Table.Td ta="center">
                <Text span c="dark.9" fz="1rem" fw={600}>
                  {price(item.total.finalPrice)}
                </Text>
              </Table.Td>
            </Table.Tr>
          ))}
        </Table.Tbody>
      </Table>
    </Table.ScrollContainer>
  );
}
