import { LoadingOverlay } from "@mantine/core";
import { NotFound } from "@mongez/react-components";
import { useOnce } from "@mongez/react-hooks";
import { useState } from "react";
import { registerVerifyAtom } from "../../atoms";
import VerifyRegisterCode from "./VerifyRegisterCode";

export default function RegisterVerify() {
  const [content, setContent] = useState(<LoadingOverlay visible />);
  const email = registerVerifyAtom.get("email");
  useOnce(() => {
    if (!email) return <NotFound />;
    setContent(<VerifyRegisterCode />);
  });

  return <>{content}</>;
}
