import { Flex, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { Form } from "@mongez/react-form";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import { TertiaryButton } from "apps/front-office/design-system/components/Buttons/TertiaryButton";
import TextInput from "apps/front-office/design-system/components/Form/TextInput";
import usePrimaryColor from "design-system/hooks/use-primary-color";
import { useState } from "react";
import { useSendRegisterCodeAgain, useVerifyRegisterCode } from "../../hooks";

export default function VerifyRegisterCode() {
  const VerifyRegisterCode = useVerifyRegisterCode();
  const color = usePrimaryColor();
  const sendCodeAgain = useSendRegisterCodeAgain();
  const [clickSend, setClickSend] = useState(false);
  const [count, setCount] = useState(60);

  const handelResendCode = () => {
    sendCodeAgain();
    setClickSend(true);
    const id = setInterval(() => {
      setCount(prev => prev - 1);
    }, 1000);

    // clear interval after 60 seconds
    setTimeout(() => {
      clearInterval(id);
      setClickSend(false);
      setCount(10);
    }, 60000);
  };
  return (
    <Flex direction="column" gap="1rem">
      <Title order={2} ta="center">
        {trans("verifyYourAccount")}
      </Title>
      <Text c="gray.5" ta="center">
        {trans("weHaveSendCodeToYourEmail")}
      </Text>
      <Form onSubmit={VerifyRegisterCode}>
        <Flex align="center" justify="center">
          <TextInput
            name="code"
            placeholder={trans("verifyCode")}
            required
            autoFocus
          />
        </Flex>
        <Flex align="center" gap="0.75rem">
          <Text c="gray.5" fz=".8rem">
            {trans("didYouReceiveCode")}?
          </Text>
          <TertiaryButton
            onClick={handelResendCode}
            size=".6rem"
            p=".4rem"
            disabled={clickSend}
            leftIcon={clickSend ? count : ""}>
            {trans("resend")}
          </TertiaryButton>
        </Flex>
        <SubmitButton size="md" color={color}>
          {trans("verify")}
        </SubmitButton>
      </Form>
    </Flex>
  );
}
