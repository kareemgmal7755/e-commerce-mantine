import { Badge, Box, Divider, Flex, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { getOrderStatusColor } from "apps/front-office/utils/order-status-color";
import { orderAtom } from "../../atoms";

export default function OderInvoice() {
  const order = orderAtom.useValue();
  const totalsText = [...(orderAtom.use("totalsText") || [])];
  const finalPrice = totalsText.pop();

  return (
    <Flex p="1rem" bg="romance.0" direction="column" gap="0.7rem" w="100%">
      <Flex justify="space-between" align="center">
        <Title order={5}>
          {trans("orderInvoice")}
        </Title>
        <Badge color={getOrderStatusColor(order.status.name)} size="sm">
          {order.status.label}
        </Badge>
      </Flex>
      <Divider />
      {totalsText.map(total => (
        <Box key={total.type}>
          <Flex justify="space-between" align="center" key={total.label}>
            <Text tt="uppercase" component="p" fz="0.8rem" fw={500}>
              {total.label}
            </Text>
            <Text fw={600} component="p" fz="0.8rem">
              {total.valueText}
            </Text>
          </Flex>
        </Box>
      ))}
      <Flex justify="space-between" align="center">
        <Text tt="uppercase" component="p" fz="1rem" fw={600}>
          {trans("total")}
        </Text>
        {finalPrice && (
          <Text tt="uppercase" component="p" fz="1rem" fw={700}>
            {finalPrice.valueText}
          </Text>
        )}
      </Flex>
    </Flex>
  );
}
