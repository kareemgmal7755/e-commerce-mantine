import { Text, Timeline, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import {
  IconHomeHeart,
  IconProgress,
  IconShoppingCartBolt,
  IconTruckDelivery,
} from "@tabler/icons-react";
import { Order } from "apps/front-office/store/utils/types";
import { orderAtom } from "../../atoms";

export enum OrderState {
  pending = 0,
  processing = 1,
  shipped = 2,
  delivered = 3,
}

export default function OrderStatus() {
  const order: Order = orderAtom.useValue();
  const orderActive =
    order.status.name === "pending"
      ? 0
      : order.status.name === "processing"
        ? 1
        : order.status.name === "shipped"
          ? 2
          : 3;

  return (
    <Timeline
      active={orderActive}
      bulletSize={35}
      lineWidth={2}
      color="rhino.9">
      <Timeline.Item
        bullet={<IconProgress size={22} color="rhino.9" stroke={1.5} />}
        title={<Title order={5}>{trans("pending")}</Title>}>
        <Text c="rhino.9" size="xs" fw={600}>
          {trans("orderPlaced")}
        </Text>
      </Timeline.Item>
      <Timeline.Item
        bullet={<IconShoppingCartBolt size={22} color="rhino.9" stroke={1.5} />}
        title={<Title order={5}>{trans("processing")}</Title>}>
        <Text c="rhino.9" size="xs" fw={600}>
          {trans("orderProcessed")}
        </Text>
      </Timeline.Item>
      <Timeline.Item
        bullet={<IconTruckDelivery size={22} color="rhino.9" stroke={1.5} />}
        title={<Title order={5}>{trans("shipped")}</Title>}>
        <Text c="rhino.9" size="xs" fw={600}>
          {trans("orderShipped")}
        </Text>
      </Timeline.Item>
      <Timeline.Item
        bullet={<IconHomeHeart size={22} color="rhino.9" stroke={1.5} />}
        title={<Title order={5}>{trans("delivered")}</Title>}>
        <Text c="rhino.9" size="xs" fw={600}>
          {trans("orderDelivered")}
        </Text>
      </Timeline.Item>
    </Timeline>
  );
}
