import { Button, Divider, Flex, Modal, Text, Title } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { trans } from "@mongez/localization";
import { Form, FormSubmitOptions } from "@mongez/react-form";
import { IconBrandFacebook, IconBrandGoogleFilled } from "@tabler/icons-react";
import { useMedia } from "apps/front-office/design-system/components";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import Container from "apps/front-office/design-system/components/Container";
import EmailInput from "apps/front-office/design-system/components/Form/EmailInput";
import PasswordInput from "apps/front-office/design-system/components/Form/PasswordInput";
import TextInput from "apps/front-office/design-system/components/Form/TextInput";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import { useCreateAccount } from "../../hooks";
import RegisterVerify from "../RegisterVerify";
import style from "../style.module.scss";

export default function RegisterForm() {
  const [opened, { open, close }] = useDisclosure(false);
  const media = useMedia(1000);
  const submitForm: any = useCreateAccount();

  const createAccount = async (options: FormSubmitOptions) => {
    await submitForm(options);
    open();
  };

  return (
    <>
      <Flex justify="center" align="center" my="4rem">
        <Container>
          <Flex
            justify="center"
            m="auto"
            align="center"
            gap="1rem"
            direction="column"
            w={media ? "100%" : "50%"}>
            <Title order={3}>{trans("registerHere")}</Title>
            <Form className={style.form_wrapper} onSubmit={createAccount}>
              <TextInput
                name="firstName"
                label={trans("firstName")}
                placeholder={trans("firstName")}
                autoFocus
                size="md"
                required
              />
              <TextInput
                name="lastName"
                placeholder={trans("lastName")}
                label={trans("lastName")}
                size="md"
                required
              />
              <TextInput
                size="md"
                type="phoneNumber"
                label={trans("phoneNumber")}
                name="phoneNumber"
                required
                placeholder={trans("phoneNumber")}
              />
              <EmailInput
                size="md"
                label={trans("email")}
                name="email"
                required
                placeholder={trans("email")}
              />
              <PasswordInput
                size="md"
                name="password"
                required
                id="password"
                minLength={8}
                label={trans("password")}
                placeholder={trans("password")}
              />
              <PasswordInput
                size="md"
                name="confirmPassword"
                required
                match="password"
                placeholder={trans("confirmPassword")}
                label={trans("confirmPassword")}
              />
              <Flex w="100%" justify="space-between"></Flex>
              <SubmitButton w="100%" size="md">
                {trans("createAccount")}
              </SubmitButton>
            </Form>
            <Flex gap="0.5rem" align="center">
              <Text span>{trans("alreadyHaveAccount")}</Text>
              <UnStyledLink to={URLS.auth.login} c="dark.9" fw={700}>
                {trans("login")}
              </UnStyledLink>
            </Flex>
            <Divider
              label={trans("or")}
              labelPosition="center"
              color="gray.5"
              tt="uppercase"
              fw={600}
              w="100%"
              styles={{
                label: {
                  fontSize: "1.1rem",
                },
              }}
            />
            <Text span fz="1.2rem" c="gray.6">
              {trans("signWithSocial")}
            </Text>
            <Flex gap="1rem" align="center">
              <Button size="md">
                <Flex gap="0.2rem" align="center">
                  <IconBrandGoogleFilled size={18} />
                  <Text span fw={600}>
                    {trans("google")}
                  </Text>
                </Flex>
              </Button>
              <Button size="md" bg="#4867AA">
                <Flex gap="0.2rem" align="center">
                  <IconBrandFacebook size={18} />
                  <Text span fw={600}>
                    {trans("facebook")}
                  </Text>
                </Flex>
              </Button>
            </Flex>
          </Flex>
        </Container>
      </Flex>
      <Modal
        opened={opened}
        onClose={close}
   
        centered
        size="lg">
        <RegisterVerify />
      </Modal>
    </>
  );
}
