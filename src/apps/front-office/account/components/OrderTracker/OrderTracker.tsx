import { Stepper } from "@mantine/core";
import { useMediaQuery } from "@mantine/hooks";
import { trans } from "@mongez/localization";
import {
  IconHome,
  IconHomeHeart,
  IconProgress,
  IconShoppingCart,
  IconShoppingCartBolt,
  IconTruck,
  IconTruckDelivery,
} from "@tabler/icons-react";
import { orderAtom } from "../../atoms";
import { IconHomeCog } from "@tabler/icons-react";

export default function OrderTracker() {
  const media = useMediaQuery("(max-width:1000px)");
  const order: any = orderAtom.useValue();
  const orderActive =
    order.status.name === "pending"
      ? 1
      : order.status.name === "processing"
        ? 2
        : order.status.name === "shipped"
          ? 3
          : 4;

  return (
    <Stepper
      color="rhino.9"
      active={orderActive}
      size="md"
      orientation={media ? "vertical" : "horizontal"}
      styles={{
        step: {
          alignItems: "center",
        },
      }}>
      <Stepper.Step
        label={trans("pending")}
        icon={<IconProgress size={25} color="#3a5173" stroke={1.5} />}
      />
      <Stepper.Step
        label={trans("processing")}
        icon={<IconShoppingCartBolt size={25} color="#3a5173" stroke={1.5} />}
      />
      <Stepper.Step
        label={trans("shipped")}
        icon={<IconTruckDelivery size={25} color="#3a5173" stroke={1.5} />}
      />
      <Stepper.Step
        label={trans("delivered")}
        icon={<IconHomeHeart size={25} color="#3a5173" stroke={1.5} />}
      />
    </Stepper>
  );
}
