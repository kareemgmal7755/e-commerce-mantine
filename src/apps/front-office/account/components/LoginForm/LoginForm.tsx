import { Button, Divider, Flex, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { Form } from "@mongez/react-form";
import { IconBrandFacebook } from "@tabler/icons-react";
import { useMedia } from "apps/front-office/design-system/components";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import Container from "apps/front-office/design-system/components/Container";
import EmailInput from "apps/front-office/design-system/components/Form/EmailInput";
import PasswordInput from "apps/front-office/design-system/components/Form/PasswordInput";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import { useLogin } from "../../hooks";
import GoogleLogin from "../GoogleLogin";
import style from "../style.module.scss";

export default function LoginForm() {
  const media = useMedia(1000);
  const loginSubmit: any = useLogin();

  return (
    <Flex justify="center" align="center" my="4rem">
      <Container>
        <Flex
          justify="center"
          m="auto"
          align="center"
          gap="1rem"
          direction="column"
          w={media ? "100%" : "50%"}>
          <Title order={3}>{trans("login")}</Title>
          <Text span>{trans("haveAnAccount")}</Text>
          <Form className={style.form_wrapper} onSubmit={loginSubmit}>
            <EmailInput
              size="md"
              placeholder={trans("email")}
              required
              name="email"
              autoFocus
            />
            <PasswordInput
              name="password"
              placeholder={trans("password")}
              size="md"
              required
            />
            <SubmitButton w="100%">{trans("login")}</SubmitButton>
          </Form>
          <Flex justify="space-between" w="100%" fw={600}>
            <UnStyledLink to={URLS.auth.forgetPassword} c="dark.9">
              {trans("forgetYourPassword")}
            </UnStyledLink>
            <Flex gap="0.5rem" align="center">
              <Text span>{trans("haveAccount")}</Text>
              <UnStyledLink to={URLS.auth.register} c="dark.9" fw={600}>
                {trans("createAccount")}
              </UnStyledLink>
            </Flex>
          </Flex>
          <Divider
            label={trans("or")}
            labelPosition="center"
            color="gray.5"
            tt="uppercase"
            fw={600}
            w="100%"
            styles={{
              label: {
                fontSize: "1.1rem",
              },
            }}
          />
          <Text span fz="1.2rem" c="gray.6">
            {trans("signWithSocial")}
          </Text>
          <Flex gap="1rem" align="center">
            <GoogleLogin />
            <Button size="md" bg="#4867AA">
              <Flex gap="0.2rem" align="center">
                <IconBrandFacebook size={18} />
                <Text span fw={600}>
                  {trans("facebook")}
                </Text>
              </Flex>
            </Button>
          </Flex>
        </Flex>
      </Container>
    </Flex>
  );
}
