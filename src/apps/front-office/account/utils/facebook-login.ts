import { loadScript } from "@mongez/dom";

export type CredentialResponse = {
  credential: string;
  select_by: string;
};

export type onSuccessCallback = (credentials: CredentialResponse) => void;

export type FacebookLoginSuccessResponse = {
  authResponse: {
    accessToken: string;
    expiresIn: string;
    signedRequest: string;
    userID: string;
  };
  status?: string;
};

export type FacebookSuccessLoginCallback = (
  response: FacebookLoginSuccessResponse,
) => void;

loadScript("https://connect.facebook.net/en_US/sdk.js", () => {
  //
});

export function initializeFacebookLogin({ appId, version }) {
  let fbRoot = document.getElementById("fb-root");
  if (!fbRoot) {
    fbRoot = document.createElement("div");
    fbRoot.id = "fb-root";
    document.body.appendChild(fbRoot);
  }

  const FB = window["FB"];

  FB.init({
    appId,
    version,
  });

  // FB.AppEvents.logPageView();
}

export function loginByFacebook(onSuccess: FacebookSuccessLoginCallback) {
  const FB = window["FB"];
  FB.login(onSuccess);
}
