import endpoint from "shared/endpoint";

export function getOrders(params: any = {}) {
  return endpoint.get("/orders", {
    params,
  });
}

export function getOrder(id: any) {
  return endpoint.get(`/orders/${id}`);
}
