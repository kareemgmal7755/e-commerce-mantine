import { TokenResponse } from "@react-oauth/google";
import endpoint from "shared/endpoint";

export function loginByGoogle(data: TokenResponse) {
  return endpoint.post("/login/google", {
    token: data.access_token,
    scope: data.scope,
  });
}

export function loginByFacebook(token: string) {
  return endpoint.post("/login/facebook", token);
}
