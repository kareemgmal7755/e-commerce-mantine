import { trans } from "@mongez/localization";
import router, {
  navigateBack,
  navigateTo,
  refresh,
} from "@mongez/react-router";
import parseError from "apps/front-office/utils/parse-error";
import URLS from "apps/front-office/utils/urls";
import "react-simple-toasts/dist/theme/dark.css";

import cache from "@mongez/cache";
import { useGoogleLogin } from "@react-oauth/google";
import {
  toastError,
  toastSuccess,
} from "apps/front-office/design-system/utils/toast";
import { registerVerifyAtom, resetPasswordAtom } from "../atoms";
import {
  changePassword,
  editProfile,
  forgetPassword,
  login,
  loginByGoogle,
  register,
  resendRegisterCode,
  resetPassword,
  verifyCode,
  verifyForgetPassword,
} from "../service/auth";
import user from "../user";
import {
  FacebookSuccessLoginCallback,
  initializeFacebookLogin,
  loginByFacebook,
} from "../utils/facebook-login";

const goBack = () => {
  setTimeout(() => {
    if (!Object.values(URLS.auth).includes(router.getPreviousRoute())) {
      navigateBack();
    } else {
      navigateTo(URLS.home);
    }
  }, 0);
};

/**
 * Login hook
 * It return the onSubmit callback
 */
export function useLogin() {
  const loginSubmit = ({ values, form }) => {
    login(values)
      .then(response => {
        const userData = response.data.user;
        toastSuccess(trans("successfullyLoggedIn"));
        goBack();
        user.login(userData);
      })
      .catch(error => {
        toastError(parseError(error));
        form.submitting(false);
      });
  };
  return loginSubmit;
}

/**
 * Create account/Register hook
 * It return the onSubmit callback
 */
export function useCreateAccount() {
  return ({ values, form }) => {
    return new Promise((resolve, reject) => {
      register(values)
        .then(() => {
          registerVerifyAtom.update({
            ...registerVerifyAtom.value,
            email: form.value("email"),
          });
          toastSuccess(trans("sentLink"));
          resolve(true);
        })
        .catch(error => {
          toastError(parseError(error));
          form.submitting(false);
          reject(error);
        });
    });
  };
}

/**
 * Verify register code hook
 * Use this hook to verify user account after registration
 */
export function useVerifyRegisterCode() {
  const verifyRegisterCode = ({ form }) => {
    verifyCode({
      email: registerVerifyAtom.get("email"),
      code: form.value("code"),
    })
      .then(() => {
        toastSuccess(trans("createdAccountSuccessfully"));
        registerVerifyAtom.reset();
        navigateTo(URLS.auth.login);
      })
      .catch(error => {
        toastError(parseError(error));
        form.submitting(false);
      });
  };

  return verifyRegisterCode;
}

/**
 * Perform logout
 */
export function useLogout(hardReload = true) {
  return () => {
    user.logout();
    toastSuccess(trans("loggedOutSuccessfully"));
    setTimeout(() => {
      if (hardReload) {
        window.location.reload();
      } else {
        refresh();
      }
    }, 0);
  };
}

export function useSendRegisterCodeAgain() {
  return () => {
    resendRegisterCode({
      email: registerVerifyAtom.get("email"),
    })
      .then(() => {
        toastSuccess(trans("sentCode"));
      })
      .catch(error => {
        toastError(parseError(error));
      });
  };
}

/**
 * Send forget password request hook
 */
export function useForgetPassword() {
  const forgetPasswordSubmit = ({ values, form }) => {
    forgetPassword(values)
      .then(() => {
        resetPasswordAtom.update({
          ...resetPasswordAtom.value,
          email: form.value("email"),
        });
        navigateTo(URLS.auth.verifyForgetPassword);
      })
      .catch(error => {
        toastError(parseError(error));
      });
  };

  return forgetPasswordSubmit;
}

/**
 * Verify forget password code hook
 */
export function useVerifyForgetPasswordOTP() {
  const verifyForgetPasswordOTPSubmit = ({ form }) => {
    verifyForgetPassword({
      email: resetPasswordAtom.get("email"),
      code: form.value("code"),
    })
      .then(() => {
        resetPasswordAtom.update({
          ...resetPasswordAtom.value,
          code: form.value("code"),
        });
        navigateTo(URLS.auth.resetPassword);
      })
      .catch(error => {
        toastError(parseError(error));
        form.submitting(false);
      });
  };

  return verifyForgetPasswordOTPSubmit;
}

/**
 * Reset password hook
 */
export function useResetPassword() {
  const resetPasswordSubmit = ({ form }) => {
    resetPassword({
      email: resetPasswordAtom.get("email"),
      code: resetPasswordAtom.get("code"),
      ...form.values(["password", "confirmPassword"]),
    })
      .then(() => {
        toastSuccess(trans("resetPasswordSuccess"));
        resetPasswordAtom.reset();
      })
      .catch(error => {
        toastError(parseError(error));
        form.submitting(false);
      });
  };

  return resetPasswordSubmit;
}

/**
 * Edit profile hook
 */
export function useEditProfile(city?: string, gender?: string) {
  const editAccount = ({ values, form }) => {
    editProfile({ ...values, city, gender })
      .then(() => {
        toastSuccess(trans("modifiedSuccessfully"));
      })
      .catch(error => {
        toastError(parseError(error));
      })
      .finally(() => {
        form.submitting(false);
      });
  };
  return editAccount;
}

/**
 * Update password hook
 */
export function useUpdatePassword() {
  const updatePassword = ({ form, values }) => {
    changePassword(values)
      .then(() => {
        toastSuccess(trans("modifiedSuccessfully"));
      })
      .catch(error => {
        toastError(parseError(error));
      })
      .finally(() => {
        form.submitting(false);
      });
  };
  return updatePassword;
}

export default function useFacebookLogin({
  appId,
  version,
  onSuccess,
}: {
  appId: string;
  version: string;
  onSuccess: FacebookSuccessLoginCallback;
}) {
  initializeFacebookLogin({ appId, version });
  return () => {
    loginByFacebook(onSuccess);
  };
}

function useGLogin() {
  return useGoogleLogin({
    onSuccess: tokenResponse => {
      cache.set("gtoken", tokenResponse);

      loginByGoogle(tokenResponse)
        .then(() => {
          navigateBack();
        })
        .catch(error => {
          toastError(parseError(error));
        });
    },
  });
}

export function useSocialLogin() {
  const facebookLogin = useFacebookLogin;
  return {
    facebookLogin,
    googleLogin: useGLogin,
  };
}
