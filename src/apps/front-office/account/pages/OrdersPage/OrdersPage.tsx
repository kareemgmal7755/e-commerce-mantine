import { Badge, Box, Flex, Table, Title, Tooltip } from "@mantine/core";
import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import { queryString } from "@mongez/react-router";
import { preload } from "@mongez/react-utils";
import { IconEye } from "@tabler/icons-react";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import PaginationInfo from "apps/front-office/store/components/PaginationInfo";
import { price } from "apps/front-office/store/utils/price";
import { Order } from "apps/front-office/store/utils/types";
import { getOrderStatusColor } from "apps/front-office/utils/order-status-color";
import URLS from "apps/front-office/utils/urls";
import style from "../../components/style.module.scss";
import { getOrders } from "../../service/orders";

function _OrdersPage({ response }) {
  const orders = response.data.orders as Order[];
  const paginationInfo = response.data.paginationInfo;
  const tableHeaders = [
    "orderId",
    "date",
    "status",
    "totalItems",
    "totalPrice",
  ];

  return (
    <>
      <Helmet title={trans("orders")} />
      <Title order={3} c="dark.9">
        {trans("orders")}
      </Title>
      <Flex direction="column" gap="1.5rem">
        <Table.ScrollContainer minWidth={950}>
          <Table
            mt="1rem"
            horizontalSpacing="xl"
            withTableBorder
            withRowBorders>
            <Table.Thead>
              <Table.Tr tt="uppercase">
                {tableHeaders.map((header, index) => (
                  <Table.Th fw={500} ta="center" key={index} fz="0.9rem">
                    {trans(header)}
                  </Table.Th>
                ))}
              </Table.Tr>
            </Table.Thead>
            <Table.Tbody>
              {orders.map(order => (
                <Table.Tr key={order.id} fz="0.85rem">
                  <Table.Td ta="center" fw={500}>
                    #{order.id}
                  </Table.Td>
                  <Table.Td ta="center" fw={500}>
                    {order.createdAt}
                  </Table.Td>
                  <Table.Td ta="center" fw={500}>
                    <Badge color={getOrderStatusColor(order.status.name)}>
                      {order.status.label}
                    </Badge>
                  </Table.Td>
                  <Table.Td ta="center" fw={500}>
                    {order.totals.quantity}
                  </Table.Td>
                  <Table.Td ta="center" fw={500}>
                    {price(order.finalPrice)}
                  </Table.Td>
                  <Table.Td ta="center">
                    <Flex justify="center" w="100%">
                      <Tooltip
                        label={trans("viewOrder")}
                        position="top"
                        fz="0.7rem"
                        fw={600}
                        withArrow
                        offset={-3}>
                        <Box>
                          <UnStyledLink
                            className={style.view_order_button}
                            to={URLS.account.viewOrder(order)}>
                            <IconEye />
                          </UnStyledLink>
                        </Box>
                      </Tooltip>
                    </Flex>
                  </Table.Td>
                </Table.Tr>
              ))}
            </Table.Tbody>
          </Table>
        </Table.ScrollContainer>
        {/* {orders.map(order => (
          <Table.ScrollContainer minWidth={900} key={order.id}>
            <Table
              mt="1rem"
              horizontalSpacing="xl"
              withTableBorder
              withRowBorders>
              <Table.Thead>
                <Table.Tr tt="uppercase">
                  {tableHeaders.map((header, index) => (
                    <Table.Th fw={500} ta="center" key={index}>
                      {trans(header)}
                    </Table.Th>
                  ))}
                </Table.Tr>
              </Table.Thead>
              <Table.Tbody>
                {order.items.map(item => (
                  <Table.Tr key={item.product.id}>
                    <Table.Td ta="center">
                      <img
                        src={item.product.images[0].url}
                        alt={item.product.name}
                        height={80}
                        style={{ objectFit: "contain" }}
                        width={80}
                      />
                    </Table.Td>
                    <Table.Td ta="center" fw={500}>
                      #{order.id}
                    </Table.Td>
                    <Table.Td ta="center" fw={500}>
                      {item.product.name}
                    </Table.Td>
                    <Table.Td ta="center" fw={500}>
                      {price(item.product.price)}
                    </Table.Td>
                    <Table.Td ta="center">
                      <Badge
                        color={
                          order.status.name === "shipped"
                            ? "green.7"
                            : order.status.name === "delivered"
                              ? "indigo.7"
                              : order.status.name === "canceled"
                                ? "gray.7"
                                : order.status.name === "pending"
                                  ? "red.7"
                                  : "black.7"
                        }>
                        {order.status.label}
                      </Badge>
                    </Table.Td>
                  </Table.Tr>
                ))}

                <Table.Tr>
                  <Table.Td></Table.Td>
                  <Table.Td></Table.Td>
                  <Table.Td>
                    <Flex justify="center" w="100%">
                      <Tooltip
                        label={trans("viewOrder")}
                        position="top"
                        fz="0.7rem"
                        fw={600}
                        withArrow
                        offset={-3}>
                        <Box>
                          <UnStyledLink
                            className={style.view_order_button}
                            top={URLS.account.viewOrder(order)}>
                            <IconEye />
                          </UnStyledLink>
                        </Box>
                      </Tooltip>
                    </Flex>
                  </Table.Td>
                  <Table.Td></Table.Td>
                  <Table.Td></Table.Td>
                  <Table.Td></Table.Td>
                </Table.Tr>
              </Table.Tbody>
              <Table.Caption>
                <Tooltip
                  label={trans("viewOrder")}
                  position="top"
                  fz="0.7rem"
                  fw={600}
                  withArrow
                  offset={-3}>
                  <Flex justify="center" align="center">
                    <UnStyledLink
                      className={style.view_order_button}
                      top={URLS.account.viewOrder(order)}>
                      <IconEye />
                    </UnStyledLink>
                  </Flex>
                </Tooltip>
              </Table.Caption>
            </Table>
          </Table.ScrollContainer>
        ))} */}
      </Flex>
      <PaginationInfo paginationInfo={paginationInfo} />
    </>
  );
}

const OrdersPage = preload(_OrdersPage, () =>
  getOrders({
    page: queryString.get("page", 1),
  }),
);
export default OrdersPage;
