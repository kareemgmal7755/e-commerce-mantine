import { Flex, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import { Form } from "@mongez/react-form";
import Helmet from "@mongez/react-helmet";
import { IconChevronLeft, IconChevronRight } from "@tabler/icons-react";
import { useMedia } from "apps/front-office/design-system/components";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { verificationCodeItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import Container from "apps/front-office/design-system/components/Container";
import TextInput from "apps/front-office/design-system/components/Form/TextInput";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import style from "../../components/style.module.scss";
import { useVerifyForgetPasswordOTP } from "../../hooks";

export default function VerifyCodePage() {
  const verifyForgetPasswordOTPSubmit = useVerifyForgetPasswordOTP();
  const media = useMedia(1000);

  return (
    <>
      <Helmet title={trans("verificationCode")} />
      <Breadcrumb
        items={verificationCodeItems}
        title={trans("verificationCode")}
      />
      <Flex justify="center" align="center" my="4rem">
        <Container>
          <Flex
            justify="center"
            m="auto"
            align="center"
            gap="1rem"
            direction="column">
            <Title order={3}>{trans("verificationCode")} </Title>
            <Text span>{trans("enterCode")}</Text>
            <Form
              className={style.form_wrapper}
              onSubmit={verifyForgetPasswordOTPSubmit}>
              <Flex w={media ? "100%" : "50%"} direction="column" gap="1rem">
                <TextInput
                  size="md"
                  placeholder={trans("verificationCode")}
                  label={trans("verificationCode")}
                  required
                  name="code"
                  autoFocus
                />
                <Flex justify="space-between" align="center">
                  <SubmitButton size="lg" fz="1rem">
                    {trans("nextStep")}
                  </SubmitButton>
                  <Flex gap="0.3rem" align="center">
                    {current("localeCode") === "ar" ? (
                      <IconChevronRight size={18} />
                    ) : (
                      <IconChevronLeft size={18} />
                    )}
                    <UnStyledLink
                      to={URLS.auth.login}
                      c="gray.9"
                      fz="0.9rem"
                      fw={500}>
                      {trans("backToLogin")}
                    </UnStyledLink>
                  </Flex>
                </Flex>
              </Flex>
            </Form>
          </Flex>
        </Container>
      </Flex>
    </>
  );
}
