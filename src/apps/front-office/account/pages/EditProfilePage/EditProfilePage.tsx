import { Box, SimpleGrid, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { Form } from "@mongez/react-form";
import Helmet from "@mongez/react-helmet";
import { cityAtom } from "apps/front-office/design-system/atoms/select-atom";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import EmailInput from "apps/front-office/design-system/components/Form/EmailInput";
import TextInput from "apps/front-office/design-system/components/Form/TextInput";
import CitiesSelectInput from "../../../design-system/components/CitiesSelectInput";
import { useEditProfile } from "../../hooks";
import user from "../../user";

export default function EditProfilePage() {
  const city = cityAtom.useValue();

  const gender = cityAtom.useValue();

  const editProfileSubmit = useEditProfile(city, gender);

  return (
    <>
      <Helmet title={trans("editProfile")} />
      <Box>
        <Title order={3} c="dark.9">
          {trans("editProfile")}
        </Title>
        <Form onSubmit={editProfileSubmit}>
          <SimpleGrid cols={{ base: 1, md: 2 }} mt="1.5rem">
            <TextInput
              label={trans("firstName")}
              name="firstName"
              placeholder={trans("firstName")}
              required
              defaultValue={user.get("firstName")}
            />
            <TextInput
              label={trans("lastName")}
              name="lastName"
              placeholder={trans("lastName")}
              defaultValue={user.get("lastName")}
              required
            />
            <EmailInput
              label={trans("email")}
              name="email"
              placeholder={trans("email")}
              required
              defaultValue={user.get("email")}
            />
            <TextInput
              type="phoneNumber"
              label={trans("phoneNumber")}
              name="phoneNumber"
              required
              placeholder={trans("phoneNumber")}
              defaultValue={user.get("phoneNumber")}
            />
            <CitiesSelectInput />
            {/* <GenderSelectInput /> */}
          </SimpleGrid>
          <SubmitButton mt="1.5rem">{trans("updateInformation")}</SubmitButton>
        </Form>
      </Box>
    </>
  );
}
