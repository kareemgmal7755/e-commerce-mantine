import { Flex, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import { Form } from "@mongez/react-form";
import Helmet from "@mongez/react-helmet";
import { IconChevronLeft, IconChevronRight } from "@tabler/icons-react";
import { useMedia } from "apps/front-office/design-system/components";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { forgetPasswordItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import Container from "apps/front-office/design-system/components/Container";
import EmailInput from "apps/front-office/design-system/components/Form/EmailInput";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import style from "../../components/style.module.scss";
import { useForgetPassword } from "../../hooks";

export default function ForgetPassword() {
  const formSubmit = useForgetPassword();
  const media = useMedia(1000);

  return (
    <>
      <Helmet title={trans("forgetPassword")} />
      <Breadcrumb items={forgetPasswordItems} title={trans("forgetPassword")} />
      <Flex justify="center" align="center" my="4rem">
        <Container>
          <Flex
            justify="center"
            m="auto"
            align="center"
            gap="1rem"
            direction="column">
            <Title order={3}>{trans("forgetPassword")}</Title>
            <Text span>{trans("enterEmailBelow")}</Text>
            <Form className={style.form_wrapper} onSubmit={formSubmit}>
              <Flex w={media ? "100%" : "50%"} direction="column" gap="1rem">
                <EmailInput
                  size="md"
                  placeholder={trans("email")}
                  required
                  name="email"
                  autoFocus
                />
                <Flex justify="space-between" align="center">
                  <SubmitButton size="lg" fz="1rem">
                    {trans("nextStep")}
                  </SubmitButton>
                  <Flex gap="0.3rem" align="center">
                    {current("localeCode") === "ar" ? (
                      <IconChevronRight size={18} />
                    ) : (
                      <IconChevronLeft size={18} />
                    )}
                    <UnStyledLink
                      to={URLS.auth.login}
                      c="gray.9"
                      fz="0.9rem"
                      fw={500}>
                      {trans("backToLogin")}
                    </UnStyledLink>
                  </Flex>
                </Flex>
              </Flex>
            </Form>
          </Flex>
        </Container>
      </Flex>
    </>
  );
}
