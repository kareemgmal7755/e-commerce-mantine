import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { loginItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import LoginForm from "../../components/LoginForm";
import VerifyRegisterCode from "../../components/RegisterVerify/VerifyRegisterCode";

export default function LoginPage() {
  return (
    <>
      <Helmet title={trans("login")} />
      <Breadcrumb items={loginItems} title="login" />
      <LoginForm />
    </>
  );
}
