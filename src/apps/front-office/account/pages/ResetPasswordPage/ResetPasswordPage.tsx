import { Flex, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import { Form } from "@mongez/react-form";
import Helmet from "@mongez/react-helmet";
import { IconChevronLeft, IconChevronRight } from "@tabler/icons-react";
import { useMedia } from "apps/front-office/design-system/components";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { resetPasswordItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import Container from "apps/front-office/design-system/components/Container";
import PasswordInput from "apps/front-office/design-system/components/Form/PasswordInput";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import style from "../../components/style.module.scss";
import { useResetPassword } from "../../hooks";

export default function ResetPasswordPage() {
  const resetPasswordSubmit = useResetPassword();
  const media = useMedia(1000);

  return (
    <>
      <Helmet title={trans("resetPassword")} />
      <Breadcrumb items={resetPasswordItems} title={trans("resetPassword")} />
      <Flex justify="center" align="center" my="4rem">
        <Container>
          <Flex
            justify="center"
            m="auto"
            align="center"
            gap="1rem"
            direction="column">
            <Title order={3}>{trans("resetPassword")} </Title>
            <Text span>{trans("enterNewPassword")}</Text>
            <Form className={style.form_wrapper} onSubmit={resetPasswordSubmit}>
              <Flex w={media ? "100%" : "50%"} direction="column" gap="1rem">
                <Flex direction="column" gap="0.5rem">
                  <PasswordInput
                    label={trans("newPassword")}
                    name="password"
                    required
                    id="password"
                    minLength={8}
                    placeholder={trans("newPassword")}
                  />
                  <PasswordInput
                    label={trans("confirmPassword")}
                    name="confirmPassword"
                    required
                    match="password"
                    placeholder={trans("confirmPassword")}
                  />
                </Flex>
                <Flex justify="space-between" align="center">
                  <SubmitButton size="lg" fz="1rem">
                    {trans("resetPassword")}
                  </SubmitButton>
                  <Flex gap="0.3rem" align="center">
                    {current("localeCode") === "ar" ? (
                      <IconChevronRight size={18} />
                    ) : (
                      <IconChevronLeft size={18} />
                    )}
                    <UnStyledLink
                      to={URLS.auth.login}
                      c="gray.9"
                      fz="0.9rem"
                      fw={500}>
                      {trans("backToLogin")}
                    </UnStyledLink>
                  </Flex>
                </Flex>
              </Flex>
            </Form>
          </Flex>
        </Container>
      </Flex>
    </>
  );
}
