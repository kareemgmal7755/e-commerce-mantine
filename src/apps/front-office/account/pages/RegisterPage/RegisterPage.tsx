import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { registerItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import RegisterForm from "../../components/RegisterForm";

export default function RegisterPage() {
  return (
    <>
      <Helmet title={trans("register")} />
      <Breadcrumb items={registerItems} title="register" />
      <RegisterForm />
    </>
  );
}
