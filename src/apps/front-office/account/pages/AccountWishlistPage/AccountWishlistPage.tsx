import { Flex, Table, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { preload } from "@mongez/react-utils";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import { Product } from "apps/front-office/home/utils/types";
import AddToCartButton from "apps/front-office/store/components/AddToCartButton";
import { useCart } from "apps/front-office/store/hooks";
import { price } from "apps/front-office/store/utils/price";
import URLS from "apps/front-office/utils/urls";
import { getWishlist } from "apps/front-office/wishlist/services/wishlist-services";

function _AccountWishlistPage({ response }) {
  const products: Product[] = response.data.products;
  const { addProductToCart } = useCart();

  return (
    <Flex direction="column" gap="1.5rem">
      <Title order={3} c="dark.9">
        {trans("wishlist")}
      </Title>
      <Table.ScrollContainer minWidth={800}>
        <Table horizontalSpacing="sm" verticalSpacing="sm">
          <Table.Thead>
            <Table.Tr>
              <Table.Th tt="uppercase" fw={500} ta="center">
                {trans("image")}
              </Table.Th>
              <Table.Th tt="uppercase" fw={500} ta="center">
                {trans("productId")}
              </Table.Th>
              <Table.Th tt="uppercase" fw={500} ta="center">
                {trans("productDetails")}
              </Table.Th>
              <Table.Th tt="uppercase" fw={500} ta="center">
                {trans("price")}
              </Table.Th>
              <Table.Th tt="uppercase" fw={500} ta="center">
                {trans("action")}
              </Table.Th>
            </Table.Tr>
          </Table.Thead>
          <Table.Tbody>
            {products.map(product => (
              <Table.Tr key={product.id}>
                <Table.Td ta="center">
                  <UnStyledLink to={URLS.shop.viewProduct(product)}>
                    <img
                      src={product.images[0].url}
                      alt={product.name}
                      width={80}
                      height={120}
                      style={{ objectFit: "contain" }}
                    />
                  </UnStyledLink>
                </Table.Td>
                <Table.Td ta="center">
                  <Text span fz="0.9rem" fw={600}>
                    #{product.id}
                  </Text>
                </Table.Td>
                <Table.Td ta="center">
                  <Text span c="dark.9" fz="0.9rem" fw={500}>
                    {product.name}
                  </Text>
                </Table.Td>
                <Table.Td ta="center">
                  <Text span c="dark.9" fz="1rem" fw={600}>
                    {price(product.price)}
                  </Text>
                </Table.Td>
                <Table.Td ta="center">
                  <AddToCartButton product={product} />
                </Table.Td>
              </Table.Tr>
            ))}
            <Table.Tr></Table.Tr>
          </Table.Tbody>
        </Table>
      </Table.ScrollContainer>
    </Flex>
  );
}

const AccountWishlistPage = preload(_AccountWishlistPage, getWishlist);
export default AccountWishlistPage;
