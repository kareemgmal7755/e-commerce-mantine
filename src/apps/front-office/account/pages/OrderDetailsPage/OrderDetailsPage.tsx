import { Flex, Grid, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { preload } from "@mongez/react-utils";
import { orderAtom } from "../../atoms";
import OrderAddress from "../../components/OrderAddress";
import OderInvoice from "../../components/OrderInvoice/OrderInvoice";
import OrderProductsTable from "../../components/OrderProductsTable";
import OrderStatus from "../../components/OrderStatus";
import OrderTracker from "../../components/OrderTracker";
import style from "../../components/style.module.scss";
import { getOrder } from "../../service/orders";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";

function _OrderDetailsPage() {
  const order = orderAtom.useValue();

  return (
    <Flex direction="column" gap="1.5rem">
      <Title order={3} c="dark.9">
        {trans("orderDetails")}
      </Title>
      <OrderTracker />
      <Grid>
        <Grid.Col span={{ base: 12, md: 8 }}>
          <Flex direction="column" gap="1rem">
            <OrderProductsTable items={order.items} />
            <OrderAddress />
          </Flex>
        </Grid.Col>
        <Grid.Col span={{ base: 12, md: 4 }}>
          <Flex direction="column" gap="1.5rem" align="center">
            <OderInvoice />
            <Flex
              className={style.payment_method_wrapper}
              px={10}
              py={8}
              fw={600}
              align="center"
              w="100%"
              fz="0.9rem"
              justify="space-between">
              <Text span fz="0.85rem" fw={600}>
                {trans("paymentMethod")}
              </Text>
              <Text span fz="0.9rem" fw={700} c="rhino.8">
                {trans(order.paymentMethod.label)}
              </Text>
            </Flex>
            <OrderStatus />
            <SubmitButton>{trans("cancelOrder")}</SubmitButton>
          </Flex>
        </Grid.Col>
      </Grid>
    </Flex>
  );
}

const OrderDetailsPage = preload(
  _OrderDetailsPage,
  ({ params }) => getOrder(params.id),
  {
    onSuccess(response) {
      orderAtom.update(response.data.order);
    },
  },
);

export default OrderDetailsPage;
