import { Box, Flex, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { Form } from "@mongez/react-form";
import Helmet from "@mongez/react-helmet";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import PasswordInput from "apps/front-office/design-system/components/Form/PasswordInput";
import { useUpdatePassword } from "../../hooks";

export default function UpdatePasswordPage() {
  const updatePasswordSubmit = useUpdatePassword();

  return (
    <>
      <Helmet title={trans("updatePassword")} />
      <Box>
        <Title order={3} c="dark.9">
          {trans("updatePassword")}
        </Title>
        <Form onSubmit={updatePasswordSubmit}>
          <Flex mt="1.5rem" direction="column" gap="0.5rem">
            <PasswordInput
              label={trans("password")}
              name="currentPassword"
              required
              minLength={8}
              placeholder={trans("password")}
            />
            <PasswordInput
              label={trans("newPassword")}
              name="password"
              required
              id="password"
              minLength={8}
              placeholder={trans("newPassword")}
            />
            <PasswordInput
              label={trans("confirmPassword")}
              name="confirmPassword"
              required
              match="password"
              placeholder={trans("confirmPassword")}
            />
          </Flex>
          <SubmitButton mt="1.5rem">{trans("updateInformation")}</SubmitButton>
        </Form>
      </Box>
    </>
  );
}
