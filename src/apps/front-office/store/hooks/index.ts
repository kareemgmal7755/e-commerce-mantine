import { trans } from "@mongez/localization";
import { SingleCartItem, cartAtom } from "apps/front-office/cart/atoms";
import {
  addToCart,
  clearCart,
  removeFromCart,
  setCartItemQuantity,
} from "apps/front-office/cart/services/cart-services";
import { settingsAtom } from "apps/front-office/common/atoms";
import {
  addToCompare,
  removeFromCompare,
} from "apps/front-office/compare/services/compare-services";
import {
  toastError,
  toastSuccess,
} from "apps/front-office/design-system/utils/toast";
import { SimpleProduct } from "apps/front-office/home/utils/types";
import parseError from "apps/front-office/utils/parse-error";
import {
  addToWishlist,
  removeFromWishlist,
} from "apps/front-office/wishlist/services/wishlist-services";
import { useEffect, useState } from "react";
import { remainingTimeAtom } from "../atoms/remaining-time-atom";
import { wishlistEvents } from "../events/wishlist";

export function useSocial(product: SimpleProduct) {
  const [isCopied, setIsCopied] = useState(false);

  // Facebook
  const shareOnFacebook = () => {
    const url = window.location.href;
    window.open(`https://www.facebook.com/sharer.php?u=${url}`, "_blank");
  };

  // Twitter
  const shareOnTwitter = () => {
    const url = window.location.href;
    window.open(`https://www.twitter.com/intent/tweet?text=${url}`), "_blank";
  };

  // Whatsapp
  const shareOneWhatsapp = () => {
    const { phoneNumber } = settingsAtom.get("contact");
    const url = window.location.href;
    const message = `Hi, I'm interested in ${product.name} ${product.model} ${product.sku} ${product.brand.name} ${url}`;
    window.open(
      `https://api.whatsapp.com/send?phone=${phoneNumber}&text=${message}`,
      "_blank",
    );
  };

  // Copy Link
  const isCopiedChecked = () => {
    const url = window.location.href;
    setIsCopied(true);
    navigator.clipboard.writeText(url);
  };

  return {
    shareOnFacebook,
    shareOnTwitter,
    shareOneWhatsapp,
    isCopiedChecked,
    isCopied,
  };
}

export function useCart() {
  function addProductToCart(productId: number, quantity: number = 1) {
    return new Promise((resolve, reject) => {
      addToCart(productId, quantity)
        .then((response: any) => {
          toastSuccess(
            `${trans("addToCartSuccessfully")}, ${trans(
              "quantity",
            )}: ${quantity} `,
          );
          resolve(response.data.cart);
        })
        .catch(error => {
          reject(error);
          toastError(parseError(error));
        });
    });
  }

  function removeProductFromCart(itemId: number) {
    return new Promise((resolve, reject) => {
      removeFromCart(itemId)
        .then((response: any) => {
          toastSuccess(trans("removeFromCartSuccessfully"));
          resolve(response.data.cart);
        })
        .catch(error => {
          reject(error);
          toastError(parseError(error));
        });
    });
  }

  return {
    addProductToCart,
    removeProductFromCart,
  };
}

export function clearAllCart() {
  clearCart()
    .then(() => {
      toastSuccess(trans("clearCartSuccessfully"));
      setTimeout(() => {
        cartAtom.reset();
      }, 1000);
    })
    .catch(error => toastError(parseError(error)));
}

export function useCompare() {
  function addProductToCompare(productId: number) {
    return new Promise((resolve, reject) => {
      addToCompare(productId)
        .then((response: any) => {
          toastSuccess(trans("addToCompareSuccessfully"));
          resolve(response.data.compare);
        })
        .catch(error => {
          reject(error);
          toastError(parseError(error));
        });
    });
  }
  function removeProductFromCompare(productId: number) {
    return new Promise((resolve, reject) => {
      removeFromCompare(productId)
        .then((response: any) => {
          toastSuccess(trans("removeFromCompareSuccessfully"));
          resolve(response.data.compare);
        })
        .catch(error => {
          reject(error);
          toastError(parseError(error));
        });
    });
  }

  return {
    addProductToCompare,
    removeProductFromCompare,
  };
}

export function useWishlist() {
  function addProductToWishlist(productId: number) {
    return new Promise((resolve, reject) => {
      addToWishlist(productId)
        .then((response: any) => {
          toastSuccess(trans("addToWishlistSuccessfully"));
          resolve(response.data.wishlist);
          wishlistEvents.triggerAdd(productId);
          wishlistEvents.toggle(productId, "add");
        })
        .catch(error => {
          reject(error);
          toastError(parseError(error));
        });
    });
  }
  function removeProductFromWishlist(productId: number) {
    return new Promise((resolve, reject) => {
      removeFromWishlist(productId)
        .then((response: any) => {
          toastSuccess(trans("removeFromWishlistSuccessfully"));
          resolve(response.data.records);
          wishlistEvents.triggerRemove(productId);
          wishlistEvents.toggle(productId, "remove");
        })
        .catch(error => {
          reject(error);
          toastError(parseError(error));
        });
    });
  }

  return {
    addProductToWishlist,
    removeProductFromWishlist,
  };
}

export function useCartItem(item: SingleCartItem) {
  const product = item.product;
  const [quantity, updateQuantity] = useState(item.quantity);

  const incrementQuantity = () => {
    const newQuantity = quantity + 1;

    if (newQuantity === quantity) return;

    changeQuantity(newQuantity);
  };

  const decrementQuantity = () => {
    let newQuantity = quantity - 1;

    if (newQuantity < 1) {
      newQuantity = 1;
    }

    changeQuantity(newQuantity);
  };

  const changeQuantity = (newQuantity: number) => {
    if (newQuantity === quantity) return;

    if (isNaN(newQuantity)) return;

    const oldQuantity = quantity;
    updateQuantity(newQuantity);

    setCartItemQuantity(item.id, newQuantity)
      .then(response => {
        toastSuccess(
          `${trans("cartItemQuantityUpdated")}, ${trans(
            "quantity",
          )}: ${quantity}`,
        );
        cartAtom.update(response.data.cart);
      })
      .catch(error => {
        updateQuantity(oldQuantity);
        toastError(parseError(error));
      });
  };

  const removeCartItem = () => {
    removeFromCart(item.id)
      .then(response => {
        toastSuccess(trans("cartItemRemovedSuccessfully"));
        cartAtom.update(response.data.cart);
      })
      .catch(error => {
        toastError(parseError(error));
      });
  };

  return {
    product,
    quantity,
    incrementQuantity,
    decrementQuantity,
    removeCartItem,
    changeQuantity,
  };
}

export function useCountdown(targetDate) {
  const countDownDate = new Date(targetDate.timestamp).getTime();
  const [countDown, setCountDown] = useState(
    countDownDate - new Date().getTime(),
  );

  useEffect(() => {
    const interval = setInterval(() => {
      const remainingTime = countDownDate - new Date().getTime();
      if (remainingTime <= 0) {
        clearInterval(interval);
      } else {
        setCountDown(remainingTime);
        remainingTimeAtom.update(remainingTime);
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [countDownDate]);

  return getReturnValues(countDown);
}

const getReturnValues = countDown => {
  const days = Math.floor(countDown / (1000 * 60 * 60 * 24));
  const hours = Math.floor(
    (countDown % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
  );
  const minutes = Math.floor((countDown % (1000 * 60 * 60)) / (1000 * 60));
  const seconds = Math.floor((countDown % (1000 * 60)) / 1000);

  return [days, hours, minutes, seconds];
};
