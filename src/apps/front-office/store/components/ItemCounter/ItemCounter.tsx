import { Flex, Text, UnstyledButton } from "@mantine/core";
import { IconMinus, IconPlus } from "@tabler/icons-react";
import { SingleCartItem } from "apps/front-office/cart/atoms";
import { useCartItem } from "../../hooks";
import style from "../style.module.scss";

export default function ItemCounter({ item }: { item: SingleCartItem }) {
  const { incrementQuantity, decrementQuantity, quantity } = useCartItem(item);

  return (
    <Flex gap="0.5rem">
      <Flex
        align="center"
        justify="space-between"
        className={style.counter_wrapper}
        w="100%">
        <UnstyledButton onClick={incrementQuantity} variant="transparent" p={4}>
          <IconPlus size={14} />
        </UnstyledButton>
        <Text component="p" pb={8} px={4} pt={4} fz="1rem">
          {quantity}
        </Text>
        <UnstyledButton onClick={decrementQuantity} variant="transparent" p={4}>
          <IconMinus size={14} />
        </UnstyledButton>
      </Flex>
    </Flex>
  );
}
