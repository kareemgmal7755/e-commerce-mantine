import { Image } from "@mantine/core";

type BannerImage = {
  src: string;
  alt: string;
};

export default function Banner({ src, alt }: BannerImage) {
  
  return (
    <>
      <Image src={src} alt={alt} w="100%" />
    </>
  );
}
