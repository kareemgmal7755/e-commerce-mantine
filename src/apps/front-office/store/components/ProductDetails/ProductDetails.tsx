import {
  Button,
  Divider,
  Flex,
  Grid,
  Rating,
  Text,
  Title,
  Tooltip,
} from "@mantine/core";
import { useCounter } from "@mantine/hooks";
import {
  IconArrowsShuffle,
  IconBrandFacebook,
  IconBrandTwitter,
  IconBrandWhatsapp,
  IconClipboard,
  IconHeart,
  IconMinus,
  IconPlus,
} from "@tabler/icons-react";
import { Product } from "apps/front-office/home/utils/types";
import { useSocial } from "../../hooks";
import { price } from "../../utils/price";
import ProductImage from "../ProductImage";
import style from "../style.module.scss";

import { trans } from "@mongez/localization";
import HTML from "apps/front-office/design-system/components/HTML";
import { useCompare, useWishlist } from "../../hooks";
import AddToCartButton from "../AddToCartButton";

export default function ProductDetails({ product }: { product: Product }) {
  const [count, handlers] = useCounter(0, {
    min: product.purchase.minQuantity,
    max: product.purchase.maxQuantity,
  });

  const { addProductToWishlist } = useWishlist();
  const { addProductToCompare } = useCompare();

  const {
    shareOnFacebook,
    isCopied,
    shareOnTwitter,
    shareOneWhatsapp,
    isCopiedChecked,
  } = useSocial(product);

  return (
    <Grid gutter={40} my="4rem">
      <Grid.Col span={{ base: 12, md: 6 }}>
        <ProductImage product={product} />
      </Grid.Col>
      <Grid.Col span={{ base: 12, md: 6 }}>
        <Flex gap="0.3rem" direction="column">
          <Title order={2} c="dark.7">
            {product.name}
          </Title>
          <Flex align="center" gap="0.5rem">
            <Rating value={product.rating} readOnly size="sm" />
            <Text span>
              {product.rating} {trans("reviews")}
            </Text>
          </Flex>
          <Flex align="center" gap="0.2rem">
            <Text span fw={600} fz="0.9rem">
              {trans("availability")}:
            </Text>
            <Text
              tt="uppercase"
              fw={600}
              span
              c={product.inStock ? "grassGreen.9" : "red.9"}
              fz="0.85rem">
              {product.inStock ? trans("inStock") : trans("outOfStock")}
            </Text>
          </Flex>
          <Flex gap="0.2rem">
            <Text span fw={600} fz="0.9rem">
              {trans("model")}:
            </Text>
            <Text tt="uppercase" fw={600} span fz="0.85rem">
              {product.model}
            </Text>
          </Flex>
          <Flex gap="0.2rem">
            <Text span fw={600} fz="0.9rem">
              {trans("sku")}:
            </Text>
            <Text tt="uppercase" fw={600} span fz="0.85rem">
              {product.sku}
            </Text>
          </Flex>
          {product.discount ? (
            <Flex gap="0.3rem" align="center">
              <Text span fz="1.2rem" fw={500} c="gray.5" td="line-through">
                {price(product.price)}
              </Text>
              <Text span fz="1.5rem" fw={700} c="artyClickRed.7">
                {price(product.salePrice)}
              </Text>
            </Flex>
          ) : (
            <Text span fz="1.5rem" fw={700} c="artyClickRed.7">
              {price(product.price)}
            </Text>
          )}
          <Divider my="sm" variant="dashed" />
          <HTML html={product.shortDescription} />
          <Divider my="sm" variant="dashed" />
          <Flex gap="1rem" align="center">
            <Flex
              align="center"
              h="2.7rem"
              className={style.counter_wrapper}
              w="6rem"
              justify="space-between">
              <Button
                w="fit-content"
                c="dark.9"
                p={10}
                onClick={handlers.increment}
                variant="transparent">
                <IconPlus size={14} />
              </Button>
              <Text component="p" px={5} fz="1rem">
                {count}
              </Text>
              <Button
                w="fit-content"
                size="xs"
                c="dark.9"
                p={10}
                onClick={handlers.decrement}
                variant="transparent">
                <IconMinus size={14} />
              </Button>
            </Flex>
            <AddToCartButton product={product} />
          </Flex>
          <Flex align="center" gap="1rem">
            <Button
              variant="transparent"
              c="dark.9"
              p={8}
              onClick={() => addProductToWishlist(product.id)}
              className={style.product_settings_button}>
              <Flex gap="0.2rem" align="center">
                <IconHeart size={18} stroke={1.4} />
                <Text span fz="0.9rem" fw={500}>
                  {trans("addToWishlist")}
                </Text>
              </Flex>
            </Button>
            <Button
              className={style.product_settings_button}
              variant="transparent"
              c="dark.9"
              p={8}
              onClick={() => addProductToCompare(product.id)}>
              <Flex gap="0.2rem" align="center">
                <IconArrowsShuffle size={18} stroke={1.4} />
                <Text span fz="0.9rem" fw={500}>
                  {trans("addToCompare")}
                </Text>
              </Flex>
            </Button>
          </Flex>
          <Divider />
          <Flex gap="0.7rem" align="center">
            <Text span fw={600} fz="0.9rem">
              {trans("share")}:
            </Text>
            <Tooltip
              color="rhino.9"
              transitionProps={{ transition: "scale", duration: 300 }}
              label={trans("facebook")}
              fz="0.7rem"
              fw={600}
              withArrow>
              <IconBrandFacebook
                onClick={shareOnFacebook}
                size={18}
                fill="black"
                stroke={0}
                className={style.share_product_button}
              />
            </Tooltip>
            <Tooltip
              w="fit-content"
              color="rhino.9"
              transitionProps={{ transition: "scale", duration: 300 }}
              label={trans("twitter")}
              fz="0.7rem"
              fw={600}
              withArrow>
              <IconBrandTwitter
                onClick={shareOnTwitter}
                size={18}
                fill="black"
                stroke={0}
                className={style.share_product_button}
              />
            </Tooltip>
            <Tooltip
              color="rhino.9"
              transitionProps={{ transition: "scale", duration: 300 }}
              label={trans("whatsapp")}
              fz="0.7rem"
              fw={600}
              withArrow>
              <IconBrandWhatsapp
                onClick={shareOneWhatsapp}
                size={18}
                stroke={1.4}
                className={style.share_product_button}
              />
            </Tooltip>
            <Tooltip
              color="rhino.9"
              transitionProps={{ transition: "scale", duration: 300 }}
              fz="0.7rem"
              fw={600}
              withArrow
              label={!isCopied ? trans("copyToClipboard") : trans("copied")}>
              <IconClipboard
                onClick={isCopiedChecked}
                size={18}
                stroke={1.4}
                className={style.share_product_button}
              />
            </Tooltip>
          </Flex>
        </Flex>
      </Grid.Col>
    </Grid>
  );
}
