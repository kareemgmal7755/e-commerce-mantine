import { Box, Flex, Rating, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import { Product } from "apps/front-office/home/utils/types";
import URLS from "apps/front-office/utils/urls";
import { useState } from "react";
import "react-simple-toasts/dist/theme/dark.css";
import { price } from "../../utils/price";
import ProductActionsIcons from "../ProductActionsIcons";
import style from "../style.module.scss";

export default function ProductGridCard({ product }: { product: Product }) {

  return (
    <Flex
      gap="1.5rem"
      m="1rem"
      mih="22rem"
      direction="column"
      align="center"
      ta="center">
      <Box
        className={style.product_link_image}
        pos="relative"
        h="15rem"
        w="100%">
        {!product.inStock && (
          <Text
            component="p"
            bg="red.9"
            c="gray.0"
            pos="absolute"
            top={0}
            p={5}
            fz="0.65rem"
            fw={600}
            left={current("localeCode") === "en" ? 0 : ""}
            right={current("localeCode") === "en" ? "" : 0}>
            {trans("outOfStock")}
          </Text>
        )}
        <ProductActionsIcons product={product} type="grid" />
        <UnStyledLink to={URLS.shop.viewProduct(product)} w="100%">
          <Flex h="100%" w="100%" justify="center" align="center">
            <img
              src={product.images[0].url}
              alt={product.name}
              width="100%"
              height="100%"
            />
          </Flex>
        </UnStyledLink>
      </Box>
      <Flex w="100%" direction="column" gap="xs" align="center">
        <Text span fw={600} c="gray.5" tt="uppercase" fz="0.75rem">
          {product.category.name}
        </Text>
        <UnStyledLink
          to={URLS.shop.viewProduct(product)}
          c="dark.9"
          fw={600}
          fz="1rem">
          {product.name}
        </UnStyledLink>
        {product.discount ? (
          <Flex gap="0.3rem" align="baseline">
            <Text span c="rhino.9" fz="1rem" fw={600}>
              {price(product.salePrice)}
            </Text>
            <Text span fz="0.9rem" fw={600} c="gray.3" td="line-through">
              {price(product.price)}
            </Text>
          </Flex>
        ) : (
          <Text span fz="1rem" fw={600} c="rhino.9">
            {price(product.price)}
          </Text>
        )}
        <Rating defaultValue={product.rating} size="xs" readOnly />
        {/* <ProductCardImages product={product} setImageIndex={setImageIndex} /> */}
      </Flex>
    </Flex>
  );
}
