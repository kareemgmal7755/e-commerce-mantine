import { Button, Flex, Tooltip } from "@mantine/core";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import { IconArrowsShuffle } from "@tabler/icons-react";
import { SimpleProduct } from "apps/front-office/home/utils/types";
import { useState } from "react";
import { useCompare } from "../../hooks";

export default function CompareButton({
  product,
  type,
}: {
  product: SimpleProduct;
  type?: "grid" | "list";
}) {
  const [isCompare, setIsCompare] = useState(product.inCompare);

  const positionInList = current("localeCode") === "ar" ? "right" : "left";

  const { addProductToCompare, removeProductFromCompare } = useCompare();

  const addToCompare = () => {
    addProductToCompare(product.id);
    setIsCompare(!isCompare);
  };

  const removeFromCompare = () => {
    removeProductFromCompare(product.id);
    setIsCompare(!isCompare);
  };

  return (
    <>
      {!isCompare ? (
        <Flex bg="gray.1">
          <Tooltip
            label={trans("addToCompare")}
            tt="uppercase"
            fw={600}
            position={type === "list" ? positionInList : "top"}
            withArrow
            fz="0.6rem"
            radius={0}>
            <Button
              w={32}
              p={0}
              h={32}
              radius={0}
              bg="gray.0"
              onClick={addToCompare}>
              <IconArrowsShuffle size={18} stroke={1.3} color="#000" />
            </Button>
          </Tooltip>
        </Flex>
      ) : (
        <Flex bg="gray.1">
          <Tooltip
            label={trans("removeFromCompare")}
            tt="uppercase"
            fw={600}
            position={type === "list" ? positionInList : "top"}
            withArrow
            fz="0.6rem"
            radius={0}>
            <Button
              w={32}
              p={0}
              h={32}
              radius={0}
              bg="rhino.9"
              onClick={removeFromCompare}>
              <IconArrowsShuffle size={18} stroke={1.3} color="#fff" />
            </Button>
          </Tooltip>
        </Flex>
      )}
    </>
  );
}
