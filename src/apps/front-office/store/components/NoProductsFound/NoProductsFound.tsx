import { Flex, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { navigateBack } from "@mongez/react-router";
import { useMedia } from "apps/front-office/design-system/components";
import { PrimaryButton } from "apps/front-office/design-system/components/Buttons/PrimaryButton";
import { RhinoButton } from "apps/front-office/design-system/components/Buttons/RhinoButton";
import Container from "apps/front-office/design-system/components/Container";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import cartEmpty from "assets/images/empty.gif";

export default function NoProductsFound({ text }: { text: string }) {
  const isMobile = useMedia(800);

  return (
    <Container>
      <Flex
        h="100%"
        ta="center"
        justify="center"
        align="center"
        gap="1rem"
        py="2rem"
        direction="column">
        <img
          src={cartEmpty}
          alt={trans("empty")}
          width={!isMobile ? 500 : "100%"}
        />
        <Title order={2} fz="2rem">
          {trans(text)}
        </Title>
        <Flex
          gap="1.5rem"
          align="center"
          direction={isMobile ? "column" : "row"}>
          <PrimaryButton w="13rem" h="3.2rem" onClick={() => navigateBack()}>
            {trans("goBack")}
          </PrimaryButton>
          <UnStyledLink to={URLS.shop.list}>
            <RhinoButton h="3.2rem" w="13rem">
              {trans("continueShopping")}
            </RhinoButton>
          </UnStyledLink>
        </Flex>
      </Flex>
    </Container>
  );
}
