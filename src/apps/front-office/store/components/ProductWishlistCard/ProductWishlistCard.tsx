import {
  Box,
  Button,
  Divider,
  Flex,
  Rating,
  Text,
  Tooltip,
} from "@mantine/core";
import { trans } from "@mongez/localization";
import { IconX } from "@tabler/icons-react";
import { useMedia } from "apps/front-office/design-system/components";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import { Product } from "apps/front-office/home/utils/types";
import URLS from "apps/front-office/utils/urls";
import "react-simple-toasts/dist/theme/dark.css";
import { useCart, useWishlist } from "../../hooks";
import { price } from "../../utils/price";
import AddToCartButton from "../AddToCartButton";
import CompareButton from "../CompareButton";
import QuickViewButton from "../QuickViewButton";
import SelectOptionsButton from "../SelectOptionsButton";
import WishlistButton from "../WishlistButton";
import style from "../style.module.scss";

export default function ProductWishlistCard({ product }: { product: Product }) {
  const { removeProductFromWishlist } = useWishlist();
  const { addProductToCart } = useCart();

  const media = useMedia(1000);

  return (
    <Flex
      gap="1.5rem"
      m="1rem"
      mih="22rem"
      direction="column"
      align="center"
      pos="relative">
      <Tooltip
        label={trans("removeFromWishlist")}
        fw={600}
        tt="uppercase"
        withArrow
        fz="0.6rem">
        <Button
          onClick={() => removeProductFromWishlist(product.id)}
          bg="rhino.9"
          c="gray.0"
          size="xs"
          p={7}
          radius="50%"
          right={-3}
          style={{ zIndex: 3 }}
          top={-5}
          pos="absolute">
          <IconX size={14} />
        </Button>
      </Tooltip>
      <Box
        className={style.product_link_image}
        pos="relative"
        h="12rem"
        w="100%">
        <Flex pos="absolute" className={style.product_grid_actions_wrapper}>
          <SelectOptionsButton product={product} type="grid" />
          <Divider />
          <QuickViewButton product={product} type="grid" />
          <Divider />
          <WishlistButton product={product} type="grid" />
          <Divider />
          <CompareButton product={product} type="grid" />
        </Flex>

        <UnStyledLink to={URLS.shop.viewProduct(product)} w="100%">
          <Flex h="100%" w="100%" justify="center" align="center">
            <img
              src={product.images[0].url}
              alt={product.name}
              width="100%"
              height="100%"
            />
          </Flex>
        </UnStyledLink>
      </Box>
      <Flex w="100%" direction="column" gap="xs" align="center">
        <Text span fw={600} c="gray.5" tt="uppercase" fz="0.75rem">
          {product.category.name}
        </Text>
        <UnStyledLink
          to={URLS.shop.viewProduct(product)}
          c="dark.9"
          fw={700}
          fz="0.9rem">
          {product.name}
        </UnStyledLink>
        {product.discount ? (
          <Flex gap="0.3rem" align="baseline">
            <Text span c="rhino.9" fz="1rem" fw={600}>
              {price(product.salePrice)}
            </Text>
            <Text span fz="0.9rem" fw={600} c="gray.3" td="line-through">
              {price(product.price)}
            </Text>
          </Flex>
        ) : (
          <Text span fz="1rem" fw={600} c="rhino.9">
            {price(product.price)}
          </Text>
        )}
        <Rating defaultValue={product.rating} size="xs" readOnly />
        <AddToCartButton product={product} />
      </Flex>
    </Flex>
  );
}
