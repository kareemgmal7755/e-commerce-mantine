import { preload } from "@mongez/react-utils";
import { getCategories } from "../../services/categories-services";

import { Carousel } from "@mantine/carousel";
import { Flex, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import Autoplay from "embla-carousel-autoplay";
import { useRef } from "react";
import style from "../style.module.scss";

function _CategoriesCarousel({ response }) {
  const autoplay = useRef(Autoplay({ delay: 2000 }));
  const categories: any = response.data.categories;
  if (!categories) return;

  return (
    <Carousel
      loop
      slideSize={{ base: "50%", sm: "33.3%", md: "20%", xs: "100%" }}
      slideGap={30}
      plugins={[autoplay.current]}
      slidesToScroll="auto"
      withControls={false}>
      {categories.map(category => (
        <Carousel.Slide key={category.id} className={style.carousel_slide}>
          <UnStyledLink
            to={URLS.shop.viewCategory(category)}
            c="dark.9"
            className={style.category_link}>
            <Flex direction="column" w="100%">
              <Flex justify="center" align="center" w="100%" h="10rem">
                <img
                  src={category.image.url}
                  alt={category.name}
                  width="100%"
                  height={250}
                  style={{ objectFit: "contain" }}
                />
              </Flex>
              <Flex
                bg="gray.0"
                direction="column"
                gap="0.5rem"
                py="0.5rem"
                align="center"
                ta="center">
                <Title order={4} c="dark.9">
                  {category.name}
                </Title>
                <Text span c="gray.6">
                  {category.totalProducts} {trans("products")}
                </Text>
              </Flex>
            </Flex>
          </UnStyledLink>
        </Carousel.Slide>
      ))}
    </Carousel>
  );
}

const CategoriesCarousel = preload(_CategoriesCarousel, getCategories);

export default CategoriesCarousel;
