import { Grid } from "@mantine/core";
import ReviewsList from "../ReviewsList";
import WriteReview from "../WriteReview";

export default function ProductReviews() {
  return (
    <Grid justify="space-between">
      <Grid.Col span={{ base: 12, md: 6 }}>
        <ReviewsList />
      </Grid.Col>
      <Grid.Col span={{ base: 12, md: 6 }}>
        <WriteReview />
      </Grid.Col>
    </Grid>
  );
}
