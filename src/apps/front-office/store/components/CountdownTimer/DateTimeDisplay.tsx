import { Box, Flex, Text } from "@mantine/core";

const DateTimeDisplay = ({ value, type, isDanger }: any) => {
  return (
    <Box>
      <Flex gap={5} direction="column" justify="center" align="center">
        <Flex
          bg="gray.3"
          fz="xs"
          ta="center"
          justify="center"
          align="center"
          h="25rem"
          w="25rem">
          {value}
        </Flex>
        <Text
          ta="center"
          fz="0.6rem"
          color={isDanger ? "red.6" : "dark.1"}
          fw={isDanger ? 600 : 500}
          tt="uppercase">
          {type}
        </Text>
      </Flex>
    </Box>
  );
};

export default DateTimeDisplay;
