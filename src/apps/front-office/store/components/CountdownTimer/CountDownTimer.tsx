import { Flex } from "@mantine/core";
import { trans } from "@mongez/localization";
import DateTimeDisplay from "./DateTimeDisplay";
import { useCountdown } from "../../hooks";

export type CountdownProps = {
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
};

const ShowCounter = ({ days, hours, minutes, seconds }: CountdownProps) => {
  return (
    <Flex gap={10}>
      <DateTimeDisplay value={days} type={trans("days")} isDanger={false} />
      <DateTimeDisplay value={hours} type={trans("hours")} isDanger={false} />
      <DateTimeDisplay value={minutes} type={trans("mins")} isDanger={false} />
      <DateTimeDisplay value={seconds} type={trans("secs")} isDanger={false} />
    </Flex>
  );
};

const CountdownTimer = ({ expireDate }: any) => {
  const [days, hours, minutes, seconds] = useCountdown(expireDate);

  return (
    <ShowCounter
      days={days}
      hours={hours}
      minutes={minutes}
      seconds={seconds}
    />
  );
};

export default CountdownTimer;
