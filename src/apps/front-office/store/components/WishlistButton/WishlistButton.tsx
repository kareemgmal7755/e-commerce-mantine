import { Button, Flex, Tooltip } from "@mantine/core";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import { IconHeart } from "@tabler/icons-react";
import { SimpleProduct } from "apps/front-office/home/utils/types";
import { useState } from "react";
import { useWishlist } from "../../hooks";

export default function WishlistButton({
  product,
  type,
}: {
  product: SimpleProduct;
  type?: "grid" | "list";
}) {
  const [inWishlist, setInWishlist] = useState(product.inWishlist);

  const positionInList = current("localeCode") === "ar" ? "right" : "left";

  const { addProductToWishlist, removeProductFromWishlist } = useWishlist();

  const addToWishlist = () => {
    addProductToWishlist(product.id);
    setInWishlist(!inWishlist);
  };

  const removeFromWishlist = () => {
    removeProductFromWishlist(product.id);
    setInWishlist(!inWishlist);
  };

  return (
    <>
      {!inWishlist ? (
        <Flex bg="gray.1">
          <Tooltip
            label={trans("addToWishlist")}
            tt="uppercase"
            fw={600}
            position={type === "list" ? positionInList : "top"}
            withArrow
            fz="0.6rem"
            radius={0}>
            <Button
              w={32}
              p={0}
              h={32}
              radius={0}
              bg="gray.0"
              onClick={addToWishlist}>
              <IconHeart size={18} stroke={1.3} color="#000" />
            </Button>
          </Tooltip>
        </Flex>
      ) : (
        <Flex bg="gray.1">
          <Tooltip
            label={trans("removeFromWishlist")}
            tt="uppercase"
            fw={600}
            position={type === "list" ? positionInList : "top"}
            withArrow
            fz="0.6rem"
            radius={0}>
            <Button
              w={32}
              p={0}
              h={32}
              radius={0}
              bg="rhino.9"
              onClick={removeFromWishlist}>
              <IconHeart size={18} stroke={1.3} color="#fff" />
            </Button>
          </Tooltip>
        </Flex>
      )}
    </>
  );
}
