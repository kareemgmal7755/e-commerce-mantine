import { Box, Flex, Rating, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import { IconShoppingCart } from "@tabler/icons-react";
import { useMedia } from "apps/front-office/design-system/components";
import { RhinoButton } from "apps/front-office/design-system/components/Buttons/RhinoButton";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import { Product } from "apps/front-office/home/utils/types";
import URLS from "apps/front-office/utils/urls";
import { useState } from "react";
import { price } from "../../utils/price";
import ProductActionsIcons from "../ProductActionsIcons";
import style from "../style.module.scss";

export default function ProductListCard({ product }: { product: Product }) {

  const media = useMedia(1000);

  return (
    <Flex
      justify="space-between"
      align="center"
      gap="1.5rem"
      pos="relative"
      p="1rem"
      direction={media ? "column" : "row"}
      className={style.product_wrapper}>
      {!product.inStock && (
        <Text
          component="p"
          bg="red.9"
          c="gray.0"
          pos="absolute"
          top={0}
          p={5}
          fz="0.65rem"
          fw={600}
          left={current("localeCode") === "en" ? 0 : ""}
          right={current("localeCode") === "en" ? "" : 0}>
          {trans("outOfStock")}
        </Text>
      )}
      <Box
        h="15rem"
        w={media ? "100%" : "30%"}
        className={style.product_link_image}
        pos="relative">
        <ProductActionsIcons product={product} type="list" />

        <UnStyledLink to={URLS.shop.viewProduct(product)}>
          <Flex
            className={style.product_link_image}
            h="100%"
            w="100%"
            justify="center"
            align="center">
            <img
              src={product.images[0].url}
              alt={product.name}
              width="100%"
              height="100%"
            />
          </Flex>
        </UnStyledLink>
      </Box>
      <Flex w={media ? "100%" : "70%"} direction="column" gap="xs">
        <Text span fw={600} c="gray.5" tt="uppercase" fz="0.75rem">
          {product.category.name}
        </Text>
        <UnStyledLink to={URLS.shop.viewProduct(product)} c="dark.9" fw={700}>
          {product.name}
        </UnStyledLink>
        {product.discount ? (
          <Flex gap="0.3rem" align="center">
            <Text span fz="0.9rem" fw={600} c="gray.3" td="line-through">
              {price(product.price)}
            </Text>
            <Text span c="rhino.9" fz="0.9rem" fw={600}>
              {price(product.salePrice)}
            </Text>
          </Flex>
        ) : (
          <Text span fz="0.9rem" fw={600} c="rhino.9">
            {price(product.price)}
          </Text>
        )}
        <Rating defaultValue={product.rating} size="xs" readOnly />
        <Text component="p" fz="0.8rem" fw={600}>
          {product.shortDescription}
        </Text>
        <UnStyledLink to={URLS.shop.viewProduct(product)}>
          <RhinoButton size="md">
            <Flex gap="0.5rem" align="center">
              <IconShoppingCart size={18} />
              <Text span tt="uppercase" fw={600} fz="0.9rem">
                {trans("quickShop")}
              </Text>
            </Flex>
          </RhinoButton>
        </UnStyledLink>
      </Flex>
    </Flex>
  );
}
