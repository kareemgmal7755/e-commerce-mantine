import { Box, Flex, Table, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { checkoutAtom } from "apps/front-office/checkout/atom";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import { price } from "apps/front-office/store/utils/price";
import URLS from "apps/front-office/utils/urls";
import style from "../style.module.scss";

export default function OrderTable() {
  const items = checkoutAtom.use("cart").items;

  return (
    <Box className={style.address_wrapper} p="1rem" w="100%">
      <Title order={4} tt="uppercase">
        {trans("orderSummary")}
      </Title>
      <Table.ScrollContainer minWidth={800}>
        <Table horizontalSpacing="sm" verticalSpacing="sm">
          <Table.Thead>
            <Table.Tr>
              <Table.Th tt="uppercase" fw={500}>
                {trans("product")}
              </Table.Th>
              <Table.Th tt="uppercase" fw={500} ta="center">
                {trans("quantity")}
              </Table.Th>
              <Table.Th tt="uppercase" fw={500} ta="center">
                {trans("price")}
              </Table.Th>
              <Table.Th tt="uppercase" fw={500} ta="center">
                {trans("discount")}
              </Table.Th>
              <Table.Th tt="uppercase" fw={500} ta="center">
                {trans("total")}
              </Table.Th>
            </Table.Tr>
          </Table.Thead>
          <Table.Tbody>
            {items.map(item => (
              <Table.Tr key={item.id}>
                <Table.Td>
                  <Flex gap="1rem" align="center">
                    <UnStyledLink to={URLS.shop.viewProduct(item.product)}>
                      <img
                        src={item.product.images[0].url}
                        alt={item.product.name}
                        width={80}
                        height={120}
                        style={{ objectFit: "contain" }}
                      />
                    </UnStyledLink>
                    <Flex direction="column" gap="0.3rem">
                      <Text span fz="1.05rem">
                        {item.product.name}
                      </Text>
                      <Flex gap="0.1rem">
                        <Text span fz="0.8rem">
                          {trans("brand")}:
                        </Text>
                        <Text span fz="0.8rem">
                          {item.product.brand.name}
                        </Text>
                      </Flex>
                    </Flex>
                  </Flex>
                </Table.Td>
                <Table.Td ta="center">
                  <Text span>{item.quantity}</Text>
                </Table.Td>
                <Table.Td ta="center">
                  <Text span c="dark.9" fz="1rem" fw={600}>
                    {price(item.total.price)}
                  </Text>
                </Table.Td>
                <Table.Td ta="center">
                  <Text span c="dark.9" fz="1rem" fw={600}>
                    {price(item.total.discount)}
                  </Text>
                </Table.Td>
                <Table.Td ta="center">
                  <Text span c="dark.9" fz="1rem" fw={600}>
                    {price(item.total.finalPrice)}
                  </Text>
                </Table.Td>
              </Table.Tr>
            ))}
            <Table.Tr></Table.Tr>
          </Table.Tbody>
        </Table>
      </Table.ScrollContainer>
    </Box>
  );
}
