import { Divider, Flex } from "@mantine/core";
import { Product } from "apps/front-office/home/utils/types";
import "react-simple-toasts/dist/theme/dark.css";
import CompareButton from "../CompareButton";
import QuickViewButton from "../QuickViewButton";
import SelectOptionsButton from "../SelectOptionsButton";
import WishlistButton from "../WishlistButton";
import style from "../style.module.scss";

export default function ProductActionsIcons({
  product,
  type,
}: {
  product: Product;
  type: "grid" | "list";
}) {
  return (
    <Flex
      direction={type === "grid" ? "row" : "column"}
      pos="absolute"
      className={
        type === "grid"
          ? style.product_grid_actions_wrapper
          : style.product_list_actions_wrapper
      }>
      <SelectOptionsButton product={product} type={type} />
      <Divider />
      <QuickViewButton product={product} type={type} />
      <Divider />
      <WishlistButton product={product} type={type} />
      <Divider />
      <CompareButton product={product} type={type} />
    </Flex>
  );
}
