import {
  Box,
  Button,
  Divider,
  Flex,
  Modal,
  Rating,
  Text,
  Title,
  Tooltip,
} from "@mantine/core";
import { useCounter, useDisclosure } from "@mantine/hooks";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import {
  IconArrowsShuffle,
  IconBrandFacebook,
  IconBrandTwitter,
  IconBrandWhatsapp,
  IconClipboard,
  IconHeart,
  IconMinus,
  IconPlus,
  IconZoomIn,
} from "@tabler/icons-react";
import { useMedia } from "apps/front-office/design-system/components";
import HTML from "apps/front-office/design-system/components/HTML";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import { Product } from "apps/front-office/home/utils/types";
import URLS from "apps/front-office/utils/urls";
import { useCompare, useSocial, useWishlist } from "../../hooks";
import { price } from "../../utils/price";
import AddToCartButton from "../AddToCartButton";
import ProductImage from "../ProductImage";
import style from "../style.module.scss";

export default function QuickViewButton({
  product,
  type,
}: {
  product: Product;
  type?: "grid" | "list";
}) {
  const [opened, { open, close }] = useDisclosure(false);
  const [count, handlers] = useCounter(0, {
    min: product.purchase.minQuantity,
    max: product.purchase.maxQuantity,
  });

  const {
    shareOnFacebook,
    isCopied,
    shareOnTwitter,
    shareOneWhatsapp,
    isCopiedChecked,
  } = useSocial(product);

  const { addProductToWishlist } = useWishlist();

  const { addProductToCompare } = useCompare();

  const positionInList = current("localeCode") === "ar" ? "right" : "left";

  const isMobile = useMedia(1000);

  return (
    <>
      <Flex bg="gray.1">
        <Tooltip
          label={trans("quickView")}
          tt="uppercase"
          fw={600}
          display={current("route") === "/compare" ? "none" : ""}
          position={type === "list" ? positionInList : "top"}
          withArrow
          fz="0.6rem"
          radius={0}>
          <Button w={32} p={0} h={32} radius={0} bg="gray.1" onClick={open}>
            <IconZoomIn size={18} stroke={1.3} color="#000" />
          </Button>
        </Tooltip>
      </Flex>
      <Modal
        opened={opened}
        onClose={close}
        size="xl"
        styles={{
          header: {
            padding: "0.5rem",
          },
        }}>
        <Flex
          justify="space-between"
          gap="md"
          direction={isMobile ? "column" : "row"}>
          <Flex
            w={isMobile ? "100%" : "45%"}
            ta="center"
            direction="column"
            justify="space-between"
            h="100%"
            gap="2rem">
            <ProductImage product={product} />
            <UnStyledLink
              to={URLS.shop.viewProduct(product)}
              c="dark.9"
              td="underline"
              w="100%">
              {trans("viewMoreDetails")}
            </UnStyledLink>
          </Flex>
          <Flex w={isMobile ? "100%" : "55%"} direction="column" gap="0.5rem">
            <UnStyledLink to={URLS.shop.viewProduct(product)} c="dark.9">
              <Title order={4} tt="uppercase">
                {product.name}
              </Title>
            </UnStyledLink>
            <Rating defaultValue={product.rating} readOnly size="xs" />
            <Flex gap="0.3rem" align="baseline">
              <Text component="p">{trans("category")}:</Text>
              <UnStyledLink
                fw={600}
                c="rhino.9"
                tt="uppercase"
                to={URLS.shop.viewCategory(product.category)}>
                {product.category.name}
              </UnStyledLink>
            </Flex>
            <Flex gap="0.3rem" align="baseline">
              <Text component="p">{trans("brand")}:</Text>
              <UnStyledLink
                fw={600}
                c="rhino.9"
                tt="uppercase"
                to={URLS.shop.viewBrand(product.brand)}>
                {product.brand.name}
              </UnStyledLink>
            </Flex>
            {product.model && (
              <Flex gap="0.3rem" align="baseline">
                <Text component="p">{trans("model")}:</Text>
                <Text component="p" fw={600} c="rhino.9" tt="uppercase">
                  {product.model}
                </Text>
              </Flex>
            )}
            <Flex gap="0.3rem" align="baseline">
              <Text component="p">{trans("sku")}:</Text>
              <Text component="p" fw={600} c="rhino.9" tt="uppercase">
                {product.sku}
              </Text>
            </Flex>
            {product.discount ? (
              <Flex gap="0.3rem" align="center">
                <Text span fz="1.2rem" fw={600} c="gray.5" td="line-through">
                  {price(product.price)}
                </Text>
                <Text span c="rhino.9" fz="1.4rem" fw={600}>
                  {price(product.salePrice)}
                </Text>
              </Flex>
            ) : (
              <Text span fz="1.4rem" fw={600} c="rhino.9">
                {price(product.price)}
              </Text>
            )}
            <Divider my="sm" variant="dashed" />
            <HTML html={product.shortDescription} />
            <Divider my="sm" variant="dashed" />
            <Flex gap="0.5rem" align="flex-start">
              {product.inStock && (
                <Flex
                  w="10rem"
                  align="center"
                  justify="space-between"
                  h={42.2}
                  className={style.counter_wrapper}>
                  <Button
                    w="fit-content"
                    c="dark.9"
                    p={10}
                    onClick={handlers.increment}
                    variant="transparent">
                    <IconPlus size={14} />
                  </Button>
                  <Text component="p" px={5} fz="1rem">
                    {count}
                  </Text>
                  <Button
                    w="fit-content"
                    size="xs"
                    c="dark.9"
                    p={10}
                    onClick={handlers.decrement}
                    variant="transparent">
                    <IconMinus size={14} />
                  </Button>
                </Flex>
              )}
              <Box className={style.add_to_cart_wrapper}>
                <AddToCartButton product={product} />
              </Box>
            </Flex>
            <Flex align="center" gap="1rem">
              <Button
                variant="transparent"
                c="rhino.9"
                p={8}
                onClick={() => addProductToWishlist(product.id)}>
                <Flex gap="0.2rem" align="center">
                  <IconHeart size={18} stroke={1.4} />
                  <Text span fz="0.9rem" fw={500}>
                    {trans("addToWishlist")}
                  </Text>
                </Flex>
              </Button>
              <Button
                variant="transparent"
                c="rhino.9"
                p={8}
                onClick={() => addProductToCompare(product.id)}>
                <Flex gap="0.2rem" align="center">
                  <IconArrowsShuffle size={18} stroke={1.4} />
                  <Text span fz="0.9rem" fw={500}>
                    {trans("addToCompare")}
                  </Text>
                </Flex>
              </Button>
            </Flex>
            <Flex align="center" gap="1rem">
              <Tooltip
                offset={-5}
                color="rhino.9"
                transitionProps={{ transition: "scale", duration: 300 }}
                label="Facebook"
                fz="0.7rem"
                fw={600}
                withArrow>
                <Button
                  onClick={shareOnFacebook}
                  variant="transparent"
                  c="#2f415d">
                  <IconBrandFacebook size={20} />
                </Button>
              </Tooltip>
              <Tooltip
                offset={-5}
                color="#2f415d"
                transitionProps={{ transition: "scale", duration: 300 }}
                label="Twitter"
                fz="0.7rem"
                fw={600}
                withArrow>
                <Button
                  onClick={shareOnTwitter}
                  variant="transparent"
                  c="#2f415d">
                  <IconBrandTwitter size={20} stroke={1.4} />
                </Button>
              </Tooltip>
              <Tooltip
                offset={-5}
                color="#2f415d"
                transitionProps={{ transition: "scale", duration: 300 }}
                label="Whatsapp"
                fz="0.7rem"
                fw={600}
                withArrow>
                <Button
                  onClick={shareOneWhatsapp}
                  variant="transparent"
                  c="#2f415d">
                  <IconBrandWhatsapp size={20} stroke={1.4} />
                </Button>
              </Tooltip>
              <Tooltip
                offset={-5}
                color="#2f415d"
                transitionProps={{ transition: "scale", duration: 300 }}
                fz="0.7rem"
                fw={600}
                withArrow
                label={!isCopied ? "Copy To Clipboard" : "Copied"}>
                <Button
                  onClick={isCopiedChecked}
                  variant="transparent"
                  c="#2f415d">
                  <IconClipboard size={20} stroke={1.4} />
                </Button>
              </Tooltip>
            </Flex>
          </Flex>
        </Flex>
      </Modal>
    </>
  );
}
