import { Tabs } from "@mantine/core";
import { trans } from "@mongez/localization";
import HTML from "apps/front-office/design-system/components/HTML";
import { SimpleProduct } from "apps/front-office/home/utils/types";
import { useState } from "react";
import ProductReviews from "../ProductReviews";
import ProductSpecifications from "../ProductSpecifications";

export type ProductTabsProps = {
  product: SimpleProduct;
};

const tabsData = ["details", "specifications", "reviews"];

export default function ProductTabs({ product }: ProductTabsProps) {
  const [activeTab, setActiveTab] = useState<string | null>("details");

  return (
    <Tabs
      defaultValue="details"
      variant="none"
      color="cloudBurst.9"
      radius={0}
      onChange={setActiveTab}>
      <Tabs.List justify="center" mb="2rem">
        {tabsData.map((tab, index) => (
          <Tabs.Tab
            key={index}
            m="0.5rem"
            bg={activeTab === tab ? "rhino.9" : "romance.0"}
            c={activeTab === tab ? "gray.0" : "dark.5"}
            value={tab}
            px={30}
            py={12}
            fw={600}
            fz="1rem"
            tt="uppercase">
            {trans(tab)}
          </Tabs.Tab>
        ))}
      </Tabs.List>
      <Tabs.Panel value="details">
        <HTML html={product.description} />
      </Tabs.Panel>
      <Tabs.Panel value="specifications">
        <ProductSpecifications />
      </Tabs.Panel>
      <Tabs.Panel value="reviews">
        <ProductReviews />
      </Tabs.Panel>
    </Tabs>
  );
}
