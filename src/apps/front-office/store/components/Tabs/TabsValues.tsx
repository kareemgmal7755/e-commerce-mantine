import { Tabs } from "@mantine/core";
import { trans } from "@mongez/localization";

export default function TabsValues({ text, value }: any) {
  return (
    <Tabs.Tab value={value} w="100%" ta="start" p="0.5rem" fw={600} fz="0.7rem">
      {trans(text)}
    </Tabs.Tab>
  );
}
