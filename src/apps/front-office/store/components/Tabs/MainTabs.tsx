import { Flex, Tabs } from "@mantine/core";
import { trans } from "@mongez/localization";
export default function MainTabs({ data }: any) {
  return (
    <Flex justify="space-between" wrap="wrap">
      {data.map(tab => (
        <Tabs.Tab fw={600} key={tab} value={tab}>
          {trans(tab)}
        </Tabs.Tab>
      ))}
    </Flex>
  );
}
