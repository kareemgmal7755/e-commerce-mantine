import { Button, Menu } from "@mantine/core";
import TabsValues from "./TabsValues";

export default function ResponsiveTabs({ data, Icon }: any) {
  return (
    <Menu position="bottom-end">
      <Menu.Target>
        <Button mb="0.5rem">
          <Icon />
        </Button>
      </Menu.Target>
      <Menu.Dropdown p={0}>
        {data.map(tab => (
          <TabsValues key={tab} value={tab} text={tab} />
        ))}
      </Menu.Dropdown>
    </Menu>
  );
}
