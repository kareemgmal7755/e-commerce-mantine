import { Button, Flex, Tooltip } from "@mantine/core";
import { trans } from "@mongez/localization";
import { IconLayoutGrid, IconList } from "@tabler/icons-react";
import { productsDisplayAtom } from "../../atoms/products-atom";

export default function ProductsDisplaySettings() {
  const displayMode = productsDisplayAtom.useValue();

  return (
    <Flex gap="1rem" align="center">
      <Tooltip label={trans("grid")} withArrow fw={500} fz="0.8rem">
        <Button
          color="rhino.9"
          p={8}
          size="xs"
          variant={displayMode === "grid" ? "filled" : "outline"}
          onClick={() => productsDisplayAtom.update("grid")}>
          <IconLayoutGrid size={14} />
        </Button>
      </Tooltip>
      <Tooltip label={trans("list")} withArrow fw={500} fz="0.8rem">
        <Button
          color="rhino.9"
          p={8}
          size="xs"
          variant={displayMode === "list" ? "filled" : "outline"}
          onClick={() => productsDisplayAtom.update("list")}>
          <IconList size={14} />
        </Button>
      </Tooltip>
    </Flex>
  );
}
