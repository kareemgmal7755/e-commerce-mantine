import SwitchInput from "apps/front-office/design-system/components/Form/CheckBox/SwitchInput";
import { submitProductsFilter } from "./SubmitProductsFilter";

export default function BooleanFilter({ filter, name }: any) {
  return (
    <SwitchInput
      color="rhino.9"
      name={name}
      label={
        filter.label + (filter.data.total ? ` (${filter.data.total})` : "")
      }
      onChange={submitProductsFilter}
    />
  );
}
