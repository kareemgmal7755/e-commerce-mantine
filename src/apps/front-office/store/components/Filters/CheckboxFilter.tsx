import { Flex, Text } from "@mantine/core";
import CheckBoxInput from "apps/front-office/design-system/components/Form/CheckBox/CheckBoxInput";
import { submitProductsFilter } from "./SubmitProductsFilter";

export default function CheckboxFilter({ filter, name, dataKey }: any) {
  const { data } = filter;

  const allData = data.map((data, index) => {
    const dataType = data[dataKey];

    return (
      <Flex justify="space-between" align="center" key={index}>
        <CheckBoxInput
          fw={600}
          color="rhino.9"
          defaultValue={dataType.id}
          name={name}
          label={dataType.name}
          onChange={submitProductsFilter}
        />
        <Text span>{data?.total}</Text>
      </Flex>
    );
  });

  return <>{allData}</>;
}
