import { getForm } from "@mongez/react-form";
import { queryString } from "@mongez/react-router";
import { toastError } from "apps/front-office/design-system/utils/toast";
import parseError from "apps/front-office/utils/parse-error";
import { productsAtom } from "../../atoms/products-atom";
import { getProducts } from "../../services/products-services";

let lastRequest: AbortController | undefined;

export const submitProductsFilter = (moreValues: any = {}) => {
  const filterForm = getForm("filters-form");

  if (!filterForm) return;

  setTimeout(() => {
    const values = filterForm.values();

    const params = {
      ...values,
      ...moreValues,
      page: 1,
    };

    return filterProducts(params);
  }, 0);
};

export function filterProducts(params: any) {
  Object.keys(params).forEach(key => {
    if (!params[key]) {
      delete params[key];
    }
  });

  queryString.update(params);

  getProducts(params)
    .then(response => {
      productsAtom.update({
        products: response.data.products,
        paginationInfo: response.data.paginationInfo,
      });
    })
    .catch(error => toastError(parseError(error)));
}
