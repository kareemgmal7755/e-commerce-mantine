import { Accordion, ScrollArea, Title } from "@mantine/core";
import { Form } from "@mongez/react-form";
import { useOnce } from "@mongez/react-hooks";
import React, { useMemo } from "react";
import { filtersAtom } from "../../atoms/filters-atom";
import { sortByAtom } from "../../atoms/sort-by-atoms";
import { price } from "../../utils/price";
import BooleanFilter from "./BooleanFilter";
import CheckboxFilter from "./CheckboxFilter";
import SliderFilter from "./SliderFilter";

export const scrollProps = {
  height: 300,
  mx: "auto",
  px: 15,
  type: "auto",
  offsetScrollbars: true,
  scrollbarSize: 9,
};

const filter = (
  type: string,
  component: React.ComponentType,
  moreOptions: any = {},
) => {
  return {
    type,
    component,
    scrollable: true,
    ...moreOptions,
  };
};

const filtersList = [
  filter("categories", CheckboxFilter, {
    dataKey: "category",
  }),
  filter("brands", CheckboxFilter, {
    dataKey: "brand",
  }),
  filter("price", SliderFilter, {
    scrollable: false,
    label: price,
  }),
  {
    type: "hasDiscount",
    component: BooleanFilter,
    scrollable: false,
  },
  {
    type: "inStock",
    component: BooleanFilter,
    scrollable: false,
  },
  {
    type: "featured",
    component: BooleanFilter,
    scrollable: false,
  },
];

export default function Filters() {
  const sortBy = sortByAtom.use("value");

  const filtersData: any = filtersAtom.useValue();

  if (!filtersData) return;

  useOnce(() => {
    return () => {
      filtersAtom.reset();
    };
  });

  const filter = useMemo(
    () =>
      filtersList.map((filter, index) => {
        const filterData = filtersData.find(
          filterData => filterData.type === filter.type,
        );

        if (!filterData) return <React.Fragment key={index}></React.Fragment>;

        const Component: any = filter.component;

        const Wrapper: any = filter.scrollable
          ? ScrollArea.Autosize
          : React.Fragment;

        const wrapperProps = filter.scrollable ? scrollProps : {};

        return (
          <Accordion
            defaultValue={filterData.type}
            styles={{
              content: {
                padding: "0.5rem",
              },
            }}
            variant="separated"
            mb="1rem"
            key={index}>
            <Accordion.Item key={index} value={filterData.type}>
              <Accordion.Control>
                <Title order={5}>{filterData.label}</Title>
              </Accordion.Control>
              <Accordion.Panel>
                <Wrapper {...wrapperProps}>
                  <Component
                    dataKey={filter.dataKey || filterData.type}
                    name={filterData.type}
                    key={index}
                    filter={filterData}
                    label={filterData.label}
                  />
                </Wrapper>
              </Accordion.Panel>
            </Accordion.Item>
          </Accordion>
        );
      }),
    [filtersData],
  );

  return <Form id="filters-form">{filter}</Form>;
}
