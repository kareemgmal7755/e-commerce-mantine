import { Box, Flex, RangeSlider, Tooltip } from "@mantine/core";
import { trans } from "@mongez/localization";
import { queryString } from "@mongez/react-router";
import { ucfirst } from "@mongez/reinforcements";
import { price } from "apps/front-office/store/utils/price";
import { useState } from "react";
import { submitProductsFilter } from "./SubmitProductsFilter";
import NumberInput from "apps/front-office/design-system/components/Form/NumberInput";

export default function SliderFilter({ filter, name }: any) {
  const { data } = filter;

  const minName = `min${ucfirst(name)}`;
  const maxName = `max${ucfirst(name)}`;

  const [values, setValues] = useState<[number, number]>([
    queryString.get(minName) || data.min,
    queryString.get(maxName) || data.max,
  ]);

  return (
    <>
      <Flex gap={5} justify="space-between"></Flex>
      <Box>
        <RangeSlider
          pb="1rem"
          size="sm"
          color="rhino.9"
          step={1}
          value={values}
          label={price}
          name="price"
          fz="sm"
          onChange={setValues}
          min={data.min}
          max={data.max}
          onChangeEnd={submitProductsFilter}
        />
      </Box>
      <Box>
        <Flex gap={10}>
          <Tooltip label={trans(minName)}>
            <span>
              <NumberInput
                name={minName}
                value={values[0]}
                size="xs"
                onChange={value => {
                  setValues([value, values[1]]);
                  submitProductsFilter();
                }}
              />
            </span>
          </Tooltip>
          <Tooltip label={trans(maxName)}>
            <span>
              <NumberInput
                size="xs"
                name={maxName}
                value={values[1]}
                onChange={value => {
                  setValues([values[0], value]);
                  submitProductsFilter();
                }}
              />
            </span>
          </Tooltip>
        </Flex>
      </Box>
    </>
  );
}
