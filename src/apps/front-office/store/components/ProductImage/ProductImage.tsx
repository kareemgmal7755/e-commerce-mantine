import { Carousel } from "@mantine/carousel";
import { Box, Flex } from "@mantine/core";
import { useMedia } from "apps/front-office/design-system/components";
import { SimpleProduct } from "apps/front-office/home/utils/types";
import { useState } from "react";
import Magnifier from "react-magnifier";

const ImageMagnifier = Magnifier as any;

export default function ProductImage({ product }: { product: SimpleProduct }) {
  const [imageIndex, setImageIndex] = useState<number>(0);

  const media = useMedia(1000);

  if (!product) return;

  return (
    <Flex direction="column" gap="1rem">
      <Flex h="20rem" align="center" justify="center">
        {media ? (
          <img
            src={product.images[imageIndex].url}
            alt={product.name}
            width="100%"
            height="100%"
            style={{ objectFit: "contain" }}
            
          />
        ) : (
          <ImageMagnifier
            style={{ objectFit: "contain" }}
            src={product.images[imageIndex].url}
            alt={product.name}
            width="100%"
            height="100%"
        
          />
        )}
      </Flex>
      {product.images.length > 1 && (
        <>
          {product.images.length > 3 && (
            <Carousel slideSize="33%">
              {product.images.map(image => (
                <Carousel.Slide key={image.id}>
                  <Box>
                    <img src={image.url} alt={product.name} />
                  </Box>
                </Carousel.Slide>
              ))}
            </Carousel>
          )}
          {product.images.length <= 3 && (
            <Flex justify="center" gap={20}>
              {product.images.map((image, index) => (
                <Box
                  h="5rem"
                  key={image.id}
                  style={{ cursor: "pointer" }}
                  onClick={() => setImageIndex(index)}>
                  <img
                    style={{ objectFit: "contain" }}
                    src={image.url}
                    alt={product.name}
                    width="100%"
                    height="100%"
                  />
                </Box>
              ))}
            </Flex>
          )}
        </>
      )}
    </Flex>
  );
}
