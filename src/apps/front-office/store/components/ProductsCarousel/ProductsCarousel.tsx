import { Carousel, CarouselProps } from "@mantine/carousel";
import { useMedia } from "apps/front-office/design-system/components";
import { Product } from "apps/front-office/home/utils/types";
import ProductGridCard from "../ProductGridCard";

export default function ProductsCarousel({
  products,
  ...props
}: {
  products: Product[];
} & CarouselProps) {
  const isMobile = useMedia(800);

  return (
    <Carousel
      slideSize={{ base: "100%", md: "25%", sm: "50" }}
      slidesToScroll={isMobile ? 1 : 2}
      slideGap="md"
      loop
      containScroll="trimSnaps"
      {...props}>
      {products.map((product, index) => (
        <Carousel.Slide key={index}>
          <ProductGridCard product={product} key={index} />
        </Carousel.Slide>
      ))}
    </Carousel>
  );
}
