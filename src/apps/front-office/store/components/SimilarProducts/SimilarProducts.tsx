import { Carousel } from "@mantine/carousel";
import { Flex, SimpleGrid } from "@mantine/core";
import { preload } from "@mongez/react-utils";
import SectionTitle from "apps/front-office/design-system/components/SectionTitle";
import ProductGridCard from "apps/front-office/store/components/ProductGridCard";
import { productAtom } from "../../atoms/products-atom";
import { getSimilarProducts } from "../../services/products-services";

function _SimilarProducts({ response }: any) {
  const products = response.data.products;
  if (products.length === 0) return;

  return (
    <Flex gap="1rem" direction="column">
      <SectionTitle title="YOU MAY ALSO LIKE" subtitle="Browse Bestseller" />
      {products.length <= 4 && (
        <SimpleGrid cols={{ base: 1, md: 4, sm: 2 }}>
          {products.map(product => (
            <ProductGridCard product={product} key={product.id} />
          ))}
        </SimpleGrid>
      )}
      {products.length > 4 && (
        <Carousel
          loop
          slideSize={{ base: "100%", sm: "50%", md: "33.3%" }}
          slideGap={40}
          slidesToScroll={1}
          withControls={true}>
          {products.map(product => (
            <Carousel.Slide key={product.id}>
              <ProductGridCard product={product} key={product.id} />
            </Carousel.Slide>
          ))}
        </Carousel>
      )}
    </Flex>
  );
}

const SimilarProducts = preload(_SimilarProducts, () =>
  getSimilarProducts(productAtom.get("id")),
);

export default SimilarProducts;
