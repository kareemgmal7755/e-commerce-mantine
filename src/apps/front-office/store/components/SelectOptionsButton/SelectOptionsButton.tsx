import { Box, Button, Flex, Modal, Text, Tooltip } from "@mantine/core";
import { useCounter, useDisclosure } from "@mantine/hooks";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import { IconMinus, IconPlus, IconShoppingCart } from "@tabler/icons-react";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import { Product } from "apps/front-office/home/utils/types";
import URLS from "apps/front-office/utils/urls";
import { productsDisplayAtom } from "../../atoms/products-atom";
import { price } from "../../utils/price";
import AddToCartButton from "../AddToCartButton";
import style from "../style.module.scss";

export default function SelectOptionsButton({
  product,
  type,
}: {
  product: Product;
  type: "grid" | "list";
}) {
  const [opened, { open, close }] = useDisclosure(false);

  const [count, handlers] = useCounter(0, {
    min: product.purchase.minQuantity,
    max: product.purchase.maxQuantity,
  });

  const positionInList = current("localeCode") === "ar" ? "right" : "left";

  const displayMode = productsDisplayAtom.useValue();

  return (
    <>
      <Flex bg="gray.1">
        <Tooltip
          label={trans("selectOptions")}
          tt="uppercase"
          fw={600}
          position={type === "list" ? positionInList : "top"}
          withArrow
          fz="0.6rem"
          radius={0}>
          <Button w={32} p={0} h={32} radius={0} bg="gray.0" onClick={open}>
            <IconShoppingCart size={18} stroke={1.3} color="#000" />
          </Button>
        </Tooltip>
      </Flex>
      <Modal
        opened={opened}
        onClose={close}
        centered
        size="sm"
        radius={0}
        styles={{
          header: {
            padding: "0.5rem",
          },
        }}>
        <Flex direction="column" gap="1.5rem">
          <Flex gap="0.5rem" align="center">
            <Box h="7rem" w="40%">
              <img
                src={product.images[0].url}
                alt={product.name}
                width="100%"
                height="100%"
                style={{ objectFit: "contain" }}
              />
            </Box>
            <Flex direction="column" gap="0.5rem" w="60%">
              <Text span fw={600} fz="1rem">
                {product.name}
              </Text>
              {product.discount ? (
                <Flex gap="0.3rem" align="center">
                  <Text span fz="0.85rem" fw={600} c="gray.5" td="line-through">
                    {price(product.price)}
                  </Text>
                  <Text span c="rhino.9" fz="0.9rem" fw={700}>
                    {price(product.salePrice)}
                  </Text>
                </Flex>
              ) : (
                <Text span fz="0.9rem" fw={600} c="rhino.9">
                  {price(product.price)}
                </Text>
              )}
              <Flex
                align="center"
                h="2.7rem"
                className={style.counter_wrapper}
                w="6rem"
                justify="space-between">
                <Button
                  w="fit-content"
                  c="dark.9"
                  p={10}
                  onClick={handlers.increment}
                  variant="transparent">
                  <IconPlus size={14} />
                </Button>
                <Text component="p" px={5} fz="1rem">
                  {count}
                </Text>
                <Button
                  w="fit-content"
                  size="xs"
                  c="dark.9"
                  p={10}
                  onClick={handlers.decrement}
                  variant="transparent">
                  <IconMinus size={14} />
                </Button>
              </Flex>
            </Flex>
          </Flex>
          <Flex gap="1rem" justify="center" align="center">
            <AddToCartButton product={product} count={count} />
          </Flex>
          <UnStyledLink
            to={URLS.shop.viewProduct(product)}
            c="dark.9"
            td="underline"
            w="100%"
            ta="center">
            {trans("viewMoreDetails")}
          </UnStyledLink>
        </Flex>
      </Modal>
    </>
  );
}
