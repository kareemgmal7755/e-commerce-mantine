import { Flex, Select, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { queryString } from "@mongez/react-router";
import { sortByAtom } from "../../atoms/sort-by-atoms";
import { filterProducts } from "../Filters/SubmitProductsFilter";

export default function ProductsSortByOptions() {
  const sortByOptions = sortByAtom.use("options");
  const orderBy = queryString.get("orderBy");
  if (!sortByOptions) return;

  return (
    <Flex gap="0.5rem" align="center">
      <Text span fw={600} fz="0.9rem">
        {trans("sortBy")}
      </Text>
      <Select
        data={sortByOptions.map((option: any) => ({
          value: String(option.value),
          label: option.text,
        }))}
        fz="0.8rem"
        placeholder={trans("sortBy")}
        fw={600}
        size="xs"
        withCheckIcon={false}
        radius={0}
        defaultValue={orderBy || ""}
        onChange={value => {
          filterProducts({
            orderBy: value,
          });
        }}
      />
    </Flex>
  );
}
