import { Flex, Rating, Switch, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { Form } from "@mongez/react-form";
import user from "apps/front-office/account/user";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import TextAreaInput from "apps/front-office/design-system/components/Form/TextAreaInput";
import TextInput from "apps/front-office/design-system/components/Form/TextInput";
import { useState } from "react";
import { productAtom } from "../../atoms/products-atom";
import { writeReview } from "../../services/products-services";

export default function WriteReview() {

  // const submitForm = async ({ values }) => {
  //   try {
  //     const response = await writeReview(productAtom.get("id"), values);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  return (
    <Form>
      <Flex direction="column" gap="1rem" bg="romance.0" p={15}>
        <Title order={4} tt="uppercase">
          Write a review
        </Title>
        <TextInput
          name="title"
          placeholder={trans("reviewTitle")}
          label={trans("reviewTitle")}
        />
        <TextAreaInput name="review" placeholder={trans("reviewDetails")} />
        <Flex justify="space-between" align="center">
          <Switch
            name="anonymous"
            color="rhino.9"
            styles={{
              track: { cursor: "pointer" },
            }}
          />
          <Rating name="rating" />
        </Flex>
        <SubmitButton size="md">{trans("submitReview")}</SubmitButton>
      </Flex>
    </Form>
  );
}
