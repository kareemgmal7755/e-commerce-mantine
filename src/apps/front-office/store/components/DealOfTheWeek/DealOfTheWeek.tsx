import { Badge, Box, Flex, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import usePrimaryColor from "apps/front-office/design-system/hooks/use-primary-color";
import { Product } from "apps/front-office/home/utils/types";
import image from "apps/front-office/utils/image";
import URLS from "apps/front-office/utils/urls";
import { price } from "../../utils/price";
import CountdownTimer from "../CountdownTimer/CountDownTimer";
import ProductActionsIcons from "../ProductActionsIcons";

export default function DealOfeWeek({ product }: { product: Product }) {
  const color = usePrimaryColor();

  const timestamp = product.discountEndsAt?.timestamp;

  const available: any = product.stock?.available;

  return (
    <Box pos="relative">
      <ProductActionsIcons product={product} type="grid" />
      <UnStyledLink display="block" to={URLS.shop.viewProduct(product)}>
        <Box>
          <img
            style={{
              maxWidth: "100%",
            }}
            src={image(product.images[0], {
              h: 250,
            })}
            alt={product.name}
          />
          <Badge color="white" pos="absolute" top="4%" right="4%">
            {product.discount ? product.discount?.percentage : 0}%
          </Badge>
        </Box>
      </UnStyledLink>
      <Box m="auto">
        <UnStyledLink>
          <Title order={5} pt={10} pb={7}>
            {product.name}
          </Title>
        </UnStyledLink>
        {product.discount ? (
          <Flex align="center" gap="0.4rem" justify="center">
            <Text
              component="span"
              c="gray.4"
              td="line-through"
              fz="0.9rem"
              fw={600}
              display="block"
              pt="4px">
              {price(product.price)}
            </Text>
            <Text component="span" c={color} fz="1.2rem" fw={700}>
              {price(product.salePrice)}
            </Text>
          </Flex>
        ) : (
          <Text
            component="span"
            c={color}
            fz="1.2rem"
            fw={700}
            ta="center"
            display="block">
            {price(product.price)}
          </Text>
        )}
        <Flex justify="space-between" align="center" gap={5}>
          <Text fz="xs">
            {trans("available")}:
            <Text fw={600} span color={color}>
              {product.stock?.available}
            </Text>
          </Text>
          <Text fz="xs">
            {trans("sold")}:
            <Text fw={600} span c={color}>
              {product.stock?.sold}
            </Text>
          </Text>
        </Flex>
        {/* <Progress my={10} m="auto" ta="center" radius="xl" size={10} /> */}
        {product.discountEndsAt && (
          <Flex
            align="center"
            pt={20}
            justify="space-between"
            direction="column">
            <Box>
              <Text fw={600} fz="md">
                {trans("hurryUp!")}
              </Text>
              <Text fz="xs" span>
                {trans("offerEndsIn")}:
              </Text>
            </Box>
            <Box>
              <CountdownTimer expireDate={timestamp} />
            </Box>
          </Flex>
        )}
      </Box>
    </Box>
  );
}
