import { Box, Flex, Grid } from "@mantine/core";
import { current } from "@mongez/react";
import { useOnce } from "@mongez/react-hooks";
import { useMedia } from "apps/front-office/design-system/components";
import Container from "apps/front-office/design-system/components/Container";
import {
  productAtom,
  productsAtom,
  productsDisplayAtom,
} from "../../atoms/products-atom";
import CategoriesCarousel from "../CategoriesCarousel";
import Filters from "../Filters/Filters";
import MobileFilters from "../MobileFilters";
import NoProductsFound from "../NoProductsFound";
import ProductGridCard from "../ProductGridCard";
import ProductListCard from "../ProductListCard";
import ProductsDisplaySettings from "../ProductsDisplaySettings";
import ProductsSortByOptions from "../ProductsSortByOptions";

export default function ProductsListing() {
  const products = productsAtom.use("products");
  const displayMode = productsDisplayAtom.useValue();
  const isMobile = useMedia(1000);

  if (products.length < 1) return <NoProductsFound text="noProductsFound" />;

  return (
    <Container>
      <Flex my="3rem" direction="column" gap="2rem">
        <CategoriesCarousel />
        <Grid>
          <Grid.Col
            span={{ base: 2.5 }}
            h="100%"
            display={isMobile ? "none" : "block"}>
            <Box pos="relative">
              <Filters />
            </Box>
          </Grid.Col>
          {isMobile && <MobileFilters />}
          <Grid.Col span={{ base: 12, md: 9.5 }}>
            <Flex direction="column" gap="1.5rem">
              <Flex justify="space-between" align="center">
                <ProductsDisplaySettings />
                <ProductsSortByOptions />
              </Flex>
              {displayMode === "list" && (
                <Flex gap="1.5rem" direction="column">
                  {products.map(product => {
                    productAtom.update(product);
                    return (
                      <ProductListCard key={product.id} product={product} />
                    );
                  })}
                </Flex>
              )}
              {displayMode === "grid" && (
                <Grid gutter={50}>
                  {products.map(product => {
                    productAtom.update(product);
                    return (
                      <Grid.Col
                        span={{ base: 12, md: 4, sm: 6 }}
                        key={product.id}
                        py="1rem">
                        <ProductGridCard product={product} />
                      </Grid.Col>
                    );
                  })}
                </Grid>
              )}
            </Flex>
          </Grid.Col>
        </Grid>
      </Flex>
    </Container>
  );
}
