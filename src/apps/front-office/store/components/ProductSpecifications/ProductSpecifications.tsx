import { SimpleGrid, Text } from "@mantine/core";
import { useMediaQuery } from "@mantine/hooks";
import { productAtom } from "../../atoms/products-atom";

export default function ProductSpecifications() {
  const isMobile = useMediaQuery("(max-width: 1000px)");
  const specifications = productAtom.use("specifications");

  if (!specifications) return null;

  return (
    <SimpleGrid cols={isMobile ? 1 : 2}>
      {specifications.map(specification => (
        <SimpleGrid
          cols={2}
          key={specification.label}
          style={{
            border: "1px solid #eee",
          }}>
          <Text p="xs">{specification.label}</Text>
          <Text p="xs" bg="gray.0" c="dark.3" fw="bold">
            {specification.value}
          </Text>
        </SimpleGrid>
      ))}
    </SimpleGrid>
  );
}
