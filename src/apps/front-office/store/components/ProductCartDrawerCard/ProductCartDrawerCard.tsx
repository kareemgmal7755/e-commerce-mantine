import { Divider, Flex, Text, Tooltip, UnstyledButton } from "@mantine/core";
import { trans } from "@mongez/localization";
import { IconX } from "@tabler/icons-react";
import { SingleCartItem } from "apps/front-office/cart/atoms";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import ItemCounter from "apps/front-office/store/components/ItemCounter";
import { useCart } from "apps/front-office/store/hooks";
import { price } from "apps/front-office/store/utils/price";
import URLS from "apps/front-office/utils/urls";

export default function ProductCartDrawerCard({ item }: { item: SingleCartItem }) {
  const { removeProductFromCart } = useCart();

  return (
    <>
      <Flex justify="space-between" gap="0.2rem" align="center">
        <Flex gap="0.5rem" w="75%">
          <img
            src={item.product.images[0].url}
            alt={item.product.name}
            width={80}
            style={{ objectFit: "contain" }}
          />
          <Flex direction="column" gap="0.2rem">
            <UnStyledLink
              fz="0.8rem"
              fw={600}
              c="dark.9"
              to={URLS.shop.viewProduct(item.product)}>
              {item.product.name}
            </UnStyledLink>
            {item.product.discount ? (
              <Flex gap="0.3rem" align="center">
                <Text span c="dark.9" fz="1rem" fw={600}>
                  {price(item.product.salePrice)}
                </Text>
                <Text span fz="0.8rem" fw={600} c="gray.3" td="line-through">
                  {price(item.product.price)}
                </Text>
              </Flex>
            ) : (
              <Text span fz="1rem" fw={600} c="dark.9">
                {price(item.product.price)}
              </Text>
            )}
          </Flex>
        </Flex>
        <Flex direction="column" gap="0.5rem" w="23%">
          <ItemCounter item={item} />
          <Tooltip
            label={trans("delete")}
            position="top"
            fz="0.6rem"
            fw={600}
            withArrow
            offset={-3}>
            <UnstyledButton
              w="fit-content"
              onClick={() => removeProductFromCart(item.id)}>
              <IconX size={15} />
            </UnstyledButton>
          </Tooltip>
        </Flex>
      </Flex>
      <Divider />
    </>
  );
}
