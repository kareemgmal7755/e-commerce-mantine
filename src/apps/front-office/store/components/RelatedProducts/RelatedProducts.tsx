import { Carousel } from "@mantine/carousel";
import { Flex, SimpleGrid } from "@mantine/core";
import SectionTitle from "apps/front-office/design-system/components/SectionTitle";
import { Product } from "apps/front-office/home/utils/types";
import ProductGridCard from "apps/front-office/store/components/ProductGridCard";

export default function relatedProducts({ products }: { products: Product[] }) {
  if (!products) return;

  return (
    <Flex gap="1rem" direction="column">
      <SectionTitle title="Discover Similar" subtitle="RELATED PRODUCTS" />
      {products.length <= 4 && (
        <SimpleGrid cols={{ base: 1, md: 4, sm: 2 }}>
          {products.map(product => (
            <ProductGridCard product={product} key={product.id} />
          ))}
        </SimpleGrid>
      )}
      {products.length > 4 && (
        <Carousel
          withIndicators
          loop
          slideSize={{ base: "100%", sm: "50%", md: "33.3%" }}
          slideGap={40}
          slidesToScroll={1}
          withControls={false}>
          {products.map(product => (
            <Carousel.Slide key={product.id}>
              <ProductGridCard product={product} key={product.id} />
            </Carousel.Slide>
          ))}
        </Carousel>
      )}
    </Flex>
  );
}
