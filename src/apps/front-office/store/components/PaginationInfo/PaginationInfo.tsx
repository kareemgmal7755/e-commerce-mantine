import { Flex, Pagination } from "@mantine/core";
import { currentRoute, queryString } from "@mongez/react-router";
import { PaginationInfo } from "apps/front-office/home/utils/types";

export default function PaginationInfo({
  paginationInfo,
}: {
  paginationInfo?: PaginationInfo;
}) {
  if (!paginationInfo) return null;

  const totalPages = paginationInfo.pages;

  if (totalPages < 2) return null;

  const page = paginationInfo.page;

  const updatePage = page => {
    currentRoute() +
      "?" +
      queryString.update(
        {
          ...queryString.all(),
          page,
        },
        true,
      );
  };

  return (
    <Flex align="center" mt="xl" justify="center">
      <Pagination
        total={totalPages}
        value={page}
        onChange={updatePage}
        color="rhino.9"
        radius={0}
        my="2rem"
      />
    </Flex>
  );
}
