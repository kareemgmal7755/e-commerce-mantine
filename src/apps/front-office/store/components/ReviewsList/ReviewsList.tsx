import {
  Avatar,
  Box,
  Divider,
  Flex,
  Progress,
  Rating,
  Text,
  Title,
} from "@mantine/core";
import { trans } from "@mongez/localization";
import { preload } from "@mongez/react-utils";
import React from "react";
import { productAtom } from "../../atoms/products-atom";
import { getProductReviews } from "../../services/products-services";

function _ReviewsList({ response }) {
  const reviews = response.data.reviews;

  const product = productAtom.useValue();


  if (!product.rating) return;

  const ratingsList = product.ratingsList;

  return (
    <Flex direction="column" gap="0.85rem">
      <Flex gap="0.5rem" align="center">
        <Title order={2} fz="4rem">
          {product.rating}
        </Title>
        <Flex gap="0.2rem" direction="column">
          <Text span fz="1rem" fw={600}>
            {trans("averageRating")}
          </Text>
          <Flex align="center" gap="0.5rem">
            <Rating value={product.rating} readOnly size="sm" />
            <Text span>
              {product.rating} {trans("ratings")}
            </Text>
          </Flex>
        </Flex>
      </Flex>
      {ratingsList.map((rating, index) => {
        const percentage = product.reviews
          ? (rating.count / product.reviews) * 100
          : 0;

        return (
          <Box key={index}>
            <Flex align="center" gap={5}>
              <Rating value={rating.rating} readOnly size="xs" />
              <Box w="100%">
                <Progress value={percentage} color="rhino.9" size="lg" />
              </Box>
              <Text span fz="0.9rem" fw={600} w="5%" ta="end">
                {percentage}%
              </Text>
            </Flex>
          </Box>
        );
      })}
      <Divider color="gray.5" />
      <Title order={4} tt="uppercase">
        {trans("customerReviews")}
      </Title>
      {reviews.map(review => (
        <React.Fragment key={review.id}>
          <Flex gap="md" align="center">
            <Avatar radius="50%" size="lg" color="rhino">
              {review.createdBy.name[0]}
            </Avatar>
            <Flex w="100%" direction="column">
              <Flex justify="space-between" w="100%" align="center">
                <Title order={6}>{review.createdBy.name}</Title>
                <Rating value={review.rating} readOnly size="xs" />
              </Flex>
              <Text component="p">{review.review}</Text>
            </Flex>
          </Flex>
          <Divider color="gray.5" />
        </React.Fragment>
      ))}
    </Flex>
  );
}

const ReviewsList = preload(_ReviewsList, () =>
  getProductReviews(productAtom.get("id")),
);

export default ReviewsList;
