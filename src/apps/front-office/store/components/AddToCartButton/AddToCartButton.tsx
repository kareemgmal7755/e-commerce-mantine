import { Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { RhinoButton } from "apps/front-office/design-system/components/Buttons/RhinoButton";
import { Product } from "apps/front-office/home/utils/types";
import { useCart } from "../../hooks";

export default function AddToCartButton({
  product,
  count = 1,
}: {
  product: Product;
  count?: number;
}) {
  const { addProductToCart } = useCart();

  return (
    <>
      {product.inStock ? (
        <RhinoButton
          size="md"
          onClick={() => addProductToCart(product.id, count)}>
          {trans("addToCart")}
        </RhinoButton>
      ) : (
        <Text
          span
          ta="center"
          bg="red.7"
          tt="uppercase"
          c="gray.0"
          py={8.5}
          px={12}
          fz="1rem"
          fw={600}>
          {trans("outOfStock")}
        </Text>
      )}
    </>
  );
}
