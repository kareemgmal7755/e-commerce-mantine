import { Affix, Drawer, Transition } from "@mantine/core";
import { useDisclosure, useWindowScroll } from "@mantine/hooks";
import { IconFilter } from "@tabler/icons-react";
import { PrimaryButton } from "apps/front-office/design-system/components/Buttons/PrimaryButton";
import Filters from "../Filters/Filters";
import { RhinoButton } from "apps/front-office/design-system/components/Buttons/RhinoButton";

export default function MobileFilters() {
  const [scroll, scrollTo] = useWindowScroll();
  const [opened, { open, close }] = useDisclosure(false);

  return (
    <>
      <Affix position={{ bottom: 20, left: 20 }} zIndex={3}>
        <Transition transition="slide-up" mounted={scroll.y >= 0}>
          {transitionStyles => (
            <RhinoButton
              // style={transitionStyles}
              onClick={open}
              w="2.5rem"
              h="2.5rem"
              px={0}
              py={0}
              style={{ borderRadius: "50%",   border: "1px solid #e96f84" }}>
              <IconFilter  size={20}/>
            </RhinoButton>
          )}
        </Transition>
      </Affix>
      <Drawer opened={opened} onClose={close}>
        <Filters />
      </Drawer>
    </>
  );
}
