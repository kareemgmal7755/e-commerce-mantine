import events from "@mongez/events";

export const wishlistEvents = {
  onRemove: (callback: (productId: number) => void) => {
    return events.subscribe("wishlist.remove", callback);
  },
  triggerRemove: (productId: number) => {
    events.trigger("wishlist.remove", productId);
  },

  onAdd: (callback: (productId: number) => void) => {
    return events.subscribe("wishlist.add", callback);
  },

  triggerAdd: (productId: number) => {
    return events.trigger("wishlist.add", productId);
  },

  onToggle: (callback: (productId: number, mode: "add" | "remove") => void) => {
    return events.subscribe("wishlist.toggle", callback);
  },

  toggle: (productId: number, mode: "add" | "remove") => {
    events.trigger("wishlist.toggle", productId, mode);
  },
};
