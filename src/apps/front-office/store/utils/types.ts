import { Address } from "apps/front-office/address-book/types";
import { Time } from "apps/front-office/blog/utils/types";
import { SingleCartItem, TotalText } from "apps/front-office/cart/atoms";

export type Attachment = {
  id: string | number;
  extension: string;
  url: string;
  fileName: string;
  mimeType: string;
  relativePath: string;
  hash: string;
  size: number;
};

export type Category = {
  id: number;
  slug: string;
  image: Attachment;
  name: string;
};

export type OrderStatus =
  | "pending"
  | "processing"
  | "shipped"
  | "delivered"
  | "canceled";

export type Order = {
  id: number;
  status: {
    name: OrderStatus;
    label: string;
  };
  billingAddress: Address;
  shippingAddress: Address;
  createdAt: Time | any;
  currency: {
    code: string;
    id: number;
    name: string;
    symbol: string;
    value: number;
  };
  finalPrice: number;
  items: SingleCartItem[];
  paymentMethod: {
    name: string;
    label: string;
  };
  totalsText: TotalText[];
  totals: {
    additionalPrice: number;
    commission: number;
    coupon: number;
    discount: number;
    finalPrice: number;
    paymentFees: number;
    price: number;
    quantity: number;
    salePrice: number;
    shippingFees: number;
    subtotal: number;
    tax: number;
  };
};
