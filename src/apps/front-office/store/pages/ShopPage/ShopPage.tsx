import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import { queryString } from "@mongez/react-router";
import { preload } from "@mongez/react-utils";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { filtersAtom } from "../../atoms/filters-atom";
import { productsAtom } from "../../atoms/products-atom";
import { sortByAtom } from "../../atoms/sort-by-atoms";
import PaginationInfo from "../../components/PaginationInfo";
import ProductsListing from "../../components/ProductsListing";
import { getProducts, withFilters } from "../../services/products-services";

function _ShopPage() {
  const paginationInfo = productsAtom.use("paginationInfo");

  return (
    <>
      <Helmet title={trans("shop")} />
      <Breadcrumb title="shop" />
      <ProductsListing />
      <PaginationInfo paginationInfo={paginationInfo} />
    </>
  );
}

const ShopPage = preload(
  _ShopPage,
  () => getProducts({ ...queryString.all(), [withFilters]: true }),
  {
    onSuccess: response => {
      productsAtom.update({
        products: response.data.products,
        paginationInfo: response.data.paginationInfo,
      });
      sortByAtom.update({ options: response.data.sortOptions });
      filtersAtom.update(response.data.filters);
    },
  },
);

export default ShopPage;
