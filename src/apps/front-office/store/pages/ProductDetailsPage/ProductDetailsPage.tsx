import { Box, Flex } from "@mantine/core";
import Helmet from "@mongez/react-helmet";
import { preload } from "@mongez/react-utils";
import Container from "apps/front-office/design-system/components/Container";
import ProductBreadcrumb from "apps/front-office/design-system/components/ProductBreadcrumb";
import { productAtom } from "../../atoms/products-atom";
import ProductDetails from "../../components/ProductDetails";
import ProductTabs from "../../components/ProductTabs";
import RelatedProducts from "../../components/RelatedProducts";
import SimilarProducts from "../../components/SimilarProducts";
import { getProduct } from "../../services/products-services";

function _ProductDetailsPage() {
  const product = productAtom.useValue();

  if (!product) return;

  const relatedProducts = product.relatedProducts;

  return (
    <>
      <Helmet title={product.name} />
      <ProductBreadcrumb />
      <Box mb="2rem">
        <Container>
          <Flex direction="column" gap="2rem">
            <ProductDetails product={product} />
            <ProductTabs product={product} />
            {relatedProducts && <RelatedProducts products={relatedProducts} />}
            <SimilarProducts />
          </Flex>
        </Container>
      </Box>
    </>
  );
}

const ProductDetailsPage = preload(
  _ProductDetailsPage,
  ({ params }) => getProduct(params.id),
  {
    onSuccess(response) {
      productAtom.update(response.data.product);
    },
  },
);

export default ProductDetailsPage;
