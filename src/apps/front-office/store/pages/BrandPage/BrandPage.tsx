import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import { queryString } from "@mongez/react-router";
import { preload } from "@mongez/react-utils";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { shopItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import { filtersAtom } from "../../atoms/filters-atom";
import { productsAtom } from "../../atoms/products-atom";
import { sortByAtom } from "../../atoms/sort-by-atoms";
import PaginationInfo from "../../components/PaginationInfo";
import ProductsListing from "../../components/ProductsListing";
import { getBrand } from "../../services/brands-services";
import { withFilters } from "../../services/products-services";

function _BrandPage() {
  const paginationInfo = productsAtom.use("paginationInfo");

  return (
    <>
      <Helmet title={trans("shop")} />
      <Breadcrumb items={shopItems} title="shop" />
      <ProductsListing />
      <PaginationInfo paginationInfo={paginationInfo} />
    </>
  );
}

const BrandPage = preload(
  _BrandPage,
  ({ params }) =>
    getBrand(params.id, { ...queryString.all(), [withFilters]: true }),
  {
    onSuccess: response => {
      productsAtom.update({
        products: response.data.products,
        paginationInfo: response.data.paginationInfo,
      });
      sortByAtom.update({ options: response.data.sortOptions });
      filtersAtom.update(response.data.filters);
    },
  },
);

export default BrandPage;
