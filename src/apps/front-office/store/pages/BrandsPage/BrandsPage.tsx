import { Grid } from "@mantine/core";
import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import { queryString } from "@mongez/react-router";
import { preload } from "@mongez/react-utils";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { brandsItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import Container from "apps/front-office/design-system/components/Container";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import { productsAtom } from "../../atoms/products-atom";
import { getBrands } from "../../services/brands-services";
import { withFilters } from "../../services/products-services";
import style from "../style.module.scss";

function _BrandsPage({ response }: any) {
  const paginationInfo = productsAtom.use("paginationInfo");

  const brands = response.data.brands;

  if (!brands) return;

  return (
    <>
      <Helmet title={trans("brands")} />
      <Breadcrumb items={brandsItems} title="brands" />
      <Container>
        <Grid my="4rem" gutter={70}>
          {brands.map(brand => (
            <Grid.Col key={brand.id} span={{ base: 6, md: 3 }} w="100%">
              <UnStyledLink
                className={style.brand_image_link}
                to={URLS.shop.viewBrand(brand)}>
                <img
                  src={brand.logo.url}
                  alt={brand.name}
                  width="70%"
                  height={60}
                />
              </UnStyledLink>
            </Grid.Col>
          ))}
        </Grid>
      </Container>
    </>
  );
}

const BrandsPage = preload(_BrandsPage, () =>
  getBrands({ ...queryString.all(), [withFilters]: true }),
);

export default BrandsPage;
