import Helmet from "@mongez/react-helmet";
import { queryString } from "@mongez/react-router";
import { preload } from "@mongez/react-utils";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { filtersAtom } from "../../atoms/filters-atom";
import { productsAtom } from "../../atoms/products-atom";
import { sortByAtom } from "../../atoms/sort-by-atoms";
import PaginationInfo from "../../components/PaginationInfo";
import ProductsListing from "../../components/ProductsListing";
import { getCategory } from "../../services/categories-services";
import { withFilters } from "../../services/products-services";

function _CategoryPage({ response }) {
  const paginationInfo = productsAtom.use("paginationInfo");
  const category = response.data.category;

  return (
    <>
      <Helmet title={category.name} />
      <Breadcrumb title={category.name} />
      <ProductsListing />
      <PaginationInfo paginationInfo={paginationInfo} />
    </>
  );
}

const CategoryPage = preload(
  _CategoryPage,
  ({ params }) =>
    getCategory(params.id, { ...queryString.all(), [withFilters]: true }),
  {
    onSuccess: response => {
      productsAtom.update({
        products: response.data.products,
        paginationInfo: response.data.paginationInfo,
      });
      sortByAtom.update({ options: response.data.sortOptions });
      filtersAtom.update(response.data.filters);
    },
  },
);

export default CategoryPage;
