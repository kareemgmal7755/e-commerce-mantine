import endpoint from "shared/endpoint";

export function getBrands(params: any = {}) {
  return endpoint.get("/brands", {params});
}

export function getBrand(id: number, params: any = {}) {
  return endpoint.get(`/brands/${id}`, {params});
}
