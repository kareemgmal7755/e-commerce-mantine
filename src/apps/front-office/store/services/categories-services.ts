import endpoint from "shared/endpoint";

export function getCategories() {
  return endpoint.get(`/categories`);
}

export function getCategory(id: number | string, params: any = {}) {
  return endpoint.get(`/categories/${id}`, {
    params,
  });
}
