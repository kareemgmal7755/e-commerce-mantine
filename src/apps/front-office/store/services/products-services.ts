import endpoint from "shared/endpoint";

export const withFilters = "wf";

export function getProducts(params: any = {}) {
  return endpoint.get("/products", { params });
}

export function getProduct(id: number) {
  return endpoint.get(`/products/${id}`);
}

export function getSearchedProducts(params: any = {}) {
  return endpoint.get("/products/search", { params });
}

export function getOffers(params: any = {}) {
  return endpoint.get("/products/offers", { params });
}

export function writeReview(data: any, productId: number) {
  return endpoint.post("/reviews", {
    ...data,
    product: productId,
  });
}

export function getProductReviews(productId: number, params: any = {}) {
  return endpoint.get(`/reviews/${productId}`, {
    params,
  });
}

export function getSimilarProducts(id: number) {
  return endpoint.get(`products/${id}/similar-products`, {});
}
