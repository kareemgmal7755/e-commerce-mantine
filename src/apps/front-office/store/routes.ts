import { publicRoutes } from "../utils/router";
import URLS from "../utils/urls";
import BrandPage from "./pages/BrandPage";
import BrandsPage from "./pages/BrandsPage";
import CategoryPage from "./pages/CategoryPage";
import OffersPage from "./pages/OffersPage";
import ProductDetailsPage from "./pages/ProductDetailsPage";
import SearchPage from "./pages/SearchPage";
import ShopPage from "./pages/ShopPage";

publicRoutes([
  {
    path: URLS.shop.list,
    component: ShopPage,
  },
  {
    path: URLS.shop.viewProductRoute,
    component: ProductDetailsPage,
  },
  {
    path: URLS.shop.search,
    component: SearchPage,
  },
  {
    path: URLS.shop.viewProductRoute,
    component: ProductDetailsPage,
  },
  {
    path: URLS.shop.offers,
    component: OffersPage,
  },
  {
    path: URLS.brands,
    component: BrandsPage,
  },
  {
    path: URLS.shop.viewBrandRoute,
    component: BrandPage,
  },
  {
    path: URLS.shop.viewCategoryRoute,
    component: CategoryPage,
  },
]);
