import cache from "@mongez/cache";
import { atom } from "@mongez/react-atom";
import {
  PaginationInfo,
  Product,
  SimpleProduct,
} from "apps/front-office/home/utils/types";

export type ProductsAtomTypes = {
  products: Product[];
  paginationInfo: PaginationInfo;
};

export const productsAtom = atom<ProductsAtomTypes>({
  key: "products",
  default: {
    products: [],
    paginationInfo: {
      limit: 0,
      page: 0,
      pages: 0,
      results: 0,
      total: 0,
    },
  },
});

export const productsDisplayAtom = atom<"grid" | "list">({
  key: "productsDisplayMode",
  beforeUpdate(value) {
    cache.set("productsDisplayMode", value);
    return value;
  },
  default: cache.get("productsDisplayMode", "list"),
});

export const imageIndexAtom = atom<number>({
  key: "imageIndex",
  default: 0,
});

export const productAtom = atom<Product>({
  key: "product",
  default: {},
});

export const productSelectOptionsAtom = atom<SimpleProduct>({
  key: "product",
  default: {},
});
