import { atom } from "@mongez/react-atom";

export const remainingTimeAtom = atom({
  key: "remainingTime",
  default: 0,
});
