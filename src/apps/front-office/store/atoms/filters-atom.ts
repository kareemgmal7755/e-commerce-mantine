import { atom } from "@mongez/react-atom";

export const filtersAtom = atom({
  key: "filters",
  default: [],
});
