import { atom } from "@mongez/react-atom";

type SortByAtomTypes = {
  options: {
    text: string;
    value: string;
  }[];
  value?: string;
};

export const sortByAtom = atom<SortByAtomTypes>({
  key: "sortBy",
  default: {},
});
