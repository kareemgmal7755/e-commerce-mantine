import cache from "@mongez/cache";
import { atom } from "@mongez/react-atom";

export const postsDisplayModeAtom = atom<"grid" | "list">({
  key: "postsDisplayMode",
  beforeUpdate(value) {
    cache.set("displayMode", value);
    return value;
  },
  default: cache.get("displayMode", "grid"),
});
