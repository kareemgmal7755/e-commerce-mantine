import { Box, Flex } from "@mantine/core";
import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import { preload } from "@mongez/react-utils";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { blogItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import Container from "apps/front-office/design-system/components/Container";
import { postsDisplayModeAtom } from "../../atoms/blog-atom";
import PostGridCard from "../../components/PostGridCard";
import PostListCard from "../../components/PostListCard";
import PostsDisplaySettings from "../../components/PostsDisplaySettings";
import PostsSearch from "../../components/PostsSearch";
import { getPosts } from "../../services/posts-services";

function _BlogPage({ response }: any) {
  const posts = response.data.posts;
  const displayMode = postsDisplayModeAtom.useValue();

  return (
    <>
      <Helmet title={trans("blog")} />
      <Breadcrumb title={trans("blog")} items={blogItems} />
      <Box my="4rem">
        {posts.length === 0 && "not found"}
        <Container>
          <Flex justify="space-between" align="center" mb="2rem">
            <PostsSearch />
            <PostsDisplaySettings />
          </Flex>
          {displayMode === "grid" && <PostGridCard posts={posts} />}
          {displayMode === "list" && <PostListCard posts={posts} />}
        </Container>
      </Box>
    </>
  );
}

const BlogPage = preload(_BlogPage, getPosts);

export default BlogPage;
