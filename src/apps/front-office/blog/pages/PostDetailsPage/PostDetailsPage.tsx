import { Box, Flex, Text, Title } from "@mantine/core";
import Helmet from "@mongez/react-helmet";
import { preload } from "@mongez/react-utils";
import { IconCalendar } from "@tabler/icons-react";
import { useMedia } from "apps/front-office/design-system/components";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import Container from "apps/front-office/design-system/components/Container";
import HTML from "apps/front-office/design-system/components/HTML";
import URLS from "apps/front-office/utils/urls";
import { getPost } from "../../services/posts-services";
import { SinglePostTypes } from "../../utils/types";

function _PostDetailsPage({ response }) {
  const media = useMedia(1000);
  const post: SinglePostTypes = response.data.post;
  const postItems = [
    { text: "blog", url: URLS.blog.root },
    { text: post.title, url: URLS.blog.view(post) },
  ];

  return (
    <>
      <Helmet title={post.title} />
      <Breadcrumb items={postItems} title={post.title} />
      <Container>
        <Box my="3rem">
          <Flex justify="center" align="center">
            <img
              src={post.image.url}
              alt={post.title}
              height={media ? 300 : 500}
              width={media ? "100%" : "80%"}
            />
          </Flex>
          <Flex p="1.5rem" gap="1.5rem" direction="column">
            <Flex
              justify="space-between"
              align="center"
              wrap="wrap"
              gap="0.5rem">
              <Title order={2} c="rhino.9">
                {post.title}
              </Title>
              <Flex  gap="0.2rem">
                <IconCalendar color="#3a5173" size={20}/>
                <Text component="span" fz="0.85rem" fw={500} c="rhino.5">
                  {post.createdAt.date}
                </Text>
              </Flex>
            </Flex>
            <HTML html={post.shortDescription} />
            <HTML html={post.description} />
          </Flex>
        </Box>
      </Container>
    </>
  );
}

const PostDetailsPage = preload(_PostDetailsPage, ({ params }) =>
  getPost(params.id),
);

export default PostDetailsPage;
