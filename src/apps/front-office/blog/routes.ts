import { publicRoutes } from "../utils/router";
import URLS from "../utils/urls";
import BlogPage from "./pages/BlogPage";
import PostDetailsPage from "./pages/PostDetailsPage";

publicRoutes([
  { path: URLS.blog.root, component: BlogPage },
  {
    path: URLS.blog.viewRoute,
    component: PostDetailsPage,
  },
]);
