import { Box, Flex, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { IconClockHour1 } from "@tabler/icons-react";
import { useMedia } from "apps/front-office/design-system/components";
import { PrimaryButton } from "apps/front-office/design-system/components/Buttons/PrimaryButton";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import { SinglePostTypes } from "../../utils/types";
import style from "../style.module.scss";

export default function PostListCard({ posts }: { posts: SinglePostTypes[] }) {
  const media = useMedia(1000);

  return (
    <Flex direction="column" gap="xl">
      {posts.map(post => (
        <Flex
          gap="lg"
          align="center"
          direction={media ? "column" : "row"}
          key={post.id}>
          <UnStyledLink
            h="17rem"
            w={media ? "100%" : "35%"}
            to={URLS.blog.view(post)}>
            <Box className={style.post_link_image} h="100%" w="100%">
              <img
                src={post.image.url}
                alt={post.title}
                width="100%"
                height="100%"
              />
            </Box>
          </UnStyledLink>
          <Flex
            w={media ? "100%" : "50%"}
            gap="md"
            py="md"
            direction="column"
            justify="space-between">
            <UnStyledLink to={URLS.blog.view(post)} c="dark.9">
              <Title order={5}>{post.title}</Title>
            </UnStyledLink>
            <Flex justify="space-between" align="center" color="gray.3">
              <Flex gap="0.3rem" align="center">
                <IconClockHour1 size={16} color="#828282" />
                <Text span fz="0.8rem" c="#828282" fw={600}>
                  {post.createdAt.date}
                </Text>
              </Flex>
            </Flex>
            <Text span>{post.shortDescription}</Text>
            <UnStyledLink to={URLS.blog.view(post)}>
              <PrimaryButton size="sm">{trans("readMore")}</PrimaryButton>
            </UnStyledLink>
          </Flex>
        </Flex>
      ))}
    </Flex>
  );
}
