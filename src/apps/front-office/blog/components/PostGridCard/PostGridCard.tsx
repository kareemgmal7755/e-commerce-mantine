import { Box, Flex, Grid, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { readMoreChars } from "@mongez/reinforcements";
import { IconClockHour1 } from "@tabler/icons-react";
import { PrimaryButton } from "apps/front-office/design-system/components/Buttons/PrimaryButton";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import { SinglePostTypes } from "../../utils/types";
import style from "../style.module.scss";

export default function PostGridCard({ posts }: { posts: SinglePostTypes[] }) {
  return (
    <Grid gutter={40}>
      {posts.map(post => (
        <Grid.Col span={{ base: 12, md: 4, sm: 6 }} key={post.id}>
          <Box>
            <Box
              style={{ overflow: "hidden" }}
              h="14rem"
              w="100%"
              className={style.post_link_image}>
              <UnStyledLink to={URLS.blog.view(post)}>
                <img
                  src={post.image.url}
                  alt={post.title}
                  width="100%"
                  height="100%"
                />
              </UnStyledLink>
            </Box>
            <Flex
              gap="md"
              py="md"
              direction="column"
              h="12rem"
              justify="space-between">
              <UnStyledLink to={URLS.blog.view(post)} c="dark.9">
                <Title order={5}>{post.title}</Title>
              </UnStyledLink>
              <Flex justify="space-between" align="center" color="gray.3">
                <Flex gap="0.3rem" align="center">
                  <IconClockHour1 size={16} color="#828282" />
                  <Text span fz="0.8rem" c="#828282" fw={600}>
                    {post.createdAt.date}
                  </Text>
                </Flex>
              </Flex>
              <Text span>{readMoreChars(post.shortDescription, 80)}</Text>
              <UnStyledLink to={URLS.blog.view(post)}>
                <PrimaryButton size="sm">{trans("readMore")}</PrimaryButton>
              </UnStyledLink>
            </Flex>
          </Box>
        </Grid.Col>
      ))}
    </Grid>
  );
}
