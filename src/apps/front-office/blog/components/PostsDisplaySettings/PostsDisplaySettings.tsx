import { Button, Flex, Tooltip } from "@mantine/core";
import { trans } from "@mongez/localization";
import { IconLayoutGrid, IconList } from "@tabler/icons-react";
import { postsDisplayModeAtom } from "../../atoms/blog-atom";

export default function PostsDisplaySettings() {
  const displayMode = postsDisplayModeAtom.useValue();

  return (
    <Flex gap="1rem" align="center">
      <Tooltip label={trans("grid")}>
        <Button
          color="#2f415d"
          p={8}
          size="xs"
          variant={displayMode === "grid" ? "filled" : "outline"}
          onClick={() => postsDisplayModeAtom.update("grid")}>
          <IconLayoutGrid size={14}/>
        </Button>
      </Tooltip>
      <Tooltip label={trans("list")}>
        <Button
          color="#2f415d"
          p={8}
          size="xs"
          variant={displayMode === "list" ? "filled" : "outline"}
          onClick={() => postsDisplayModeAtom.update("list")}>
          <IconList size={14}/>
        </Button>
      </Tooltip>
    </Flex>
  );
}
