import { Button } from "@mantine/core";
import { trans } from "@mongez/localization";
import { Form } from "@mongez/react-form";
import { queryString } from "@mongez/react-router";
import { IconSearch } from "@tabler/icons-react";
import TextInput from "apps/front-office/design-system/components/Form/TextInput";
import { useState } from "react";
import { searchPosts } from "../../hooks";
import style from "../style.module.scss";

export default function PostsSearch() {
  const [query, setQuery] = useState(() => queryString.get("query", ""));

  return (
    <Form className={style.search_form} onSubmit={() => searchPosts(query)}>
      <TextInput
        name="title"
        value={query}
        onChange={value => setQuery(value)}
        placeholder={trans("searchForPosts")}
      />
      <Button type="submit" variant="transparent" p={0} size="md">
        <IconSearch color="#000" size={18} />
      </Button>
    </Form>
  );
}
