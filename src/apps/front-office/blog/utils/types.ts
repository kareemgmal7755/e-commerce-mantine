import { Attachment } from "apps/front-office/design-system/components/Attachments";

export type SinglePostTypes = {
  id: number;
  image: Attachment;
  createdAt: Time;
  title: string;
  shortDescription: string;
  description: string;
  updatedBy: {
    email: string;
    id: number;
    isCustomer: boolean;
    name: string;
    phoneNumber: string;
  };
};

export type Time = {
  timestamp: number;
  date: string;
  format: string;
  humanTime: string;
  text: string;
};
