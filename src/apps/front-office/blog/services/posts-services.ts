import endpoint from "shared/endpoint";

export function getPosts() {
  return endpoint.get("posts");
}

export function getPost(id: any) {
  return endpoint.get(`/posts/${id}`);
}
