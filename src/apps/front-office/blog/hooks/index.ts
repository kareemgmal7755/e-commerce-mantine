import { navigateTo, queryString } from "@mongez/react-router";
import URLS from "apps/front-office/utils/urls";

export const searchPosts = (query: any) => {
  const data: Record<string, any> = {
    ...queryString.all(),
    title: query,
  };

  if (!query) {
    delete data.title;
    
  }

  delete data.page;

  navigateTo(URLS.blog.root + "?" + queryString.toQueryString(data));
};
