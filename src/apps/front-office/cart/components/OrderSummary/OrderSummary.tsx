import { Box, Divider, Flex, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { RhinoButton } from "apps/front-office/design-system/components/Buttons/RhinoButton";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import payment from "assets/images/payment.png";
import { cartAtom } from "../../atoms";

export default function OrderSummary() {
  const totalsText = [...(cartAtom.use("totalsText") || [])];
  const finalPrice = totalsText.pop();

  return (
    <Flex p="1rem" bg="romance.0" direction="column" gap="0.7rem">
      {totalsText.map(total => (
        <Box key={total.type}>
          <Flex justify="space-between" align="center" key={total.label}>
            <Title order={6} tt="uppercase">
              {total.label}
            </Title>
            <Title order={6} fw={500}>
              {trans(total.valueText)}
            </Title>
          </Flex>
          <Divider mt="0.7rem" />
        </Box>
      ))}
      <Flex justify="space-between" align="center">
        <Title order={4} tt="uppercase">
          {trans("total")}
        </Title>
        {finalPrice && (
          <Title order={3} c="rhino.9">
            {finalPrice.valueText}
          </Title>
        )}
      </Flex>
      <UnStyledLink to={URLS.checkout}>
        <RhinoButton w="100%" size="lg" fz="1rem">
          {trans("proceedCheckout")}
        </RhinoButton>
      </UnStyledLink>
      <Flex justify="center" align="center" py="1rem">
        <img src={payment} alt="payImage" width={250} />
      </Flex>
    </Flex>
  );
}
