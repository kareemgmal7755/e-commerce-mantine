import { Flex, Table, Text, Tooltip, UnstyledButton } from "@mantine/core";
import { trans } from "@mongez/localization";
import { IconX } from "@tabler/icons-react";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import { useCart } from "apps/front-office/store/hooks";
import { price } from "apps/front-office/store/utils/price";
import URLS from "apps/front-office/utils/urls";
import { cartAtom } from "../../atoms";

const tableHeader = ["control", "product", "price", "quantity", "total"];

export default function CartItemsTable() {
  const items = cartAtom.use("items");
  const { removeProductFromCart } = useCart();

  return (
    <Table.ScrollContainer minWidth={800}>
      <Table>
        <Table.Thead>
          <Table.Tr>
            {tableHeader.map((header, index) => (
              <Table.Th tt="uppercase" fw={500} ta="center" key={index}>
                {trans(header)}
              </Table.Th>
            ))}
          </Table.Tr>
        </Table.Thead>
        <Table.Tbody>
          {items.map(item => (
            <Table.Tr key={item.id}>
              <Table.Td ta="center">
                <Tooltip
                  label={trans("removeFromCart")}
                  position="top"
                  fz="0.7rem"
                  fw={500}
                  withArrow
                  offset={-3}>
                  <UnstyledButton
                    w="fit-content"
                    onClick={() => removeProductFromCart(item.id)}>
                    <IconX size={20} />
                  </UnstyledButton>
                </Tooltip>
              </Table.Td>
              <Table.Td>
                <Flex gap="1rem" align="center">
                  <UnStyledLink to={URLS.shop.viewProduct(item.product)}>
                    <img
                      src={item.product.images[0].url}
                      alt={item.product.name}
                      width={80}
                      height={120}
                      style={{ objectFit: "contain" }}
                    />
                  </UnStyledLink>
                  <Flex direction="column" gap="0.3rem">
                    <Text span fz="1.05rem">
                      {item.product.name}
                    </Text>
                    <Flex gap="0.1rem">
                      <Text span fz="0.8rem">
                        {trans("brand")}:
                      </Text>
                      <Text span fz="0.8rem">
                        {item.product.brand.name}
                      </Text>
                    </Flex>
                  </Flex>
                </Flex>
              </Table.Td>
              <Table.Td>
                {item.product.discount ? (
                  <Flex gap="0.3rem" align="center" justify="center">
                    <Text span c="dark.9" fz="1rem" fw={600}>
                      {price(item.product.salePrice)}
                    </Text>
                    <Text
                      span
                      fz="0.8rem"
                      fw={600}
                      c="gray.3"
                      td="line-through">
                      {price(item.product.price)}
                    </Text>
                  </Flex>
                ) : (
                  <Flex align="center" justify="center">
                    <Text span fz="1rem" fw={600} c="dark.9">
                      {price(item.product.price)}
                    </Text>
                  </Flex>
                )}
              </Table.Td>
              <Table.Td ta="center">
                <Text span c="dark.9" fz="1rem" fw={600}>
                  {item.quantity}
                </Text>
              </Table.Td>
              <Table.Td ta="center">
                <Text span c="dark.9" fz="1rem" fw={600}>
                  {price(item.total.finalPrice)}
                </Text>
              </Table.Td>
            </Table.Tr>
          ))}
          <Table.Tr></Table.Tr>
        </Table.Tbody>
      </Table>
    </Table.ScrollContainer>
  );
}
