import { Box, Divider, Flex, Grid } from "@mantine/core";
import { trans } from "@mongez/localization";
import { SecondaryButton } from "apps/front-office/design-system/components/Buttons/SecondaryButton";
import Container from "apps/front-office/design-system/components/Container";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import NoProductsFound from "apps/front-office/store/components/NoProductsFound";
import { clearAllCart } from "apps/front-office/store/hooks";
import URLS from "apps/front-office/utils/urls";
import { cartAtom } from "../../atoms";
import CartItemsTable from "../../components/CartItemsTable";
import OrderSummary from "../OrderSummary";
export default function CartContent() {
  const items = cartAtom.use("items");

  if (!items) return <NoProductsFound text="noProductsInCart" />;

  return (
    <Container>
      <Box my="3rem">
        <Grid justify="space-between">
          <Grid.Col span={{ base: 12, md: 8.5 }}>
            <CartItemsTable />
            <Flex justify="space-between" align="center" py="md" w="100%">
              <UnStyledLink to={URLS.shop.list}>
                <SecondaryButton>{trans("continueShopping")}</SecondaryButton>
              </UnStyledLink>
              <SecondaryButton onClick={clearAllCart}>
                {trans("clearCart")}
              </SecondaryButton>
            </Flex>
            <Divider w="100%" />
          </Grid.Col>
          <Grid.Col span={{ base: 12, md: 3.5 }}>
            <OrderSummary />
          </Grid.Col>
        </Grid>
      </Box>
    </Container>
  );
}
