import endpoint from "shared/endpoint";

export function getCart() {
  return endpoint.get("/cart");
}

export function clearCart() {
  return endpoint.delete("/cart");
}

export function addToCart(productId: number, quantity: number) {
  return endpoint.post("/cart", { product: productId, quantity });
}

export function removeFromCart(productId: number) {
  return endpoint.delete(`/cart/${productId}`);
}

export function setCartItemQuantity(itemId: number, quantity: number) {
  return endpoint.put(`/cart/${itemId}`, { quantity });
}
