import { atom } from "@mongez/react-atom";
import { Product } from "apps/front-office/home/utils/types";

export type SingleCartItem = {
  id: number;
  product: Product;
  quantity: number;
  total: {
    discount: number;
    finalPrice: number;
    price: number;
  };
};

export type TotalText = {
  type:
    | "quantity"
    | "price"
    | "discount"
    | "salePrice"
    | "tax"
    | "finalPrice"
    | "shippingFees";
  mode: "normal" | "minus" | "plus";
  label: string;
  value: number;
  valueText: string;
};

export type Coupon = {
  code: string;
  discount: number;
};

export type CartType = {
  id: number;
  items: SingleCartItem[];
  shippingMethod: {
    name: string;
  };
  coupon?: Coupon;
  taxIncluded: boolean;
  taxRate: number;
  totals: {
    coupon: number;
    discount: number;
    finalPrice: number;
    paymentFees: number;
    price: number;
    quantity: number;
    salePrice: number;
    shippingFees: number;
    tax: number;
  };
  totalsText: TotalText[];
};

export const cartAtom = atom<CartType>({
  key: "cart",
  default: {},
});
