import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import { preload } from "@mongez/react-utils";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { cartItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import { cartAtom } from "../../atoms";
import CartContent from "../../components/CartContent";
import { getCart } from "../../services/cart-services";

function _CartPage() {
  return (
    <>
      <Helmet title={trans("cart")} />
      <Breadcrumb items={cartItems} title="cart" />
      <CartContent />
    </>
  );
}

const CartPage = preload(_CartPage, getCart, {
  onSuccess(response) {
    cartAtom.update(response.data.cart);
  },
});
export default CartPage;
