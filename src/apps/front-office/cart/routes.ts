import { publicRoutes } from "../utils/router";
import URLS from "../utils/urls";
import CartPage from "./pages/CartPage";

publicRoutes([
  {
    path: URLS.cart,
    component: CartPage,
  },
]);
