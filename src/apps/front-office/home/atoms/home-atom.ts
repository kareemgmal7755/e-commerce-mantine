import { atom } from "@mongez/react-atom";

export const homeAtom = atom({
  key: "home",
  default: [],
});
