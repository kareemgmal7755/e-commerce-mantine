import { Grid } from "@mantine/core";
import React, { useEffect, useMemo, useRef, useState } from "react";
import { modulesList } from "../../utils/modules-list";
import RowsList from "../RowsList/RowsList";
import { Column } from "../RowsList/types";

export type ColumnProps = {
  column: Column;
  columnIndex: number;
  rowIndex: number;
};

const parseColumnSize = (size: any) => {
  if (typeof size === "number") {
    return {
      span: size,
    };
  }

  return size || {};
};

const defaultColumnSettings = {
  size: 12,
};

export default function Column({ column, rowIndex }: ColumnProps) {
  const rootRef = useRef<HTMLDivElement>(null);
  const [renderedContent, setRenderedContent] = useState<React.ReactNode>("");

  const columnContent = useMemo(() => {
    if (column.rows) {
      return <RowsList rows={column.rows} />;
    }

    const Component = modulesList[column.type as string];

    if (!Component) return null;

    return <Component column={column} />;
  }, [column]);

  useEffect(() => {
    const handleScroll = () => {
      const targetElement = rootRef.current;
      if (targetElement) {
        const targetRect = targetElement.getBoundingClientRect();
        const isInViewport =
          targetRect.top >= 0 && targetRect.bottom <= window.innerHeight;

        const isNearViewport = targetRect.top - window.innerHeight <= 200;

        if (isInViewport || isNearViewport) {
          setRenderedContent(columnContent);
          window.removeEventListener("scroll", handleScroll);
        }
      }
    };

    if (rowIndex < 3) {
      handleScroll();
    }

    // setTimeout(() => {
    // }, 50);

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [columnContent, rowIndex]);

  if (!column.rows && !modulesList[column.type as string]) return null;

  const {
    style: _s,
    perRow: _p,
    row: _r,
    size,
    ...settings
  } = column.settings || {};

  return (
    <Grid.Col
      ref={rootRef}
      {...settings}
      {...parseColumnSize(size || defaultColumnSettings.size)}
      style={{
        maxWidth: "100%",
      }}>
      {renderedContent}
    </Grid.Col>
  );
}
