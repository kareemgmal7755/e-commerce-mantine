import { Carousel } from "@mantine/carousel";
import { Flex, Text, Title } from "@mantine/core";
import Container from "apps/front-office/design-system/components/Container";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import SectionTitle from "apps/front-office/design-system/components/SectionTitle";
import URLS from "apps/front-office/utils/urls";
import Autoplay from "embla-carousel-autoplay";
import { useRef } from "react";
import { Column } from "../../RowsList/types";
import style from "../../style.module.scss";

export default function BrandsModule({ column }: { column: Column }) {
  const brands = column.data.brands;
  const autoplay = useRef(Autoplay({ delay: 2000 }));
  if (!brands) return;

  return (
    <Container>
      <Flex direction="column" gap="1rem" my="1.5rem">
        {column.title && (
          <SectionTitle title={column.title} subtitle={column.title} />
        )}
        <Carousel
          withIndicators
          loop
          slideSize={{ base: "100%", sm: "50%", md: "20%" }}
          slideGap={40}
          plugins={[autoplay.current]}
          onMouseEnter={autoplay.current.stop}
          onMouseLeave={autoplay.current.reset}
          slidesToScroll="auto"
          withControls={false}>
          {brands.map(brand => (
            <Carousel.Slide key={brand.id} className={style.carousel_slide}>
              <UnStyledLink to={URLS.shop.viewBrand(brand)}>
                <Flex direction="column">
                  <Flex justify="center">
                    <img
                      src={brand.logo.url}
                      alt={brand.name}
                      width="90%"
                      height={250}
                      style={{ objectFit: "contain" }}
                    />
                  </Flex>
                  <Flex
                    bg="gray.0"
                    direction="column"
                    gap="0.5rem"
                    py="0.5rem"
                    align="center"
                    ta="center">
                    <Title order={4} c="dark.9">
                      {brand.name}
                    </Title>
                    <Text span c="gray.6">
                      {brand.totalProducts} Products
                    </Text>
                  </Flex>
                </Flex>
              </UnStyledLink>
            </Carousel.Slide>
          ))}
        </Carousel>
      </Flex>
    </Container>
  );
}
