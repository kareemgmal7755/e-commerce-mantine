import { settingsAtom } from "apps/front-office/common/atoms";
import { Attachment } from "apps/front-office/design-system/components/Attachments";
import Banner from "apps/front-office/store/components/Banner";
import URLS from "apps/front-office/utils/urls";
import { UnStyledLink } from "design-system/components/Link";
import React from "react";

const parseUrl = (banner: any) => {
  switch (banner?.type) {
    case "product":
      return URLS.shop.viewProduct(banner.product);
    case "category":
      return URLS.shop.viewCategory(banner);
    case "brand":
      return URLS.shop.viewBrand(banner.brand);
    default:
      break;
  }
};

export type BannerImage = {
  src: string;
  alt: string;
};

export type Banner = {
  type: string;
  image: Attachment;
  title?: string;
  name?: string;
};

export default function BannerModule({ banner }: { banner: Banner }) {
  const appName = settingsAtom.use("general").appName;

  if (!banner) return null;

  const Wrapper = banner.type !== "image" ? UnStyledLink : React.Fragment;
  const wrapperProps = banner.type !== "image" ? { to: parseUrl(banner) } : {};
  return (
    <Wrapper {...wrapperProps}>
      <Banner
        src={banner.image.url}
        alt={banner.title || banner.name || appName}
      />
    </Wrapper>
  );
}
