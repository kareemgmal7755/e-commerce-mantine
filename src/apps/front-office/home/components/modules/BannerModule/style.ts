import styled from "@emotion/styled";
import { Image } from "@mantine/core";

export const ImageBanner = styled<any>(Image)`
  label: ImageBanner;
  width: 100%;
  position: relative;
  transition: 1s all ease;
  &::before {
    background-color: ${({ theme }) => theme.colors.dark[9]};
    opacity: 0.3;
    content: "";
    width: 0;
    height: 100%;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 1;
    transition: 0.6s all ease;
  }
  &:hover::before {
    width: 100%;
  }
`;

export const ImageBanner4 = styled<any>(Image)`
  label: ImageBanner4;
  object-fit: cover;
  width: 100%;
  height: 100%;
  position: relative;
  overflow: hidden;
  &::before {
    background-image: ${({ theme }) =>
      theme.fn.gradient({ from: "dark.9", to: "gray.0", deg: 0.1 })};
    opacity: 0;
    content: "";
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    transform: translate3d(0, 50%, 0);
    z-index: 1;
    transition: opacity 0.2s, transform 0.2s;
  }
  &:hover::before {
    transform: translate3d(0, 0, 0);
    opacity: 0.2;
  }
`;

export const BannerStyleResponsive: any = {
  "@media (max-width: 1000px)": {
    width: "100%",
    gap: "1.2rem",
  },
};

export const HiddenBannerStyleResponsive: any = {
  "@media (max-width: 1000px)": {
    display: "none",
  },
};
