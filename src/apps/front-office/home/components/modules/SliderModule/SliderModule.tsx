import { Carousel } from "@mantine/carousel";
import { Box } from "@mantine/core";
import Banner from "apps/front-office/home/components/modules/BannerModule";
import Autoplay from "embla-carousel-autoplay";
import { useMemo, useRef } from "react";
import { Column } from "../../RowsList/types";

export default function SliderModule({ column }: { column: Column }) {
  const autoplay = useRef(Autoplay({ delay: 3000 }));

  const slides = useMemo(() => {
    return column.data?.slider?.banners.map((slide, index) => {
      return (
        <Carousel.Slide key={index}>
          <Banner banner={slide} />
        </Carousel.Slide>
      );
    });
  }, [column.data?.slider?.banners]);

  return (

      <Box>
        <Carousel
          mx="auto"
          withControls={false}
          withIndicators
          containScroll="trimSnaps"
          plugins={[autoplay.current]}
          onMouseEnter={autoplay.current.stop}
          onMouseLeave={autoplay.current.reset}
          loop>
          {slides}
        </Carousel>
      </Box>
  );
}
