export const sliderBannerStyles = theme => ({
  indicator: {
    ":hover": {
      backgroundColor: theme.colors[theme.primaryColor][6],
    },
    "&[data-active]": {
      backgroundColor: theme.colors[theme.primaryColor][6],
    },
  },
});
