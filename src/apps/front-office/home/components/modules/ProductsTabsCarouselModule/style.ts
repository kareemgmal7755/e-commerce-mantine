import styled from "@emotion/styled";
import { Flex, FlexProps } from "@mantine/core";
import { FC } from "react";

export const SimpleGridResponsive = [
  { maxWidth: 1000, cols: 2 },
  { maxWidth: 800, cols: 1 },
];

export const TabsFlex = styled(Flex)`
  label: TabsFlex;
  border-bottom: 0.125rem solid #dee2e6;
  h3 {
    width: fit-content;
    position: relative;
  }
  h3::after {
    content: "";
    position: absolute;
    height: 0.5px;
    bottom: -2%;
    width: 100%;
    background: ${({ theme }) => theme.colors[theme.primaryColor][5]};
    left: 0;
  }
` as FC<FlexProps>;

export const BestSellerResponsive = [
  { maxWidth: "sm", cols: 1 },
  { maxWidth: "lg", cols: 2 },
];

export const BestSellerCarousel: any = theme => ({
  control: {
    color: theme.colors.gray[5],
    svg: {
      width: "1.8rem",
      height: "1.8rem",
    },
  },
});
