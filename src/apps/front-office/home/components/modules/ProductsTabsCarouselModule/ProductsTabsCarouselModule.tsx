import { Box, Flex, Tabs } from "@mantine/core";
import { useMediaQuery } from "@mantine/hooks";
import { groupBy } from "@mongez/reinforcements";
import { IconBuildingStore } from "@tabler/icons-react";
import SectionTitle from "apps/front-office/design-system/components/SectionTitle";
import ProductsCarousel from "apps/front-office/store/components/ProductsCarousel/ProductsCarousel";
import MainTabs from "apps/front-office/store/components/Tabs/MainTabs";
import ResponsiveTabs from "apps/front-office/store/components/Tabs/ResponsiveTabs";
import { Column } from "../../RowsList/types";
import { TabsFlex } from "./style";

export default function ProductsTabsCarouselModule({
  column,
}: {
  column: Column;
}) {
  const responsive = useMediaQuery("(max-width:800px)");
  const categories = groupBy(column.data.products, "category.name").slice(0, 5);

  return (
    <Box my="1.5rem">
      <Tabs
        variant="outline"
        py={1}
        defaultValue={categories[0]["category.name"]}
        keepMounted={false}>
        <TabsFlex justify="space-between">
          <Tabs.List>
            <Flex justify="space-between">
              {column.title && (
                <SectionTitle title={column.title} subtitle={column.title} />
              )}
              {responsive ? (
                <ResponsiveTabs
                  data={categories.map(category => category["category.name"])}
                  Icon={IconBuildingStore}
                />
              ) : (
                <MainTabs
                  data={categories.map(category => category["category.name"])}
                />
              )}
            </Flex>
          </Tabs.List>
        </TabsFlex>
        {categories.map((category, index) => (
          <Tabs.Panel value={category["category.name"]} pt="xs" key={index}>
            <ProductsCarousel
              slidesToScroll="auto"
              products={category.data}
              slideSize={{ base: "100%", lg: "25%", md: "33.3%", sm: "50%" }}
              withControls={category.data.length > 4}
            />
          </Tabs.Panel>
        ))}
      </Tabs>
    </Box>
  );
}
