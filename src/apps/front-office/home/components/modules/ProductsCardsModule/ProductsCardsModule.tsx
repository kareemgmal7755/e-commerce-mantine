import { Flex, SimpleGrid } from "@mantine/core";
import SectionTitle from "apps/front-office/design-system/components/SectionTitle";
import ProductGridCard from "apps/front-office/store/components/ProductGridCard";
import { Column } from "../../RowsList/types";

function usePerRow(column: Column) {
  if (!column.settings?.perRow) {
    return {
      breakpoints: [
        {
          minWidth: "xs",
          cols: 1,
        },
        {
          minWidth: "md",
          cols: 4,
        },
      ],
    };
  }

  if (typeof column.settings?.perRow === "number") {
    return {
      cols: column.settings?.perRow,
    };
  }

  // the per row now will be something like this: xs: 2, sm: 3, md: 4, lg: 5, xl: 6
  // so we need to convert it into an array of objects
  return {
    breakpoints: Object.entries(column.settings?.perRow || {}).map(
      ([key, value]) => ({
        minWidth: key as string,
        cols: value as number,
      }),
    ),
  };
}

export default function ProductsCardsModule({ column }: { column: Column }) {
  return (
    <Flex direction="column" my="1.5rem" gap="1rem">
      {column.title && (
        <SectionTitle title={column.title} subtitle={column.title} />
      )}
      <SimpleGrid
        mt="md"
        my="2rem"
        spacing={10}
        {...usePerRow(column)}
        cols={{ base: 1, sm: 2, md: 4 }}>
        {column.data.products.map(product => (
          <ProductGridCard key={product.id} product={product} />
        ))}
      </SimpleGrid>
    </Flex>
  );
}
