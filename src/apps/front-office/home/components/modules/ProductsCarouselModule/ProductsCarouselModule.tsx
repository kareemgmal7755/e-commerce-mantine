import { Carousel } from "@mantine/carousel";
import { Flex } from "@mantine/core";
import Is from "@mongez/supportive-is";
import SectionTitle from "apps/front-office/design-system/components/SectionTitle";
import ProductGridCard from "apps/front-office/store/components/ProductGridCard";
import { Column } from "../../RowsList/types";

export default function ProductsCarouselModule({ column }: { column: Column }) {
  const products = column.data.products;

  if (Is.empty(products)) return null;

  return (
    <Flex direction="column" gap="1rem" my="1.5rem">
      {column.title && (
        <SectionTitle title={column.title} subtitle={column.title} />
      )}
      <Carousel
        slideSize={{ base: "100%", lg: "25%", md: "33.3%", sm: "50%" }}
        slidesToScroll="auto"
        withControls={false}
        slideGap="md"
        loop
        containScroll="trimSnaps">
        {products.map((product, index) => (
          <Carousel.Slide key={index}>
            <ProductGridCard product={product} />
          </Carousel.Slide>
        ))}
      </Carousel>
    </Flex>
  );
}
