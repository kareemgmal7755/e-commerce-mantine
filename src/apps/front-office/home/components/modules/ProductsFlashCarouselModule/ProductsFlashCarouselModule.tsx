import { Carousel } from "@mantine/carousel";
import SectionTitle from "apps/front-office/design-system/components/SectionTitle";
import DealOfeWeek from "apps/front-office/store/components/DealOfTheWeek/DealOfTheWeek";
import { Column } from "../../RowsList/types";

export default function ProductsFlashCarouselModule({
  column,
}: {
  column: Column;
}) {
  return (
    <>
      <SectionTitle title={column.title} />
      <Carousel containScroll="trimSnaps" loop slidesToScroll={1}>
        {column.data.products.map(product => (
          <Carousel.Slide key={product.id}>
            <DealOfeWeek product={product} />
          </Carousel.Slide>
        ))}
      </Carousel>
    </>
  );
}
