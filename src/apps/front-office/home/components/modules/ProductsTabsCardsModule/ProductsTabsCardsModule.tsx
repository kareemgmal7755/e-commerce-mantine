import { Box, Flex, SimpleGrid, Tabs } from "@mantine/core";
import { useMediaQuery } from "@mantine/hooks";
import { groupBy } from "@mongez/reinforcements";
import SectionTitle from "apps/front-office/design-system/components/SectionTitle";
import ProductGridCard from "apps/front-office/store/components/ProductGridCard";
import MainTabs from "apps/front-office/store/components/Tabs/MainTabs";
import { Column } from "../../RowsList/types";

export default function ProductsTabsCardsModule({
  column,
}: {
  column: Column;
}) {
  const responsive = useMediaQuery("(max-width:800px)");
  const categories = groupBy(column.data.products, "category.name").slice(0, 5);

  return (
    <Box
      my="1.5rem"
      style={{
        overflow: "hidden",
      }}>
      {column.title && (
        <Box my="1rem">
          <SectionTitle title={column.title} subtitle={column.title} />
        </Box>
      )}
      <Tabs
        variant="outline"
        defaultValue={categories[0]["category.name"]}
        keepMounted={false}>
        <Tabs.List justify="center" grow>
          <Flex justify="space-between">
            <SectionTitle title={column.settings.title} />
            <MainTabs
              data={categories.map(category => category["category.name"])}
            />
          </Flex>
        </Tabs.List>
        {categories.map((category, index) => (
          <Tabs.Panel value={category["category.name"]} pt="xs" key={index}>
            <SimpleGrid cols={{ base: 1, lg: 4, md: 3, sm: 2 }} spacing="md">
              {category.data.map((product, index) => (
                <ProductGridCard key={index} product={product} />
              ))}
            </SimpleGrid>
          </Tabs.Panel>
        ))}
      </Tabs>
    </Box>
  );
}
