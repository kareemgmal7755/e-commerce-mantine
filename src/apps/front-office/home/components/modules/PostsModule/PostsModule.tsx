import { Carousel } from "@mantine/carousel";
import { Box, Flex, Text, Title } from "@mantine/core";
import { readMoreChars } from "@mongez/reinforcements";
import SectionTitle from "apps/front-office/design-system/components/SectionTitle";
import Autoplay from "embla-carousel-autoplay";
import { useRef } from "react";

import { trans } from "@mongez/localization";
import { PrimaryButton } from "apps/front-office/design-system/components/Buttons/PrimaryButton";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import { Column } from "../../RowsList/types";

export default function PostsModule({ column }: { column: Column }) {
  const autoplay = useRef(Autoplay({ delay: 2000 }));
  const posts = column.data.posts;
  if (!posts) return;

  return (
    <Flex direction="column" gap="1rem" mb="2rem" w="100%" my="1.5rem">
      {column.title && (
        <SectionTitle title={column.title} subtitle={column.title} />
      )}
      <Carousel
        withIndicators
        loop
        slideSize={{ base: "100%", sm: "50%", md: "33.3%" }}
        slideGap={40}
        plugins={[autoplay.current]}
        onMouseEnter={autoplay.current.stop}
        onMouseLeave={autoplay.current.reset}
        slidesToScroll="auto"
        withControls={false}>
        {posts.map(post => (
          <Carousel.Slide key={post.id}>
            <Box h="15rem" pos="relative">
              <UnStyledLink to={URLS.blog.view(post)}>
                <img
                  src={post.image.url}
                  alt={post.title}
                  width="100%"
                  height="100%"
                />
              </UnStyledLink>
              <Text
                span
                pos="absolute"
                fz="0.7rem"
                fw={600}
                top="2%"
                left="2%"
                bg="gray.0"
                p="0.3rem"
                tt="uppercase">
                {post.createdAt.date}
              </Text>
            </Box>
            <Flex p="1rem" direction="column" h="12rem" justify="space-between">
              <UnStyledLink to={URLS.blog.view(post)} c="dark.9">
                <Title order={4}>{post.title}</Title>
              </UnStyledLink>
              <Text span>{readMoreChars(post.shortDescription, 80)}</Text>
              <UnStyledLink to={URLS.blog.view(post)}>
                <PrimaryButton>{trans("readMore")}</PrimaryButton>
              </UnStyledLink>
            </Flex>
          </Carousel.Slide>
        ))}
      </Carousel>
    </Flex>
  );
}
