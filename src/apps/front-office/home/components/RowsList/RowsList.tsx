import { Box, Container, Grid } from "@mantine/core";
import { useMediaQuery } from "@mantine/hooks";
import React, { useMemo } from "react";
import Column from "../Column";
import { Row } from "./types";

export type RowsListProps = {
  rows: Row[];
};

function _RowsList({ rows }: RowsListProps) {
  const isMobile = useMediaQuery("(max-width:1000px)");

  const rowsContent = useMemo(() => {
    return rows.map((row, index) => {
      const RowContainer = row.settings?.size === "full" ? Box : Container;
      const containerProps =
        row.settings?.size === "full"
          ? {
              ...(row.settings || {}),
            }
          : { size: "xl", p: 0, ...(row.settings || {}) };

      return (
        <RowContainer key={index} {...containerProps}>
          <Grid
            grow
            gutter="xl"
            mx={row.settings?.size === "full" ? 0 : isMobile ? "md" : "xs"}
            key={index}>
            {row.columns.map((column, columnIndex) => {
              if (
                row.settings?.size === "full" &&
                !column.settings?.p &&
                row.columns.length === 1
              ) {
                column.settings.p = 0;
              }

              return (
                <Column
                  columnIndex={columnIndex}
                  rowIndex={index}
                  key={columnIndex}
                  column={column}
                />
              );
            })}
          </Grid>
        </RowContainer>
      );
    });
  }, [rows, isMobile]);
  return <>{rowsContent}</>;
}

const RowsList = React.memo(_RowsList);

export default RowsList;
