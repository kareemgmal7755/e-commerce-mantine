export type Row = {
  columns: Column[];
  settings: any;
};

export type Column = {
  type?: string;
  data?: any;
  title?: string;
  settings?: any;
  rows?: Row[];
};
