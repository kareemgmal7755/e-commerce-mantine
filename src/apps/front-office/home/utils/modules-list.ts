import BannerModule from "../components/modules/BannerModule";
import BrandsModule from "../components/modules/BrandsModule";
import CategoriesCarouselModule from "../components/modules/CategoriesCarouselModule";
import PostsModule from "../components/modules/PostsModule";
import ProductsCardsModule from "../components/modules/ProductsCardsModule";
import ProductsCarouselModule from "../components/modules/ProductsCarouselModule";
import ProductsTabsCardsModule from "../components/modules/ProductsTabsCardsModule";
import ProductsTabsCarouselModule from "../components/modules/ProductsTabsCarouselModule";
import SliderModule from "../components/modules/SliderModule";

export const modulesList = {
  slider: SliderModule,
  banner: BannerModule,
  posts: PostsModule,
  productsCarousel: ProductsCarouselModule,
  brands: BrandsModule,
  categoriesCarousel: CategoriesCarouselModule,
  productsCards: ProductsCardsModule,
  // productsFlashCarousel: ProductsFlashCarouselModule,
  productsTabsCarousel: ProductsTabsCarouselModule,
  productsTabsCards: ProductsTabsCardsModule,
};
