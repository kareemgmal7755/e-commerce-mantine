import { Keywords } from "@mongez/localization";
import { Attachment } from "apps/front-office/design-system/components/Attachments";

export type SimpleCategory = {
  id: number;
  slug: string;
  name: string;
};

export type SimpleBrand = {
  id: number;
  slug: string;
  logo: Attachment;
  name: string;
};

export type DateTime = {
  timestamp?: number;
  humanTime?: string;
  format?: string;
  text?: string;
};

export type SimpleProduct = {
  id: number;
  slug: string;
  category: SimpleCategory;
  description?: string;
  brand: SimpleBrand;
  inStock?: boolean;
  hasDiscount?: boolean;
  sku?: string;
  model?: string;
  shortDescription: string;
  purchase: {
    maxQuantity: number;
    minQuantity: number;
  };
  discount?: {
    percentage: number;
    value: number;
    endsAt: number;
    amount?: number;
  };
  images: Attachment[];
  name: string;
  price: number;
  salePrice?: number;
  stock: {
    sold?: number;
    available: number;
    lowStockThreshold: number;
  };
  rating?: number;
  inWishlist?: boolean;
  inCart?: boolean;
  inCompare?: boolean;
  discountEndsAt?: DateTime;
};

export type Product = SimpleProduct & {
  id: number;
  name: string;
  slug?: string;
  price: number;
  color?: string;
  isActive?: boolean;
  reviews?: number;
  isLowStock?: boolean;
  quantity?: number;
  sortOrder?: number;
  sold?: number;
  dimensions?: {
    width?: number;
    height?: number;
    length?: number;
    weight?: number;
  };
  keywords?: Keywords[];

  description: string;
  inWishlist: boolean;
  specifications?: {
    value: string;
    label: string;
  }[];
  discount?: {
    amount: number;
    percentage: number;
  };
  stock: {
    available: number;
    lowStockThreshold: number;
  };
  sku: string;
  model?: string;
  brand: {
    id: number;
    name: string;
    logo: Attachment;
  };
  createdAt?: DateTime;
  updatedAt?: DateTime;
  category: {
    id: number;
    name: string;
    image: Attachment;
  };
  purchase?: {
    maxQuantity: number;
    minQuantity: number;
  };
  return?: {
    type?: string;
    duration?: number;
    isAllowed?: boolean;
  };
  warranty?: {
    type?: string;
    duration?: number;
  };
  salePrice: number;
  coolingType?: string;
  images: Attachment[];
  rating?: number;
  similarProducts?: Product[];
  relatedProducts?: Product[];
  ratingsList: {
    count: number;
    rating: number;
  }[];
};

export type PaginationInfo = {
  results: number;
  page: number;
  limit: number;
  total: number;
  pages: number;
};
