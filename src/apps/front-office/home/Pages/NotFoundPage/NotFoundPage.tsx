import { Flex, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { navigateBack } from "@mongez/react-router";
import { useMedia } from "apps/front-office/design-system/components";
import { PrimaryButton } from "apps/front-office/design-system/components/Buttons/PrimaryButton";
import { RhinoButton } from "apps/front-office/design-system/components/Buttons/RhinoButton";
import Container from "apps/front-office/design-system/components/Container";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import URLS from "apps/front-office/utils/urls";
import notFound from "assets/images/404.gif";

export default function NotFoundPage() {
  const isMobile = useMedia(800);

  return (
    <Container>
      <Flex
        h="100%"
        ta="center"
        justify="center"
        align="center"
        gap="1rem"
        py="4rem"
        direction="column">
        <img
          src={notFound}
          alt={trans("notFound")}
          width={isMobile ? "100%" : 450}
        />
        <Text component="p" fz="1.5rem" fw={700}>
          {trans("pageNotFound")}
        </Text>
        <Text component="p" fz="1rem" fw={700}>
          {trans("pageLookingFor")}
        </Text>
        <Flex gap="1rem" align="center" direction={isMobile ? "column" : "row"}>
          <UnStyledLink to={URLS.home}>
            <PrimaryButton w="13rem" h="3.2rem" onClick={navigateBack}>
              {trans("goBackHome")}
            </PrimaryButton>
          </UnStyledLink>

          <UnStyledLink to={URLS.shop.list}>
            <RhinoButton h="3.2rem" w="13rem">
              {trans("continueShopping")}
            </RhinoButton>
          </UnStyledLink>
        </Flex>
      </Flex>
    </Container>
  );
}
