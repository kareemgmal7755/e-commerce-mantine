import { preload } from "@mongez/react-utils";
import RowsList from "apps/front-office/home/components/RowsList";
import { AxiosResponse } from "axios";
import { getHome } from "../../services/home-service";

export type HomeProps = {
  response: AxiosResponse;
};

function _HomePage({ response }: HomeProps) {
  const rows = response.data.rows;

  return (
    <>
      <RowsList rows={rows} />
    </>
  );
}

const HomePage = preload(_HomePage, getHome);
export default HomePage;
