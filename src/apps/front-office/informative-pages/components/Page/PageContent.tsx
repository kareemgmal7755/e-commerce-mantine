import { Box, Text, Title } from "@mantine/core";
import Helmet from "@mongez/react-helmet";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import Container from "apps/front-office/design-system/components/Container";
import { Page } from "../../atoms";
import HTML from "apps/front-office/design-system/components/HTML";

export default function PageContent({ page }: { page: Page }) {
  const pageItems = [{ text: page.title, url: `/${page.name}` }];

  return (
    <>
      <Breadcrumb title={page.title} items={pageItems} />
      <Helmet title={page.title} />
      <Box my="2rem">
        <Container>
          <Title ta="center" order={3}>{page.title}</Title>
          <Text fw={600}>
            <HTML html={page.description} />
          </Text>
        </Container>
      </Box>
    </>
  );
}
