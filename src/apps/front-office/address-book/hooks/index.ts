import { trans } from "@mongez/localization";

import {
  toastError,
  toastSuccess,
} from "apps/front-office/design-system/utils/toast";
import parseError from "apps/front-office/utils/parse-error";
import { addressesAtom } from "../atom";
import {
  addAddress,
  deleteAddress,
  editAddress,
  markAsPrimaryAddress,
} from "../services/address-book-service";

function addAddressForm(onSuccess?: any) {
  return ({ values, form }) => {
    addAddress(values)
      .then(response => {
        addressesAtom.update(response.data.addresses);
        toastSuccess(trans("addressAddedSuccessfully"));
        onSuccess?.(response);
      })
      .catch(error => {
        toastError(parseError(error));
        form.submitting(false);
      });
  };
}

function editAddressForm(addressId: number, onSuccess: any) {
  return ({ form, values }) => {
    editAddress(addressId, values)
      .then(response => {
        toastSuccess(trans("addressUpdatedSuccessfully"));
        addressesAtom.update(response.data.addresses);
        onSuccess(response);
      })
      .catch(error => {
        toastError(parseError(error));
        form.submitting(false);
      });
  };
}

async function deleteTheGivenAddress(addressId: number) {
  try {
    deleteAddress(addressId)
      .then(() => {
        const newAddresses = addressesAtom.value.filter(
          address => address.id !== addressId,
        );
        addressesAtom.update(newAddresses);
        toastSuccess(trans("addressDeletedSuccessfully"));
      })
      .catch(error => {
        toastError(parseError(error));
      });
  } catch (error) {
    toastError(parseError(error));
  }
}

function markAsPrimary(address: any) {
  markAsPrimaryAddress(address.id)
    .then(response => {
      addressesAtom.update(response.data.addresses);
      toastSuccess("success");
    })
    .catch(error => {
      toastError(parseError(error));
    });
}

export function useAddresses() {
  return {
    addAddress: addAddressForm,
    editAddress: editAddressForm,
    deleteAddress: deleteTheGivenAddress,
    markAsPrimary,
  };
}
