import { Divider, Flex, SimpleGrid, Switch, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { preload } from "@mongez/react-utils";
import { TertiaryButton } from "apps/front-office/design-system/components/Buttons/TertiaryButton";
import React, { useState } from "react";
import { addressesAtom } from "../../atom";
import { useAddresses } from "../../hooks";
import { getAddressBook } from "../../services/address-book-service";
import EditAddress from "../EditAddress";
import style from "../style.module.scss";

function _AddressesList() {
  const addresses = addressesAtom.useValue();
  const [selectedAddress, setAddress] = useState(
    addresses.find(address => address.isPrimary)?.id,
  );
  const { deleteAddress, markAsPrimary } = useAddresses();

  return (
    <SimpleGrid cols={{ base: 1, sm: 2, lg: 3 }} mt="1.5rem">
      {addresses.map(address => (
        <Flex
          className={
            selectedAddress === address.id ? style.active_address_wrapper : style.address_wrapper
          }
          p="1rem"
          w="100%"
          direction="column"
          gap="0.5rem"
          key={address.id}>
          <Flex justify="space-between" align="center">
            <Text component="p" fw={600}>
              {address.name}
            </Text>
            <Switch
              name="useShippingAddress"
              color="rhino.9"
              label={trans(
                address.isPrimary ? "primaryAddress" : "makePrimary",
              )}
              checked={selectedAddress === address.id}
              onChange={() => {
                markAsPrimary(address);
                setAddress(address.id);
              }}
            />
          </Flex>
          <Text mt="xs" c="dimmed" size="sm" component="p">
            {address.address}, {address.district?.name},
            {address.district?.city?.name}
          </Text>
          <Text component="p" fz="0.9rem">
            {trans("phoneNumber")}: {address.phoneNumber}
          </Text>
          <Divider />
          <Flex justify="space-between" align="center" py="0.5rem">
            <EditAddress address={address} />
            <TertiaryButton onClick={() => deleteAddress(address.id)}>
              {trans("remove")}
            </TertiaryButton>
          </Flex>
        </Flex>
      ))}
    </SimpleGrid>
  );
}

const AddressesList = React.memo(
  preload(_AddressesList, getAddressBook, {
    onSuccess(response) {
      addressesAtom.update(response.data.addresses);
    },
  }),
);
export default AddressesList;
