import { Grid, Modal } from "@mantine/core";
import { trans } from "@mongez/localization";

import { useDisclosure } from "@mantine/hooks";
import { Form } from "@mongez/react-form";
import user from "apps/front-office/account/user";
import { PrimaryButton } from "apps/front-office/design-system/components/Buttons/PrimaryButton";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import SwitchInput from "apps/front-office/design-system/components/Form/CheckBox/SwitchInput";
import EmailInput from "apps/front-office/design-system/components/Form/EmailInput";
import TextInput from "apps/front-office/design-system/components/Form/TextInput";
import { AxiosResponse } from "axios";
import { useAddresses } from "../../hooks";

export type AddAddressProps = {
  onSave?: (response: AxiosResponse) => void;
};

export default function AddAddress({ onSave }: AddAddressProps) {
  const { addAddress } = useAddresses();
  const [opened, { open, close }] = useDisclosure(false);

  return (
    <>
      <PrimaryButton onClick={open} size="sm">
        + {trans("addNewAddress")}
      </PrimaryButton>
      <Modal
        size="lg"
        trapFocus={false}
        title={<strong>{trans("addNewAddress")}</strong>}
        onClose={close}
        opened={opened}>
        <Form
          onSubmit={addAddress(response => {
            onSave?.(response);
            close();
          })}>
          <Grid>
            <Grid.Col span={6}>
              <TextInput
                name="name"
                autoFocus
                label={trans("name")}
                placeholder={trans("name")}
                defaultValue={user.get("name")}
                required
              />
            </Grid.Col>
            <Grid.Col span={6}>
              <TextInput
                name="phoneNumber"
                label={trans("phoneNumber")}
                placeholder={trans("phoneNumber")}
                defaultValue={user.get("phoneNumber")}
                required
              />
            </Grid.Col>
            <Grid.Col span={6}>
              <EmailInput
                name="email"
                required
                label={trans("email")}
                placeholder={trans("email")}
                defaultValue={user.get("email")}
              />
            </Grid.Col>
            <Grid.Col span={6}>
              <TextInput
                name="landmark"
                label={trans("landmark")}
                placeholder={trans("landmark")}
              />
            </Grid.Col>
            <Grid.Col span={6}>
              <TextInput
                name="label"
                label={trans("label")}
                placeholder={trans("addressLabelHint")}
                required
              />
            </Grid.Col>
            <Grid.Col span={6}>
              <TextInput
                name="address"
                label={trans("address")}
                placeholder={trans("address")}
                required
              />
            </Grid.Col>
            <Grid.Col span={6}>
              <SwitchInput name="isPrimary" label={trans("makePrimary")} />
            </Grid.Col>
          </Grid>
          <SubmitButton mt="xl">{trans("create")}</SubmitButton>
        </Form>
      </Modal>
    </>
  );
}
