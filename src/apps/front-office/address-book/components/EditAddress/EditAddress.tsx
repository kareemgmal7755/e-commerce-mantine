import { Grid, Modal, Switch, Title } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { trans } from "@mongez/localization";
import { Form } from "@mongez/react-form";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import { TertiaryButton } from "apps/front-office/design-system/components/Buttons/TertiaryButton";
import CitiesSelectInput from "apps/front-office/design-system/components/CitiesSelectInput";
import DistrictSelectInput from "apps/front-office/design-system/components/DistrictSelectInput";
import EmailInput from "apps/front-office/design-system/components/Form/EmailInput";
import TextInput from "apps/front-office/design-system/components/Form/TextInput";
import { useState } from "react";
import { addressesAtom } from "../../atom";
import { useAddresses } from "../../hooks";
import { Address } from "../../types";
import SwitchInput from "apps/front-office/design-system/components/Form/CheckBox/SwitchInput";

export default function EditAddress({ address }: { address: Address }) {
  const addresses = addressesAtom.useValue();
  const { editAddress } = useAddresses();
  const [opened, { open, close }] = useDisclosure(false);
  const [selectedAddress, setAddress] = useState(
    addresses.find(address => address.isPrimary)?.id,
  );

  return (
    <>
      <TertiaryButton onClick={open}>{trans("edit")}</TertiaryButton>
      <Modal
        onClose={close}
        opened={opened}
        size="lg"
        title={<Title order={3}>{trans("editAddress")}</Title>}>
        <Form onSubmit={editAddress(address.id, close)}>
          <Grid>
            <Grid.Col span={6}>
              <TextInput
                name="name"
                autoFocus
                label={trans("name")}
                placeholder={trans("name")}
                defaultValue={address.name}
                required
              />
            </Grid.Col>
            <Grid.Col span={6}>
              <TextInput
                name="phoneNumber"
                label={trans("phoneNumber")}
                placeholder={trans("phoneNumber")}
                defaultValue={address.phoneNumber}
                required
              />
            </Grid.Col>
            <Grid.Col span={6}>
              <EmailInput
                name="email"
                required
                label={trans("email")}
                placeholder={trans("email")}
                defaultValue={address.email}
              />
            </Grid.Col>
            <Grid.Col span={6}>
              <CitiesSelectInput />
            </Grid.Col>
            <Grid.Col span={6}>
              <DistrictSelectInput />
            </Grid.Col>
            <Grid.Col span={6}>
              <TextInput
                name="landmark"
                label={trans("landmark")}
                placeholder={trans("landmark")}
              />
            </Grid.Col>
            <Grid.Col span={6}>
              <TextInput
                name="address"
                label={trans("address")}
                placeholder={trans("address")}
                defaultValue={address.address}
                required
              />
            </Grid.Col>
            <Grid.Col span={6}>
              <TextInput
                name="label"
                label={trans("label")}
                placeholder={trans("addressLabelHint")}
                defaultValue={address.label}
                required
              />
            </Grid.Col>
            <Grid.Col span={6}>
              <SwitchInput
                name="isPrimary"
                color="rhino.9"
                defaultChecked={address.id === selectedAddress ? true : false}
                label={trans("makePrimary")}
                onClick={() => setAddress(address.id)}
              />
            </Grid.Col>
          </Grid>
          <SubmitButton mt="xl">{trans("save")}</SubmitButton>
        </Form>
      </Modal>
    </>
  );
}
