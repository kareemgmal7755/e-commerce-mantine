import endpoint from "shared/endpoint";

export function getAddressBook() {
  return endpoint.get("/addresses");
}

export function addAddress(data: any) {
  return endpoint.post("/addresses", data);
}

export function editAddress(id: number, data: any) {
  return endpoint.put(`/addresses/${id}}`, data);
}

export function deleteAddress(id: number) {
  return endpoint.delete(`/addresses/${id}}`);
}

export function markAsPrimaryAddress(id: number) {
  return endpoint.patch(`/addresses/${id}/set-primary`);
}
