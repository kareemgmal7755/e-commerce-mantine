export type Address = {
  id: number;
  title: string;
  address: string;
  name: string;
  firstName: string;
  lastName: string;
  email?: string;
  isPrimary: boolean;
  phoneNumber: string;
  label: string;
  district: {
    id: number;
    name: string;
    city?: {
      id: number;
      name: string;
      region: {
        country: {
          name: string;
        };
      };
    };
  };
};

export type AddressProps = {
  address: Address;
};
