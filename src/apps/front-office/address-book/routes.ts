import { accountRoutes } from "../utils/router";
import URLS from "../utils/urls";
import AddressesPage from "./pages/AddressesPage";

accountRoutes([
  {
    path: URLS.account.address,
    component: AddressesPage,
  },
]);
