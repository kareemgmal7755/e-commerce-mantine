import { atom } from "@mongez/react-atom";
import { Address } from "../types";

export type AddressProps = {
  address: Address;
};

export const addressesAtom = atom<Address[]>({
  key: "addresses",
  default: [],
});
