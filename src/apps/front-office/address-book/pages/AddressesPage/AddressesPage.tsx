import { Flex, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import AddAddress from "../../components/AddAddress";
import AddressesList from "../../components/AddressesList";

export default function AddressesPage() {
  return (
    <>
      <Helmet title={trans("addressBook")} />
      <Flex justify="space-between" align="center">
        <Title order={3} c="dark.9">
          {trans("addressBook")}
        </Title>
        <AddAddress />
      </Flex>
      <AddressesList />
    </>
  );
}
