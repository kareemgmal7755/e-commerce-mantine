import endpoint from "shared/endpoint";

export function getWishlist() {
  return endpoint.get("/wishlist");
}

export function addToWishlist(productId: number) {
  return endpoint.post(`/wishlist/${productId}`);
}

export function removeFromWishlist(productId: number) {
  return endpoint.delete(`/wishlist/${productId}`);
}
