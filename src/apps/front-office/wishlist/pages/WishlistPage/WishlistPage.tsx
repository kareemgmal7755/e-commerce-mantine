import { SimpleGrid } from "@mantine/core";
import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import { useEvent } from "@mongez/react-hooks";
import { preload } from "@mongez/react-utils";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { wishlistItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import Container from "apps/front-office/design-system/components/Container";
import { Product } from "apps/front-office/home/utils/types";
import NoProductsFound from "apps/front-office/store/components/NoProductsFound";
import ProductWishlistCard from "apps/front-office/store/components/ProductWishlistCard";
import { wishlistEvents } from "apps/front-office/store/events/wishlist";
import { useState } from "react";
import { getWishlist } from "../../services/wishlist-services";

function _WishlistPage({ response }) {
  const [products, setProducts] = useState(response.data.products);

  useEvent(() =>
    wishlistEvents.onRemove(productId => {
      setProducts(products =>
        products.filter((product: Product) => product.id !== productId),
      );
    }),
  );

  return (
    <>
      <Helmet title={trans("wishlist")} />
      <Breadcrumb title={trans("wishlist")} items={wishlistItems} />
      {products.length === 0 && <NoProductsFound text="noProductsWishlist" />}
      <Container>
        <SimpleGrid cols={{ base: 1, md: 4, sm: 2 }} my="2rem" spacing={70}>
          {products.map(product => (
            <ProductWishlistCard product={product} key={product.id} />
          ))}
        </SimpleGrid>
      </Container>
    </>
  );
}

const WishlistPage = preload(_WishlistPage, getWishlist);
export default WishlistPage;
