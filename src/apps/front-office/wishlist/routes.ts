import { publicRoutes } from "../utils/router";
import URLS from "../utils/urls";
import WishlistPage from "./pages/WishlistPage";

publicRoutes([
  {
    path: URLS.wishlist,
    component: WishlistPage,
  },
]);
