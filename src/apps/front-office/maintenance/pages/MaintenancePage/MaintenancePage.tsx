import { Flex, Image, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import maintenance from "assets/images/maintenance.gif";

export default function MaintenancePage() {
  return (
    <Flex gap={20} align="center" justify="center" direction="column" h="100vh">
      <Image width={350} src={maintenance} />
      <Title order={2} fz="1.8rem" ta="center" c="rhino.9">
        {trans("websiteUnderMaintenance")}
      </Title>
    </Flex>
  );
}
