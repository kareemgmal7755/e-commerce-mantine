import router from "@mongez/react-router";
import URLS from "../utils/urls";
import MaintenancePage from "./pages/MaintenancePage/MaintenancePage";

router.add(URLS.maintenance, MaintenancePage);
