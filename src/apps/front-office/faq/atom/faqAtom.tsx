import { atom } from "@mongez/react-atom";

type FAQ = {
  id: number;
  title: string;
  content: string;
};

export const faqAtom = atom<FAQ[]>({
  key: "faq",
  default: [],
});
