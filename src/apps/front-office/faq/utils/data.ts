import {
  IconBus,
  IconCreditCard,
  IconTruckDelivery,
  IconUserCircle,
} from "@tabler/icons-react";

export const faqData = [
  {
    icon: IconTruckDelivery,
    text: "shippingOrders",
  },
  {
    icon: IconBus,
    text: "exchangesReturns",
  },
  {
    icon: IconCreditCard,
    text: "paymentsPrivacy",
  },
  {
    icon: IconUserCircle,
    text: "accountSettings",
  },
];
