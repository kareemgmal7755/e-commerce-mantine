import { Accordion, Box, Flex, Grid, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { IconPlus } from "@tabler/icons-react";
import { useMedia } from "apps/front-office/design-system/components";
import Container from "apps/front-office/design-system/components/Container";
import { faqAtom } from "../../atom/faqAtom";
import { faqData } from "../../utils/data";

const getNumber = number => {
  return number > 9 ? number : `0${number}`;
};

export default function FaqContent() {
  const media = useMedia(1000);
  const faq = faqAtom.useValue();
  if (!faq) return;

  return (
    <Box my="1.5rem">
      <Container>
        <Flex
          gap="2rem"
          direction="column"
          m="auto"
          align="center"
          justify="center">
          <Title order={3} ta="center">
            {trans("frequently")}
          </Title>
          <Grid justify="space-between" align="center" w="100%" m="auto">
            {faqData.map((item, index) => (
              <Grid.Col
                span={{ base: 6, md: 3, sm: 6 }}
                ta="center"
                key={index}>
                <Flex gap="0.2rem" align="end" justify={!media ? "center" : ""}>
                  <item.icon size={25} stroke={1.5} />
                  <Text span fw={600} fz="md">
                    {trans(item.text)}
                  </Text>
                </Flex>
              </Grid.Col>
            ))}
          </Grid>
          <Accordion
            radius={0}
            w="100%"
            defaultValue={"0"}
            chevron={<IconPlus size={20} />}
            variant="separated">
            {faq.map((question, index) => {
              return (
                <Accordion.Item key={question.id} value={String(index)}>
                  <Accordion.Control>
                    <Text component="p" fw={600} fz="md">
                      {" "}
                      {question.title}
                    </Text>
                  </Accordion.Control>
                  <Accordion.Panel style={{ margin: 0 }}>
                    <Text fz="sm" fw="500">
                      {question.content}
                    </Text>
                  </Accordion.Panel>
                </Accordion.Item>
              );
            })}
          </Accordion>
        </Flex>
      </Container>
    </Box>
  );
}
