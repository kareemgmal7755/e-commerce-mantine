import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import { preload } from "@mongez/react-utils";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { faqItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import { faqAtom } from "../../atom/faqAtom";
import { getFAQ } from "../../services/services";
import FaqContent from "./FaqContent";

function FaqPageContent() {
  return (
    <>
      <Helmet title={trans("faq")} />
      <Breadcrumb items={faqItems} title="faq" />
      <FaqContent />
    </>
  );
}

const FaqPage = preload(FaqPageContent, getFAQ, {
  onSuccess: response => {
    faqAtom.update(response.data.faq);
  },
});

export default FaqPage;
