import URLS from "apps/front-office/utils/urls";
import { publicRoutes } from "../utils/router";
import ContactUsPage from "./pages/ContactUsPage";

publicRoutes([
  {
    path: URLS.contactUs,
    component: ContactUsPage,
  },
]);
