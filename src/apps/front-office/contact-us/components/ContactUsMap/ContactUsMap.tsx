import { GoogleMap } from "@mongez/react-google-map";
import { settingsAtom } from "apps/front-office/common/atoms";

export default function ContactMap() {
  const location = settingsAtom.use("contact").location;

  if (!location) return null;

  return <GoogleMap zoom={15} center={location} />;
}