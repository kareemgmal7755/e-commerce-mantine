import { Flex, Grid, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { Form } from "@mongez/react-form";
import user from "apps/front-office/account/user";
import { useContactForm } from "apps/front-office/contact-us/hooks";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import EmailInput from "apps/front-office/design-system/components/Form/EmailInput";
import TextAreaInput from "apps/front-office/design-system/components/Form/TextAreaInput";
import TextInput from "apps/front-office/design-system/components/Form/TextInput";

export default function ContactForm() {
  const submitContactForm = useContactForm();
  return (
    <Flex direction="column" gap="1.2rem">
      <Title order={2} ta="center" tt="uppercase">
        {trans("getInTouch")}
      </Title>
      <Text component="p" ta="center" c="gray.5">
        {trans("canContactUs")}
      </Text>
      <Form onSubmit={submitContactForm}>
        <Grid>
          <Grid.Col span={{ base: 12, md: 6 }}>
            <TextInput
              placeholder={trans("name")}
              name="name"
              defaultValue={user.get("name")}
              autoFocus
              required
            />
          </Grid.Col>
          <Grid.Col span={{ base: 12, md: 6 }}>
            <EmailInput
              placeholder={trans("email")}
              name="email"
              required
              defaultValue={user.get("email")}
            />
          </Grid.Col>
          <Grid.Col span={{ base: 12, md: 6 }}>
            <TextInput
              placeholder={trans("phoneNumber")}
              name="phoneNumber"
              defaultValue={user.get("phoneNumber")}
              required
            />
          </Grid.Col>
          <Grid.Col span={{ base: 12, md: 6 }}>
            <TextInput placeholder={trans("subject")} name="subject" required />
          </Grid.Col>
          <Grid.Col span={12}>
            <TextAreaInput
              placeholder={trans("message")}
              minRows={6}
              name="message"
              required
            />
          </Grid.Col>
        </Grid>

        <SubmitButton size="md" fz="1rem" mt="1rem" fw={600}>
          {trans("sendMessage")}
        </SubmitButton>
      </Form>
    </Flex>
  );
}
