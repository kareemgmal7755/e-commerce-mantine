import { Divider, Flex, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import {
  IconBrandFacebook,
  IconBrandInstagram,
  IconBrandTwitter,
  IconBrandWhatsapp,
} from "@tabler/icons-react";
import { settingsAtom } from "apps/front-office/common/atoms";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";

export default function StoreInformation() {
  const contactInfo = settingsAtom.use("contact");
  const socialMedia = settingsAtom.get("social");

  return (
    <Flex bg="aquaHaze.0" p="1rem" direction="column" gap="1rem">
      <Title order={4}>{trans("aboutUs")}</Title>
      <Flex direction="column" gap="0.5rem">
        <Flex gap="0.3rem" wrap="wrap" align="flex-end">
          <Text span fw={600} fz="0.95rem">
            {trans("address")}:
          </Text>
          <Text span fz="0.9rem">
            {contactInfo.address}
          </Text>
        </Flex>
        <Flex gap="0.3rem" wrap="wrap" align="center">
          <Text span fw={600} fz="0.95rem">
            {trans("phone")}:
          </Text>
          <Text span fz="0.9rem">
            {contactInfo.phoneNumber}
          </Text>
        </Flex>
        <Flex gap="0.3rem" wrap="wrap" align="center">
          <Text span fw={600} fz="0.95rem">
            {trans("email")}:
          </Text>
          <Text span fz="0.9rem">
            {contactInfo.email}
          </Text>
        </Flex>
      </Flex>
      <Divider />
      <Flex direction="column" gap="0.5rem">
        <Text span fw={600} fz="0.95rem">
          {trans("stayConnected")}
        </Text>
        <Flex gap="md">
          <UnStyledLink href={socialMedia.facebook} c="gray.0">
            <IconBrandFacebook color="#3a5173" size={20} />
          </UnStyledLink>
          <UnStyledLink href={socialMedia.instagram} c="gray.0">
            <IconBrandInstagram color="#3a5173" size={20} />
          </UnStyledLink>
          <UnStyledLink href={socialMedia.twitter} c="gray.0">
            <IconBrandTwitter color="#3a5173" size={20} />
          </UnStyledLink>
          <UnStyledLink
            href={`https://wa.me/${contactInfo.whatsappNumber}`}
            c="gray.0">
            <IconBrandWhatsapp color="#3a5173" size={20} />
          </UnStyledLink>
        </Flex>
      </Flex>
    </Flex>
  );
}
