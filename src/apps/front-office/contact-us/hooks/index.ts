import { trans } from "@mongez/localization";
import parseError from "apps/front-office/utils/parse-error";
import { sendContactMessage } from "../services/contact-us-service";
import { toastError, toastSuccess } from "apps/front-office/design-system/utils/toast";

export function useContactForm() {
  return ({ values, form }) => {
    sendContactMessage(values)
      .then(() => {
        form.reset();
        toastSuccess(trans("MessageSent"));
      })
      .catch(error => {
        toastError(parseError(error));
      })
      .finally(() => {
        form.submitting(false);
      });
  };
}
