import { Box, Grid } from "@mantine/core";
import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { contactUsItems } from "apps/front-office/design-system/components/Breadcrumb/data";
import Container from "apps/front-office/design-system/components/Container";
import ContactForm from "../../components/ContactUsForm/ContactUsForm";
import StoreInformation from "../../components/StoreInformation";
import ContactUsMap from "../../components/ContactUsMap";

export default function ContactUsPage() {
  return (
    <>
      <Helmet title={trans("contactUs")} />
      <Breadcrumb title="contactUs" items={contactUsItems} />
      <Box my="3rem">
        <Container>
          <Grid gutter={40} my="2rem">
            <Grid.Col span={{ base: 12, md: 8 }}>
              <ContactForm />
            </Grid.Col>
            <Grid.Col span={{ base: 12, md: 4 }}>
              <StoreInformation />
            </Grid.Col>
          </Grid>
          <ContactUsMap />
        </Container>
      </Box>
    </>
  );
}
