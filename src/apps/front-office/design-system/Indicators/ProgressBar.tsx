import { Box, Flex } from "@mantine/core";
import { useMedia } from "../components";
import Container from "../components/Container";
import style from "./style.module.scss";

/**
 * Progress bar is used for lazy loading for modules
 */
export default function ProgressBar() {
  const media = useMedia(800);

  return (
    <Container>
      <Flex
        w={media ? "100%" : "50%"}
        h="100vh"
        justify="center"
        align="center"
        m="auto">
        <Box className={style.loader} w="100%">
          <Box className={style.loader_box}>
            <Box className={style.loader_line}></Box>
          </Box>
        </Box>
      </Flex>
    </Container>
  );
}
