import { atom } from "@mongez/react-atom";

export const cityAtom = atom({
  key: "city",
  default: "",
});

export const districtAtom = atom({
  key: "district",
  default: "",
});

export const genderAtom = atom<string | null>({
  key: "gender",
  default: "",
});
