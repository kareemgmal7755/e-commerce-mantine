import { atom } from "@mongez/react-atom";

export type Breadcrumb = {
  text: string;
  url: string;
};

// Metadata is fetched from the api using the onComplete event
// @see src/shared/endpoint.ts onComplete event
export const breadcrumbAtom = atom<Breadcrumb[]>({
  key: "breadcrumb",
  default: [],
});
