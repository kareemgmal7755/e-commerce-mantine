import { Key } from "react";

export type Attachment = {
  id: Key | null | undefined;
  extension: string;
  url: string;
  fileName: string;
  mimeType: string;
  relativePath: string;
  hash: string;
  size: number;
  isHeightBiggerThanWidth?: boolean;
  dimensions: {
    width: number;
    height: number;
  };
};

export function humanSize(bytes: number) {
  const sizes = ["Bytes", "KB", "MB", "GB", "TB"];

  const i = Math.floor(Math.log(bytes) / Math.log(1024));
  return `${(bytes / 1024 ** i).toFixed(2)} ${sizes[i]}`;
}
