import {
  Avatar as BaseAvatar,
  AvatarProps as BaseAvatarProps,
} from "@mantine/core";
import { forwardRef } from "react";
import { Attachment } from "../Attachments";

export type ImageProps = Omit<
  React.ImgHTMLAttributes<HTMLImageElement>,
  "src"
> & {
  src: string | Attachment;
  w?: number;
  h?: number;
};

function _Image({ src, style, w, h, ...props }: ImageProps, ref: any) {
  // const [loaded, setLoaded] = useState(false);

  // const placeholderImage =
  //   settingsAtom.use("general").placeholderImage?.url || placeholder;

  if (typeof src === "object") {
    src = (src as any).url;
  }

  if (w || h) {
    src += "?";
    if (w) {
      src += `w=${w}`;
    }
    if (h) {
      src += `&h=${h}`;
    }
  }

  return (
    <img
      src={src as string}
      {...props}
      style={{
        ...style,
        maxWidth: "100%",
      }}
      ref={ref}
      {...props}
    />
  );
  // const loadStyles = loaded ? {} : { display: "none" };

  // return (
  //   <>
  //     {!loaded && (
  //       <img
  //         src={placeholderImage}
  //         alt={props.alt}
  //         style={{
  //           opacity: 0.5,
  //           maxWidth: "100%",
  //           maxHeight: "100%",
  //         }}
  //       />
  //     )}
  //     <img
  //       src={src as string}
  //       {...props}
  //       title={props.title || props.alt}
  //       ref={ref}
  //       onLoad={() => setLoaded(true)}
  //       style={{
  //         ...style,
  //         maxWidth: "100%",
  //         ...loadStyles,
  //       }}
  //     />
  //   </>
  // );
}

const Image = forwardRef(_Image);

export default Image;

export type AvatarProps = Omit<BaseAvatarProps, "src"> & {
  src: string | Attachment;
};

export function Avatar({ src, ...props }: AvatarProps) {
  if (typeof src === "object") {
    src = (src as any).url;
  }

  return <BaseAvatar src={src as string} {...props} />;
}
