import { Select } from "@mantine/core";
import { trans } from "@mongez/localization";
import { useOnce } from "@mongez/react-hooks";
import { getDistricts } from "apps/front-office/utils/services";
import { useState } from "react";
import { districtAtom } from "../../atoms/select-atom";

export default function DistrictSelectInput() {
  const [districts, setDistricts] = useState<any>([]);

  useOnce(() => {
    getDistricts().then(response => setDistricts(response.data.districts));
  });

  return (
    <Select
      radius={0}
      placeholder={trans("district")}
      label={trans("district")}
      data={districts.map(district => ({
        label: district.name,
        value: String(district.id),
      }))}
      onChange={value => districtAtom.update(String(value))}
    />
  );
}
