import { Anchor, AnchorProps } from "@mantine/core";
import { Link, LinkProps } from "@mongez/react-router";

export function BaseLink(props: AnchorProps & LinkProps) {
  return <Anchor component={Link as any} {...props} />;
}

export default BaseLink;
