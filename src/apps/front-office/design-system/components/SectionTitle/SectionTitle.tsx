import { Flex, Text, Title } from "@mantine/core";

export default function SectionTitle({
  title,
  subtitle,
}: {
  title: string | undefined;
  subtitle?: string;
}) {
  return (
    <Flex direction="column" gap="0.3rem" align="center" ta="center">
      <Text span c="gray.5" fz="lg">
        {subtitle}
      </Text>
      <Title order={2} tt="uppercase">{title}</Title>
    </Flex>
  );
}
