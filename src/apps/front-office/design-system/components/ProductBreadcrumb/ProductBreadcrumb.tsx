import { Box, Breadcrumbs, Flex } from "@mantine/core";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import { useEvent } from "@mongez/react-hooks";
import { routerEvents } from "@mongez/react-router";
import { IconChevronLeft, IconChevronRight } from "@tabler/icons-react";
import { breadcrumbAtom } from "../../atoms/breadcrumb-atom";
import Container from "../Container";
import { UnStyledLink } from "../Link";

export default function ProductBreadcrumb() {
  const breadcrumbs = breadcrumbAtom.useValue();

  useEvent(() =>
    routerEvents.onNavigating(() => {
      breadcrumbAtom.update([]);
    }),
  );

  return (
    <Box py="1.5rem" bg="#F3F3F3">
      <Container>
        <Breadcrumbs
          separator={
            current("localeCode") === "en" ? (
              <IconChevronRight size={13} color="black" />
            ) : (
              <IconChevronLeft size={13} color="black" />
            )
          }>
          {breadcrumbs.map((item, index) => (
            <Flex justify="space-between" align="center" key={index}>
              <UnStyledLink
                href={item.url}
                key={index}
                tt="uppercase"
                fz="0.85rem"
                c="dark.9"
                fw={index === breadcrumbs.length - 1 ? 700 : 500}>
                {trans(item.text)}
              </UnStyledLink>
            </Flex>
          ))}
        </Breadcrumbs>
      </Container>
    </Box>
  );
}
