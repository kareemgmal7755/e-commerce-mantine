import React from "react";
import style from "../style.module.scss";

export default function Container({ children }: { children: React.ReactNode }) {
  return <div className={style.container_wrapper}>{children}</div>;
}
