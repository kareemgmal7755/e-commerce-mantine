import { Select } from "@mantine/core";
import { trans } from "@mongez/localization";
import user from "../../../account/user";
import { genderAtom } from "../../atoms/select-atom";

export default function GenderSelectInput() {
  return (
    <Select
      radius={0}
      data={["male", "female"]}
      defaultValue={user.get("gender")}
      placeholder={trans("gender")}
      label={trans("gender")}
      onChange={value => genderAtom.update(value)}
    />
  );
}
