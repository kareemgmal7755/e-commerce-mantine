import { Affix, Transition } from "@mantine/core";
import { useWindowScroll } from "@mantine/hooks";
import { IconArrowUp } from "@tabler/icons-react";
import { PrimaryButton } from "../Buttons/PrimaryButton";

export default function AffixButton() {
  const [scroll, scrollTo] = useWindowScroll();

  return (
    <Affix position={{ bottom: 20, right: 20 }}>
      <Transition transition="slide-up" mounted={scroll.y > 0}>
        {transitionStyles => (
          <PrimaryButton
            // style={transitionStyles}
            onClick={() => scrollTo({ y: 0 })}
            w="2.5rem"
            h="2.5rem"
            px={0}
            py={0}
            style={{ borderRadius: "50%" }}>
            <IconArrowUp />
          </PrimaryButton>
        )}
      </Transition>
    </Affix>
  );
}
