import { Input, LoadingOverlay, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import Tooltip from "../Tooltip";

export default function InputWrapper({
  visibleElementRef,
  error,
  id,
  label,
  dir,
  loading,
  hint,
  description,
  required,
  children,
}: any) {
  let inputDescription = description || hint;

  if (hint && !description) {
    inputDescription = (
      <>
        {description || trans("didYouKnow")}
        <Tooltip withArrow label={hint}>
          <span
            style={{
              verticalAlign: "middle",
              marginInlineStart: "0.2rem",
              display: "inline-block",
            }}>
            {/* <IconHelp /> */}
          </span>
        </Tooltip>
      </>
    );
  }

  return (
    <Input.Wrapper
      w="100%"
      ref={visibleElementRef}
      error={
        <Text size="xs" c="red.5" span fw={600}>
          {error}
        </Text>
      }
      id={id}
      label={label}
      dir={dir}
      description={inputDescription}
      styles={theme => ({
        root: {
          position: "relative",
        },
        label: {
          marginBottom: !inputDescription ? 0 : "0.5rem",
          cursor: "pointer",
          fontWeight: 600,
          fontSize: "0.8rem",
        },
        description: {
          marginBottom: inputDescription ? theme.spacing.xs : undefined,
        },
        error: {
          fontWeight: "bold",
          margin: "5px 0",
        },
      })}
      withAsterisk={required}>
      <LoadingOverlay visible={loading} />

      <div>{children}</div>
    </Input.Wrapper>
  );
}
