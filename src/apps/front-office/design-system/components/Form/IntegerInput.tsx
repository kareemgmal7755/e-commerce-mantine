import {
  integerRule,
  maxRule,
  minRule,
  requiredRule,
} from "@mongez/react-form";
import BaseInput, { BaseInputProps } from "./BaseInput";

export default function IntegerInput(props: BaseInputProps) {
  return <BaseInput {...props} />;
}

IntegerInput.defaultProps = {
  type: "integer",
  rules: [requiredRule, minRule, maxRule, integerRule],
};
