import { Textarea } from "@mantine/core";
import { trans } from "@mongez/localization";
import { requiredRule, useFormControl } from "@mongez/react-form";
import { left } from "../../utils/directions";
import { BaseInputProps } from "./BaseInput";
import InputWrapper from "./InputWrapper";

export default function TextAreaInput({
  dir,
  description,
  placeholder,
  required,
  label,
  ...props
}: BaseInputProps) {
  const {
    id,
    value,
    visibleElementRef,
    inputRef,
    error,
    changeValue,
    otherProps,
  } = useFormControl(props);

  return (
    <>
      <InputWrapper
        visibleElementRef={visibleElementRef}
        error={error}
        id={id}
        label={label}
        dir={dir}
        description={description}
        required={required}>
        <Textarea
          id={id}
          styles={theme => ({
            input: {
              textAlign: left(dir),
              padding: "1rem 1rem !important",
              margin: `${theme.spacing.sm}px 0`,
              borderColor: theme.colors.gray[2],
            },
          })}
          ref={inputRef}
          required={required}
          placeholder={
            placeholder && trans(placeholder) + (required ? " *" : "")
          }
          onChange={e => changeValue(e.target.value)}
          value={value}
          {...otherProps}
        />
      </InputWrapper>
    </>
  );
}

TextAreaInput.defaultProps = {
  rules: [requiredRule],
  autosize: true,
  minRows: 6,
  type: "textarea",
};
