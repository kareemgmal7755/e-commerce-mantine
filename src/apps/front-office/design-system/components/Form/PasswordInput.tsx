import { PasswordInput as BasePasswordInput } from "@mantine/core";
import { current } from "@mongez/react";
import {
  lengthRule,
  matchRule,
  maxLengthRule,
  minLengthRule,
  requiredRule,
  useFormControl,
} from "@mongez/react-form";

import { left, right } from "../../utils/directions";
import { BaseInputProps } from "./BaseInput";
import InputWrapper from "./InputWrapper";

export default function PasswordInput({
  description,
  dir,
  placeholder,
  ...props
}: BaseInputProps) {
  const { id, value, name, changeValue, error, inputRef, otherProps, ...rest } =
    useFormControl(props);

  return (
    <>
      <div className="form-control"></div>
      <InputWrapper
        dir={dir}
        id={id}
        description={description}
        {...rest}
        error={error}>
        <BasePasswordInput
          ref={inputRef}
          id={id}
          name={name}
          onChange={e => changeValue(e.currentTarget.value)}
          value={value}
          {...otherProps}
          placeholder={
            placeholder ? placeholder + (props.required ? "*" : "") : ""
          }
          styles={theme => ({
            input: {
              textAlign:
                current("direction") === "ltr" ? left(dir) : right(dir),
              borderRadius: 0,
              marginTop: "auto",
              marginBottom: "auto",
              border: `1px solid ${theme.colors.gray[2]}`,
              borderColor: theme.colors.gray[2],
            },
            label: {
              fontSize: "0.8rem",
              fontWeight:600
            },
          })}
          {...otherProps}
        />
      </InputWrapper>
    </>
  );
}

PasswordInput.defaultProps = {
  type: "password",
  rules: [requiredRule, minLengthRule, lengthRule, maxLengthRule, matchRule],
};
