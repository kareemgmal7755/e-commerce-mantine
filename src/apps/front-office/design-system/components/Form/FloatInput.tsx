import {
  floatRule,
  FormControlProps,
  maxRule,
  minRule,
  requiredRule,
} from "@mongez/react-form";
import BaseInput from "./BaseInput";

export default function FloatInput(props: FormControlProps) {
  return <BaseInput {...props} />;
}

FloatInput.defaultProps = {
  type: "float",
  rules: [requiredRule, minRule, maxRule, floatRule],
};
