import { Form } from "@mongez/react-form";
import { forwardRef } from "react";

const BaseForm = forwardRef(function BaseForm({ props }: any, ref: any) {
  return <Form {...props} ref={ref} />;
});

export default BaseForm;
