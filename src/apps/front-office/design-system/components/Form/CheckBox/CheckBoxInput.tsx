import { Checkbox } from "@mantine/core";
import withCheckboxInput from "./withCheckboxInput";

const CheckBoxInput = withCheckboxInput<any>(Checkbox);

export default CheckBoxInput;
