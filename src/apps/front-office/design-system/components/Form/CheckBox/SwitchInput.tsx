import { Switch } from "@mantine/core";
import withCheckboxInput from "./withCheckboxInput";

const SwitchInput = withCheckboxInput<any>(Switch);

export default SwitchInput;
