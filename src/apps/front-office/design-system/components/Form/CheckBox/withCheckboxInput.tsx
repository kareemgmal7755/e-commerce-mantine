import {
  FormControlProps,
  requiredRule,
  useFormControl,
} from "@mongez/react-form";
import React from "react";

export default function withCheckboxInput<T>(
  Component: React.FC<FormControlProps & T>,
) {
  function CheckboxInput(props: FormControlProps & T) {
    const { value, checked, id, otherProps, visibleElementRef, setChecked } =
      useFormControl(props, {
        isCollectable({ checked }) {
          return checked === true;
        },
      });

    return (
      <span ref={visibleElementRef}>
        <Component
          styles={theme => ({
            label: {
              cursor: "pointer",
              color: theme.colors.rhino[9],
            },
           
          })}
          id={id}
          value={value}
          checked={checked}
          onChange={e => setChecked(e.target.checked)}
          {...otherProps}
        />
      </span>
    );
  }
  CheckboxInput.defaultProps = {
    rules: [requiredRule],
    defaultValue: 1,
  };

  return CheckboxInput as React.FC<FormControlProps & T>;
}
