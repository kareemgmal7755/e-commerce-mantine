import { Input, Loader } from "@mantine/core";
import { trans } from "@mongez/localization";
import { FormControlProps, useFormControl } from "@mongez/react-form";
import React from "react";
import Tooltip from "../Tooltip";
import InputWrapper from "./InputWrapper";

export type BaseInputProps = FormControlProps & {
  description?: React.ReactNode;
};

function _BaseInput({
  dir,
  loading,
  placeholder,
  label,
  description,
  ...props
}: BaseInputProps) {
  const { value, changeValue, inputRef, error, id, otherProps } =
    useFormControl(props);

  let rightSection: React.ReactNode = null;

  // const theme = useMantineTheme();

  if (error) {
    rightSection = (
      <Tooltip withArrow label={error} position="top-end">
        <div></div>
      </Tooltip>
    );
  } else if (loading) {
    rightSection = <Loader size={18} color="gray" />;
  }

  return (
    <>
      <InputWrapper
        dir={dir}
        id={id}
        description={description}
        error={error}
        label={label}>
        <Input
          invalid={error !== null ? "true" : "false"}
          ref={inputRef}
          id={id}
          readOnly={loading}
          rightSection={rightSection}
          styles={theme => ({
            input: {
              borderColor: theme.colors.gray[2],
              borderRadius: 0,
            },
            label: {
              fontWeight: 600,
              fontSize: "0.8rem",
            },
          })}
          placeholder={
            placeholder && trans(placeholder) + (props.required ? " *" : "")
          }
          onChange={e => changeValue(e.currentTarget.value)}
          value={value}
          {...otherProps}
        />
      </InputWrapper>
    </>
  );
}

const BaseInput = React.memo(_BaseInput);

export default BaseInput;
