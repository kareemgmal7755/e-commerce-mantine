import URLS from "apps/front-office/utils/urls";

export const homeItems = [{ text: "home", url: URLS.home }];

export const loginItems = [{ text: "login", url: URLS.auth.login }];

export const forgetPasswordItems = [{ text: "forgetPassword", url: URLS.auth.forgetPassword }];

export const verificationCodeItems = [{ text: "verificationCode", url: URLS.auth.verifyForgetPassword }];

export const resetPasswordItems = [{ text: "resetPassword", url: URLS.auth.resetPassword }];

export const registerItems = [{ text: "register", url: URLS.auth.register }];

export const faqItems = [{ text: "faq", url: URLS.faq }];

export const blogItems = [{ text: "blog", url: URLS.blog.root }];

export const shopItems = [{ text: "shop", url: URLS.shop.list }];

export const brandsItems = [{ text: "brands", url: URLS.brands }];

export const wishlistItems = [{ text: "wishlist", url: URLS.brands }];

export const cartItems = [{ text: "cart", url: URLS.cart }];

export const compareItems = [{ text: "compare", url: URLS.compare }];

export const contactUsItems = [{ text: "contactUs", url: URLS.contactUs }];


