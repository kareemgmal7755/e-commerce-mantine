import { Box, Breadcrumbs, Flex, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import { useEvent } from "@mongez/react-hooks";
import { routerEvents } from "@mongez/react-router";
import Is from "@mongez/supportive-is";
import { IconChevronLeft, IconChevronRight } from "@tabler/icons-react";
import URLS from "apps/front-office/utils/urls";
import { breadcrumbAtom } from "../../atoms/breadcrumb-atom";
import { useMedia } from "../../hooks/use-media";
import Container from "../Container";
import { UnStyledLink } from "../Link";

export default function Breadcrumb({
  title,
  items,
}: {
  title: string;
  items?: {
    text: string;
    url: string;
  }[];
}) {
  const breadcrumbs = breadcrumbAtom.useValue();

  const isMobile = useMedia(1000);

  useEvent(() =>
    routerEvents.onNavigating(() => {
      breadcrumbAtom.update([]);
    }),
  );

  const breadcrumbList: any = items ? items : breadcrumbs;

  if (Is.empty(breadcrumbList) || Is.mobile.any()) return null;

  return (
    <Box py="4rem" bg="aquaHaze.0">
      <Container>
        <Flex
          justify="space-between"
          align={isMobile ? "flex-start" : "center"}
          direction={isMobile ? "column" : "row"}
          gap="1rem">
          <Title order={4} tt="uppercase">
            {trans(title)}
          </Title>
          <Breadcrumbs
            style={{ display: "flex", flexWrap: "wrap", gap: "0.2rem" }}
            separator={
              current("localeCode") === "en" ? (
                <IconChevronRight size={16} color="black" />
              ) : (
                <IconChevronLeft size={16} color="black" />
              )
            }>
            {breadcrumbList === items && (
              <UnStyledLink
                href={URLS.home}
                tt="uppercase"
                c="dark.9"
                fw={500}
                fz="0.8rem">
                {trans("home")}
              </UnStyledLink>
            )}
            {breadcrumbList.map((item, index) => (
              <Flex justify="space-between" align="center" key={index}>
                <UnStyledLink
                  href={item.url}
                  fz="0.8rem"
                  key={index}
                  tt="uppercase"
                  c="dark.9"
                  fw={index === breadcrumbList.length - 1 ? 700 : 500}>
                  {trans(item.text)}
                </UnStyledLink>
              </Flex>
            ))}
          </Breadcrumbs>
        </Flex>
      </Container>
    </Box>
  );
}
