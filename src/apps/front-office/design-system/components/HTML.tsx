import { Box } from "@mantine/core";

export default function HTML({ html }: { html: string | any }) {
  return <Box dangerouslySetInnerHTML={{ __html: html }} maw="100%"></Box>;
}
