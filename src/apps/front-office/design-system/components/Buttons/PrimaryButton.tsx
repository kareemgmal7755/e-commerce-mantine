import { Button, ButtonProps } from "@mantine/core";
import { FC, forwardRef } from "react";
import style from "../style.module.scss";

export type BaseButtonProps = ButtonProps &
  React.ButtonHTMLAttributes<HTMLButtonElement>;

function _PrimaryButton(props: any, ref: any) {
  return (
    <Button
      fw={700}
      color="transparent"
      ref={ref}
      {...props}
      className={style.primary_button_wrapper}
    />
  );
}

export const PrimaryButton = forwardRef(_PrimaryButton) as FC<BaseButtonProps>;
