import { Button, ButtonProps } from "@mantine/core";
import { FC, forwardRef } from "react";
import style from "../style.module.scss";

export type BaseButtonProps = ButtonProps &
  React.ButtonHTMLAttributes<HTMLButtonElement>;

function _SecondaryButton(props: any, ref: any) {
  return (
    <Button
      color="transparent"
      ref={ref}
      {...props}
      className={style.secondary_button_wrapper}
    />
  );
}

export const SecondaryButton = forwardRef(_SecondaryButton) as FC<
  ButtonProps & any
>;
