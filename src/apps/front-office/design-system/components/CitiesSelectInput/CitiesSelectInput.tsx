import { Select } from "@mantine/core";
import { trans } from "@mongez/localization";
import { useOnce } from "@mongez/react-hooks";
import { getCities } from "apps/front-office/utils/services";
import { useState } from "react";
import { cityAtom } from "../../atoms/select-atom";

export default function CitiesSelectInput() {
  const [cities, setCities] = useState<any>([]);

  useOnce(() => {
    getCities().then(response => setCities(response.data.cities));
  });

  return (
    <Select
      radius={0}
      placeholder={trans("city")}
      label={trans("city")}
      data={cities.map(city => ({
        label: city.name,
        value: String(city.id),
      }))}
      onChange={value => cityAtom.update(String(value))}
    />
  );
}
