import { Tooltip as CustomTolTip, TooltipProps } from "@mantine/core";
import React from "react";
import usePrimaryColor from "../../hooks/use-primary-color";

function _Tooltip({ children, label, ...otherProps }: TooltipProps, ref) {
  const color = usePrimaryColor();

  return (
    <span ref={ref}>
      <CustomTolTip
        color="white"
        bg={color}
        withArrow
        label={label}
        {...otherProps}>
        {children}
      </CustomTolTip>
    </span>
  );
}

const Tooltip: React.FC<TooltipProps> = React.forwardRef(_Tooltip);

Tooltip.defaultProps = {
  position: "top",
};

export default Tooltip;
