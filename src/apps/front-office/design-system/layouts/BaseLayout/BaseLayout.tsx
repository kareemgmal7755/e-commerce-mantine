import { Flex } from "@mantine/core";
import React from "react";
import { useMedia } from "../../components";
import AffixButton from "../../components/AffixButton";
import "../../main.scss";
import Footer from "../Footer";
import Header from "../Header";
import Meta from "./Meta";

/**
 * Base layout can be used to wrap all pages
 */
export default function BaseLayout({
  children,
}: {
  children: React.ReactNode;
}) {

  return (
    <>
      <Flex direction="column" h="100%">
        <Meta />
        <Header />
        <main>{children}</main>
        <Footer />
      </Flex>
      <AffixButton />
    </>
  );
}
