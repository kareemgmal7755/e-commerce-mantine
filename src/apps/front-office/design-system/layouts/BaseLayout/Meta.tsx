import Helmet from "@mongez/react-helmet";
import { metaAtom } from "apps/front-office/common/atoms";

export default function Meta() {
  const metaData = metaAtom.useValue();

  if (!metaData.title) return null;

  return <Helmet {...metaData} />;
}
