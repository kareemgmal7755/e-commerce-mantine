import { Box, Flex, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { Form } from "@mongez/react-form";
import { useNewsletterSubscription } from "apps/front-office/common/hooks";
import style from "design-system/layouts/style.module.scss";
import { useMedia } from "../../components";
import { PrimaryButton } from "../../components/Buttons/PrimaryButton";
import Container from "../../components/Container";
import EmailInput from "../../components/Form/EmailInput";

export default function Newsletter() {
  const media = useMedia(1000);
  const newsletterSubmit = useNewsletterSubscription();

  return (
    <Box className={style.footer_content} py="lg">
      <Container>
        <Flex
          justify="space-between"
          align="center"
          py="1.5rem"
          gap="1rem"
          direction={media ? "column" : "row"}>
          <Flex
            direction="column"
            gap="0.5rem"
            c="gray.0"
            w={media ? "100%" : "50%"}
            ta={media ? "center" : "start"}>
            <Title order={2} tt="uppercase">
              {trans("signUpForNewsletter")}
            </Title>
            <Text span fz="0.9rem" fw={600}>
              {trans("signUpUpdates")}
            </Text>
          </Flex>
          <Flex w={media ? "100%" : "50%"}>
            <Form className={style.form_wrapper} onSubmit={newsletterSubmit}>
              <EmailInput
                w="100%"
                size="xl"
           
                name="email"
                placeholder={trans("yourEmail")}
              />
              <PrimaryButton size="xl" type="submit" fz="0.9rem">
                {trans("subscribe")}
              </PrimaryButton>
            </Form>
          </Flex>
        </Flex>
      </Container>
    </Box>
  );
}
