import { Badge, Box, Flex, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { settingsAtom } from "apps/front-office/common/atoms";
import payment from "assets/images/payment.png";
import { appVersion } from "shared/flags";
import { useMedia } from "../../components";
import Container from "../../components/Container";
import { UnStyledLink } from "../../components/Link";
import Mentoor from "../../components/Mentoor";
import { current } from "@mongez/react";

export default function CopyRight() {
  const date = new Date();
  const year = date.getFullYear();
  const media = useMedia(800);
  const generalInfo = settingsAtom.use("general");

  return (
    <Box py="1.5rem">
      <Container>
        <Flex
          justify={media ? "center" : "space-between"}
          align="center"
          direction={media ? "column" : "row"}
          gap="1rem">
          <Box w={media ? "auto" : "20%"}>
            <UnStyledLink to="https://mentoor.io" newTab>
              <Mentoor w={40} />
            </UnStyledLink>
          </Box>
          <Flex justify="center" align="center" w={media ? "auto" : "60%"}>
            <Flex
              align={current("localeCode") === "ar" ? "baseline" : "center"}
              gap="0.2rem"
              ta="center"
              fz="0.9rem"
              justify="center"
              c="gray.0">
              <Text span>{generalInfo.appName}</Text>
              <Text span>
                <span>{year}</span>
              </Text>
              <Text span>
                <span>{trans("rightsReserved")}</span>
              </Text>
              <Badge fw={600} color="rhino" size="md" fz="0.7rem">
                V {appVersion}
              </Badge>
            </Flex>
          </Flex>
          <Box w={media ? "auto" : "20%"}>
            <img src={payment} alt="payImage" width="100%" />
          </Box>
        </Flex>
      </Container>
    </Box>
  );
}
