import URLS from "apps/front-office/utils/urls";

export const informationData = [
  {
    text: "aboutUs",
    route: URLS.pages.aboutUs,
  },
  {
    text: "faq",
    route: URLS.faq,
  },
  {
    text: "blog",
    route: URLS.blog.root,
  },
  {
    text: "privacyPolicy",
    route: URLS.pages.privacyPolicy,
  },
  {
    text: "termsConditions",
    route: URLS.pages.termsConditions,
  },
];

export const myAccountData = [
  {
    text: "myAccount",
    route: URLS.account.editProfile,
  },
  {
    text: "ordersHistory",
    route: URLS.account.orders,
  },
  {
    text: "wishlist",
    route: URLS.account.wishlist,
  },
  {
    text: "addressBook",
    route: URLS.account.address,
  },
  {
    text: "checkout",
    route: URLS.checkout,
  },
];

export const servicesData = [
  {
    text: "contactUs",
    route: URLS.contactUs,
  },
  {
    text: "affiliates",
    route: URLS.affiliate,
  },
];
