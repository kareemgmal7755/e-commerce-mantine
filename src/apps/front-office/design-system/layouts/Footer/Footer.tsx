import { Box, Flex, Grid, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import {
  IconBrandFacebook,
  IconBrandInstagram,
  IconBrandTwitter,
  IconMail,
  IconMapPin,
  IconPhone,
} from "@tabler/icons-react";
import { settingsAtom } from "apps/front-office/common/atoms";
import style from "design-system/layouts/style.module.scss";
import Container from "../../components/Container";
import { UnStyledLink } from "../../components/Link";
import CopyRight from "./CopyRight";
import FooterLinks from "./FooterLinks";
import Newsletter from "./Newsletter";
import { informationData, myAccountData, servicesData } from "./data";

export default function Footer() {
  const address = settingsAtom.get("contact").address;
  const phoneNumber = settingsAtom.get("contact").phoneNumber;
  const socialMedia = settingsAtom.get("social");
  const email = settingsAtom.get("contact").email;

  return (
    <Box bg="rhino.9" mt="auto">
      <Newsletter />
      <Box className={style.footer_content} py="1.5rem">
        <Container>
          <Grid py="lg" gutter={40}>
            <Grid.Col span={{ base: 6, md: 3, xs: 6 }}>
              <FooterLinks data={informationData} title="information" />
            </Grid.Col>
            <Grid.Col span={{ base: 6, md: 3, xs: 6 }}>
              <FooterLinks data={myAccountData} title="account" />
            </Grid.Col>
            <Grid.Col span={{ base: 12, md: 3, xs: 6 }}>
              <FooterLinks data={servicesData} title="services" />
            </Grid.Col>
            <Grid.Col span={{ base: 12, md: 3, xs: 6 }}>
              <Title order={4} tt="uppercase" mb="0.8rem" c="gray.0">
                {trans("contactUs")}
              </Title>
              <Flex direction="column" gap="0.8rem">
                <Flex gap="0.2rem" align="center" c="gray.0">
                  <IconMapPin size={18} />
                  <Text span>{address}</Text>
                </Flex>
                <Flex gap="0.2rem" align="center" c="gray.0">
                  <IconPhone size={18} />
                  <Text span>{phoneNumber}</Text>
                </Flex>
                <Flex gap="0.2rem" align="end" c="gray.0">
                  <IconMail size={18} />
                  <UnStyledLink email={email} c="gray.0">
                    {email}
                  </UnStyledLink>
                </Flex>
                <Flex gap="md">
                  <UnStyledLink href={socialMedia.facebook} c="gray.0">
                    <IconBrandFacebook />
                  </UnStyledLink>
                  <UnStyledLink href={socialMedia.instagram} c="gray.0">
                    <IconBrandInstagram />
                  </UnStyledLink>
                  <UnStyledLink href={socialMedia.twitter} c="gray.0">
                    <IconBrandTwitter />
                  </UnStyledLink>
                </Flex>
              </Flex>
            </Grid.Col>
          </Grid>
        </Container>
      </Box>
      <CopyRight />
    </Box>
  );
}
