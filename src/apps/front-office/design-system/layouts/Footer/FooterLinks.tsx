import { Box, Flex, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { UnStyledLink } from "../../components/Link";
import style from "../style.module.scss";

export type FooterLinksTypes = {
  data: {
    text: string;
    route: any;
  }[];
  title?: string;
};

export default function FooterLinks({ data, title }: FooterLinksTypes) {
  return (
    <Box>
      {title && (
        <Title order={4} tt="uppercase" mb="0.8rem" c="gray.0">
          {trans(title)}
        </Title>
      )}
      <Flex direction="column" gap="0.6rem">
        {data.map((link, index) => (
          <UnStyledLink
            to={link.route}
            key={index}
            c="gray.0"
            className={style.footerLink}>
            {trans(link.text)}
          </UnStyledLink>
        ))}
      </Flex>
    </Box>
  );
}
