import { Button, Drawer, Flex, Indicator, Text, Title } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import {
  IconBrandFacebook,
  IconBrandInstagram,
  IconBrandLinkedin,
  IconBrandPinterest,
  IconBrandTwitter,
  IconBrandYoutubeFilled,
  IconLogin2,
  IconMail,
  IconMenu2,
  IconPhone,
  IconUser,
  IconUserCircle,
  IconX,
} from "@tabler/icons-react";
import { categoriesAtom, settingsAtom } from "apps/front-office/common/atoms";
import URLS from "apps/front-office/utils/urls";
import { UnStyledLink } from "../../components/Link";
import style from "./style.module.scss";

export default function NavbarDrawer() {
  const [opened, { open, close }] = useDisclosure(false);
  const { categories } = categoriesAtom.useValue();
  const { email, phoneNumber } = settingsAtom.use("contact");
  const CloseButton = () => {
    return (
      <Flex
        onClick={close}
        justify="space-between"
        align="center"
        bg="gray.0"
        p="0.7rem"
        className={style.close_drawer_button}>
        <Text span tt="uppercase" fz="0.8rem" fw={700}>
          Close menu
        </Text>
        <IconX size={16} stroke={1.3} />
      </Flex>
    );
  };

  return (
    <>
      <Button
        variant="transparent"
        className={style.navbar_menu_button}
        p={0}
        onClick={open}>
        <IconMenu2 color="#3a5173" stroke={1.8} size={22}/>
      </Button>
      <Drawer
        p={0}
        zIndex={999}
        opened={opened}
        onClose={close}
        withCloseButton={false}
        size="17rem"
        position="left"
        transitionProps={{
          transition:
            current("localeCode") === "en" ? "slide-right" : "slide-left",
          duration: 200,
          timingFunction: "linear",
        }}
        styles={{
          body: {
            padding: 0,
          },
          inner: {
            left: 0,
          },
        }}>
        <CloseButton />
        <Flex direction="column" gap="sm" p="sm">
          <UnStyledLink
            onClick={close}
            w="fit-content"
            tt="uppercase"
            c="dark.9"
            fz="1rem"
            className={style.navbar_link}
            fw={600}
            to={URLS.shop.list}>
            {trans("shop")}
          </UnStyledLink>
          <Indicator
            w="fit-content"
            size={17}
            radius="sm"
            color="rhino.9"
            tt="uppercase"
            label={
              <Text component="p" my={5} fz="0.7rem" fw={600}>
                {trans("sale")}
              </Text>
            }>
            <UnStyledLink
              onClick={close}
              w="fit-content"
              tt="uppercase"
              c="dark.9"
              fz="1rem"
              className={style.navbar_link}
              fw={600}
              to={URLS.shop.offers}>
              {trans("offers")}
            </UnStyledLink>
          </Indicator>
          {categories.map(category => (
            <UnStyledLink
              onClick={close}
              w="fit-content"
              key={category.id}
              to={URLS.shop.viewCategory(category)}
              tt="uppercase"
              c="dark.9"
              fz="1rem"
              className={style.navbar_link}
              fw={600}>
              {trans(category.name)}
            </UnStyledLink>
          ))}
        </Flex>
        <Flex direction="column" gap="1rem" className={style.drawer_section}>
          <UnStyledLink>
            <Flex gap="0.3rem" align="center" className={style.navbar_link}>
              <IconLogin2 size={20} stroke={1} />
              <Text span fz="0.8rem">
                {trans("signIn")}
              </Text>
            </Flex>
          </UnStyledLink>
          <UnStyledLink>
            <Flex gap="0.3rem" align="center" className={style.navbar_link}>
              <IconUser size={20} stroke={1} />
              <Text span fz="0.8rem">
                {trans("register")}
              </Text>
            </Flex>
          </UnStyledLink>
          <UnStyledLink>
            <Flex gap="0.3rem" align="center" className={style.navbar_link}>
              <IconUserCircle size={20} stroke={1} />
              <Text span fz="0.8rem">
                {trans("myAccount")}
              </Text>
            </Flex>
          </UnStyledLink>
        </Flex>
        <Flex direction="column" gap="0.7rem" className={style.drawer_section}>
          <Title order={6} tt="uppercase" fw={600}>
            {trans("needHelp")}
          </Title>
          <UnStyledLink tel={phoneNumber} c="#2f415d">
            <Flex gap="0.3rem" align="center">
              <IconPhone size={20} stroke={1} />
              <Text span fz="0.8rem" fw={600}>
                {phoneNumber}
              </Text>
            </Flex>
          </UnStyledLink>
          <UnStyledLink email={email} c="#2f415d">
            <Flex gap="0.3rem" align="center">
              <IconMail size={20} stroke={1} />
              <Text span fz="0.8rem" fw={600}>
                {email}
              </Text>
            </Flex>
          </UnStyledLink>
        </Flex>
        <Flex gap="0.7rem" direction="column" className={style.drawer_section}>
          <Title order={6} tt="uppercase" fw={600}>
            {trans("followUs")}
          </Title>
          <Flex gap="0.5rem">
            <UnStyledLink href="#" c="#171717">
              <IconBrandFacebook size={18} stroke={0.8} fill="#171717" />
            </UnStyledLink>
            <UnStyledLink href="#" c="#171717">
              <IconBrandTwitter size={18} stroke={0.8} fill="#171717" />
            </UnStyledLink>
            <UnStyledLink href="#" c="#171717">
              <IconBrandPinterest size={18} stroke={1.5} color="#171717" />
            </UnStyledLink>
            <UnStyledLink href="#" c="#171717">
              <IconBrandLinkedin size={18} stroke={1.5} color="#171717" />
            </UnStyledLink>
            <UnStyledLink href="#" c="#171717">
              <IconBrandInstagram size={20} stroke={1} />
            </UnStyledLink>
            <UnStyledLink href="#" c="#171717">
              <IconBrandYoutubeFilled size={18} stroke={0.8} fill="#171717" />
            </UnStyledLink>
          </Flex>
        </Flex>
      </Drawer>
    </>
  );
}
