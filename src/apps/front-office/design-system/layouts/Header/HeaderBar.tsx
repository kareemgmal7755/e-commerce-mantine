import "@mantine/core/styles.css";
import { Box, Flex, Text } from "@mantine/core";
import { headerBarData } from "./data";
import style from "./style.module.scss";

export default function HeaderBar() {
  return (
    <Box
      bg="#000000"
      py="md"
      w="100%"
      className={style.header_bar_wrapper}>
      <Flex
        justify="space-between"
        align="center"
        gap="1.5rem"
        className={style.header_bar_container}>
        {headerBarData.map((item, index) => (
          <Flex gap="0.5rem" align="center" key={index}>
            <item.Icon color="yellow" size={22} />
            <Text span c="gray.0" fz="0.8rem">
              {item.text}
            </Text>
          </Flex>
        ))}
      </Flex>
    </Box>
  );
}
