import { Button, Drawer, Flex, Select, Title } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { trans } from "@mongez/localization";
import { Form, getForm } from "@mongez/react-form";
import { navigateTo, queryString } from "@mongez/react-router";
import { IconSearch, IconX } from "@tabler/icons-react";
import { categoriesAtom } from "apps/front-office/common/atoms";
import { Category } from "apps/front-office/store/utils/types";
import URLS from "apps/front-office/utils/urls";
import { useState } from "react";
import { SubmitButton } from "../../components/Buttons/SubmitButton";
import Container from "../../components/Container";
import TextInput from "../../components/Form/TextInput";
import style from "./style.module.scss";

export default function SearchDrawer() {
  const [opened, { open, close }] = useDisclosure(false);

  const categories = categoriesAtom.use("categories");

  const [selectedCategory, setSelectedCategory] = useState<string | null>("");

  const { q = "", category } = queryString.all();

  const searchProducts = () => {
    const form = getForm("search-form");

    if (!form) return;

    setTimeout(() => {
      close();
      navigateTo(
        URLS.shop.search +
          "?" +
          queryString.toQueryString({
            q: form.value("q"),
            category: selectedCategory,
          }),
      );
    }, 1500);
  };

  return (
    <>
      <Drawer
        styles={{
          body: {
            padding: "0 0 2rem 0",
          },
          content: {
            height: "fit-content",
          },
        }}
        p={0}
        opened={opened}
        onClose={close}
        position="top"
        h="fit-content"
        withCloseButton={false}>
        <Container>
          <Flex direction="column" gap="1.5rem" p="1rem 0">
            <Flex
              justify="space-between"
              align="center"
              p="0.7rem"
              className={style.close_drawer_button}>
              <Title order={4} fw={800}>
                {trans("lookingFor")}
              </Title>
              <IconX size={22} stroke={1.3} onClick={close} />
            </Flex>
            <Form id="search-form" onSubmit={searchProducts}>
              <Flex justify="space-between">
                <Flex
                  justify="space-between"
                  align="center"
                  className={style.form_wrapper}
                  w="100%">
                  <Select
                    w="12rem"
                    p={0}
                    fw={600}
                    withCheckIcon={false}
                    name="category"
                    defaultValue={category}
                    placeholder={trans("allCategories")}
                    data={categories.map((category: Category) => ({
                      value: String(category.id),
                      label: category.name,
                    }))}
                    onChange={value => setSelectedCategory(value)}
                  />
                  <TextInput
                    placeholder={trans("searchForProducts")}
                    w="90%"
                    name="q"
                    defaultValue={q}
                  />
                </Flex>
                <SubmitButton
                  w="4rem"
                  p={0}
                  radius={0}
                  h="3rem"
                  bg="rhino.9"
                  className={style.submit_button}>
                  <IconSearch />
                </SubmitButton>
              </Flex>
            </Form>
          </Flex>
        </Container>
      </Drawer>

      <Button onClick={open} variant="transparent" p={0}>
        <IconSearch color="#2f415d" stroke={1} size={22} />
      </Button>
    </>
  );
}
