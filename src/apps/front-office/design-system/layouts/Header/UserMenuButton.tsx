import { Button, Flex, Menu, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { IconLogout2, IconUser } from "@tabler/icons-react";
import { useLogout } from "apps/front-office/account/hooks";
import user from "apps/front-office/account/user";
import { UnStyledLink } from "../../components/Link";
import { guestData, userMenuData } from "./data";

export default function UserMenuButton() {
  const logout = useLogout();

  return (
    <Menu
      width={180}
      transitionProps={{ transition: "scale-y", duration: 150 }}
      trigger="hover">
      <Menu.Target>
        <Button variant="transparent" p={0}>
          <IconUser color="#3a5173" stroke={1} size={22} />
        </Button>
      </Menu.Target>
      <Menu.Dropdown>
        {user.isGuest() || !user.isLoggedIn() ? (
          <>
            {guestData.map((link, index) => (
              <Menu.Item key={index}>
                <UnStyledLink to={link.route} c="rhino.9">
                  <Flex gap="0.5rem" align="center">
                    <link.icon size={20} stroke={1} />
                    <Text span fz="0.8rem" fw={600}>
                      {trans(link.text)}
                    </Text>
                  </Flex>
                </UnStyledLink>
              </Menu.Item>
            ))}
          </>
        ) : (
          <>
            {userMenuData.map((link, index) => (
              <Menu.Item key={index}>
                <UnStyledLink to={link.route} c="rhino.9">
                  <Flex gap="0.5rem" align="center">
                    <link.icon size={20} stroke={1} />
                    <Text span fz="0.8rem" fw={600}>
                      {trans(link.text)}
                    </Text>
                  </Flex>
                </UnStyledLink>
              </Menu.Item>
            ))}
            <Menu.Divider />
            <Menu.Item onClick={logout}>
              <Flex gap="0.5rem" align="center">
                <IconLogout2 size={20} stroke={1.5} color="#c92a2a"/>
                <Text span fz="0.8rem" fw={700} c="red.9">
                  {trans("logout")}
                </Text>
              </Flex>
            </Menu.Item>
          </>
        )}
      </Menu.Dropdown>
    </Menu>
  );
}
