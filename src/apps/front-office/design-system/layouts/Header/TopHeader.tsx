import { Box, Flex, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { Link } from "@mongez/react-router";
import { IconPhone } from "@tabler/icons-react";
import { settingsAtom } from "apps/front-office/common/atoms";
import Container from "../../components/Container";
import { useMedia } from "../../hooks/use-media";
import { LanguageSelector } from "./LanguageSelector";
import style from "./style.module.scss";
import { UnStyledLink } from "../../components/Link";
import URLS from "apps/front-office/utils/urls";

export default function TopHeader() {
  const media = useMedia(1000);
  const phoneNumber = settingsAtom.get("contact").phoneNumber;

  return (
    <Box bg="rhino.9" py="sm">
      <Container>
        <Flex justify="space-between" align="center" c="gray.0">
          <Flex gap="0.3rem" align="center">
            <IconPhone size={17} />
            <Text span fz="0.78rem" fw={600}>
              {phoneNumber}
            </Text>
          </Flex>
          {!media && (
            <Flex gap="0.5rem" align="end">
              <Text span fz="0.8rem" tt="uppercase" fw={500}>
                {trans("freeShippingOrders")}
              </Text>
              <UnStyledLink
                tt="uppercase"
                to={URLS.shop.list}
                className={style.top_header_link}
                c="gray.0"
                fz="0.8rem"
                fw={600}>
                {trans("shopNow")}
              </UnStyledLink>
            </Flex>
          )}
          <Flex gap="0.5rem" align="center">
            <LanguageSelector />
          </Flex>
        </Flex>
      </Container>
    </Box>
  );
}
