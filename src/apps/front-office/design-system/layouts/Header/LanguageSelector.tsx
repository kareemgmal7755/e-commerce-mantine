import { Flex, Image, Menu, Text, UnstyledButton } from "@mantine/core";
import { current } from "@mongez/react";
import { changeLocaleCode } from "@mongez/react-router";
import { IconChevronDown, IconChevronUp } from "@tabler/icons-react";
import arabic from "assets/images/flags/sa.png";
import english from "assets/images/flags/uke.png";
import { useState } from "react";

const data = [
  { label: "English", image: english, localeCode: "en" },
  { label: "العربية", image: arabic, localeCode: "ar" },
];

const menuStyles: any = theme => ({
  control: {
    width: 120,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "6px",
    borderRadius: theme.radius.md,
    transition: "background-color 150ms ease",
  },

  label: {
    fontWeight: 500,
    fontSize: "0.8rem",
    color: "#fff",
    textTransform: "uppercase",
  },
  item: {
    fontSize: "0.8rem",
    padding: "0.5rem",
    "&:hover": {
      backgroundColor: theme.colors.gray[3],
    },
  },
});

export function LanguageSelector() {
  const [opened, setOpened] = useState(false);
  const [selected, setSelected] = useState(() => {
    const localeCode = current("localeCode");

    return data.find(item => item.localeCode === localeCode) || data[0];
  });
  const items = data.map(item => (
    <Menu.Item
      onClick={() => {
        changeLocaleCode(item.localeCode);
        setSelected(item);
      }}
      key={item.label}>
      <Flex gap="0.3rem" align="center">
        <Image
          src={item.label === "English" ? english : arabic}
          width={18}
          height={18}
        />
        <Text span tt="uppercase" fz="0.65rem" fw={700}>
          {item.label}
        </Text>
      </Flex>
    </Menu.Item>
  ));

  return (
    <Menu
      onOpen={() => setOpened(true)}
      onClose={() => setOpened(false)}
      radius="md"
      width="md"
      transitionProps={{ transition: "fade", duration: 150 }}
      styles={menuStyles}>
      <Menu.Target>
        <UnstyledButton>
          <Flex align="center" gap="0.1rem" p={0}>
            <Image src={selected.image} width={18} height={18} />
            <Text span fz="0.8rem" tt="uppercase" fw={600}>
              {selected.label}
            </Text>
            {opened ? (
              <IconChevronUp size={16} stroke={1.5} color="#fff" />
            ) : (
              <IconChevronDown size={16} stroke={1.5} color="#fff" />
            )}
          </Flex>
        </UnstyledButton>
      </Menu.Target>
      <Menu.Dropdown fw={600}>{items}</Menu.Dropdown>
    </Menu>
  );
}
