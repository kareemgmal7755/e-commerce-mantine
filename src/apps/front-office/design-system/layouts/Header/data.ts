import {
  IconAddressBook,
  IconArrowsShuffle,
  IconBox,
  IconHeart,
  IconLogin2,
  IconPlanet,
  IconReload,
  IconTruck,
  IconUser,
  IconUserCircle,
} from "@tabler/icons-react";
import URLS from "apps/front-office/utils/urls";
import cuff from "assets/images/cuff.jpg";
import flannel from "assets/images/flannel.jpg";
import oxford from "assets/images/oxford.jpg";

export const headerBarData = [
  {
    Icon: IconPlanet,
    text: "BUY ONLINE PICK UP IN STORE",
  },
  {
    Icon: IconTruck,
    text: "FREE WORLDWIDE SHIPPING ON ALL ORDERS ABOVE $100",
  },
  {
    Icon: IconReload,
    text: "EXTENDED RETURN UNTIL 30 DAYS",
  },
  {
    Icon: IconPlanet,
    text: "BUY ONLINE PICK UP IN STORE",
  },
];

export const navbarData = [
  {
    route: URLS.home,
    text: "home",
  },
  {
    route: URLS.shop.list,
    text: "shop",
  },
  {
    route: URLS.brands,
    text: "brands",
  },
  {
    route: URLS.blog.root,
    text: "blog",
  },
];

export const userMenuData = [
  {
    route: URLS.account.editProfile,
    text: "myAccount",
    icon: IconUserCircle,
  },
  {
    route: URLS.account.wishlist,
    text: "wishlist",
    icon: IconHeart,
  },
  {
    route: URLS.account.orders,
    text: "orders",
    icon: IconBox,
  },
  {
    route: URLS.account.address,
    text: "addresses",
    icon: IconAddressBook,
  },
  {
    route: URLS.compare,
    text: "compare",
    icon: IconArrowsShuffle,
  },
];

export const guestData = [
  {
    route: URLS.auth.login,
    text: "signIn",
    icon: IconLogin2,
  },
  {
    route: URLS.auth.register,
    text: "register",
    icon: IconUser,
  },
];

export const searchDrawerProducts = [
  {
    image: oxford,
    title: "Oxford Cuban Shirt",
    price: 99,
    discountPrice: 114,
    raring: 3,
  },
  {
    image: cuff,
    title: "Cuff Beanie Cap",
    price: 128,
    raring: 5,
  },
  {
    image: flannel,
    title: "Flannel Collar Shirt",
    price: 99,
    raring: 4,
  },
];
