import HeaderBar from "./HeaderBar";
import Navbar from "./Navbar";
import TopHeader from "./TopHeader";

export default function Header() {
  return (
    <>
      {/* <HeaderBar /> */}
      <TopHeader />
      <Navbar />
    </>
  );
}
