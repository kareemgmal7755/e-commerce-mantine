import { Divider, Flex } from "@mantine/core";
import { cartAtom } from "apps/front-office/cart/atoms";
import ProductCartDrawerCard from "apps/front-office/store/components/ProductCartDrawerCard";
import React from "react";

export default function CartProducts() {
  const cart = cartAtom.use("items") || [];

  return (
    <Flex direction="column" gap="1.5rem" px="md">
      <Divider />
      {cart.map(item => (
        <React.Fragment key={item.id}>
          <ProductCartDrawerCard key={item.id} item={item} />
        </React.Fragment>
      ))}
    </Flex>
  );
}
