import { Box, Flex, Indicator, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { IconHeart } from "@tabler/icons-react";
import { userAtom } from "apps/front-office/account/atoms";
import { categoriesAtom, settingsAtom } from "apps/front-office/common/atoms";
import URLS from "apps/front-office/utils/urls";
import { useEffect, useState } from "react";
import Container from "../../components/Container";
import { UnStyledLink } from "../../components/Link";
import { useMedia } from "../../hooks/use-media";
import CartDrawerButton from "./CartDrawerButton";
import NavbarDrawer from "./NavbarDrawer";
import SearchDrawer from "./SearchDrawer";
import UserMenuButton from "./UserMenuButton";
import style from "./style.module.scss";

export default function Navbar() {
  const [navbar, setNavbar] = useState<boolean>(false);
  const totalWishlist = userAtom.use("totalWishlist");
  const { categories } = categoriesAtom.useValue();

  useEffect(() => {
    const listenerScrollEvent = () => {
      window.scrollY >= 150 ? setNavbar(true) : setNavbar(false);
    };
    window.addEventListener("scroll", listenerScrollEvent);
    return () => {
      window.removeEventListener("scroll", listenerScrollEvent);
    };
  }, []);

  const media = useMedia(1000);
  const largeScreen = useMedia(1400);
  const logo = settingsAtom.get("general").logo.url;

  return (
    <>
      <Box py="1rem" className={navbar ? style.navbar_wrapper : ""}>
        <Container>
          <Flex justify="space-between" align="center" gap="0.9rem">
            <UnStyledLink to={URLS.home}>
              <img src={logo} width={100} />
            </UnStyledLink>
            {!media && (
              <Flex gap="0.6rem" align="baseline" w="100%">
                <UnStyledLink
                  tt="uppercase"
                  c="dark.9"
                  fz={largeScreen ? "0.75rem" : "0.9rem"}
                  className={style.navbar_link}
                  fw={600}
                  to={URLS.shop.list}>
                  {trans("shop")}
                </UnStyledLink>
                <Indicator
                  size={17}
                  radius="sm"
                  color="rhino.9"
                  p={5}
                  tt="uppercase"
                  label={<Text component="p" my={5} fz="0.6rem" fw={600}>{trans("sale")}</Text>}>
                  <UnStyledLink
                    tt="uppercase"
                    c="dark.9"
                    fz={largeScreen ? "0.75rem" : "0.9rem"}
                    className={style.navbar_link}
                    fw={600}
                    to={URLS.shop.offers}>
                    {trans("offers")}
                  </UnStyledLink>
                </Indicator>
                {categories.map(category => (
                  <UnStyledLink
                    key={category.id}
                    to={URLS.shop.viewCategory(category)}
                    tt="uppercase"
                    c="dark.9"
                    fz={largeScreen ? "0.75rem" : "0.9rem"}
                    className={style.navbar_link}
                    fw={600}>
                    {trans(category.name)}
                  </UnStyledLink>
                ))}
              </Flex>
            )}
            <Flex justify="space-between" align="baseline" gap="0.7rem">
              <SearchDrawer />
              <UserMenuButton />
              <UnStyledLink to={URLS.wishlist}>
                <Indicator color="rhino.9" label={totalWishlist || 0} size={18}>
                  <IconHeart color="#3a5173" stroke={1} size={22}/>
                </Indicator>
              </UnStyledLink>
              <CartDrawerButton />
              {media && <NavbarDrawer />}
            </Flex>
          </Flex>
        </Container>
      </Box>
    </>
  );
}
