import { Indicator, UnstyledButton } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { IconShoppingCart } from "@tabler/icons-react";
import { userAtom } from "apps/front-office/account/atoms";
import { getCart } from "apps/front-office/cart/services/cart-services";
import { useState } from "react";
import CartDrawer from "./CartDrawer";

export default function CartDrawerButton() {
  const [opened, { open, close }] = useDisclosure(false);
  const [isLoading, setIsLoading] = useState(false);
  const totalCart = userAtom.use("totalCart");

  const openCart = () => {
    open();
    setIsLoading(true);
    getCart().then(() => {
      setIsLoading(false);
    });
  };

  return (
    <>
      <UnstyledButton onClick={openCart}>
        <Indicator color="rhino.9" label={totalCart || 0} size={18}>
          <IconShoppingCart color="#3a5173" stroke={1} size={22}/>
        </Indicator>
      </UnstyledButton>
      <CartDrawer isLoading={isLoading} opened={opened} close={close} />
    </>
  );
}
