import { Center, Divider, Drawer, Flex, Loader, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { current } from "@mongez/react";
import { userAtom } from "apps/front-office/account/atoms";
import { cartAtom } from "apps/front-office/cart/atoms";
import { price } from "apps/front-office/store/utils/price";
import URLS from "apps/front-office/utils/urls";
import React from "react";
import { RhinoButton } from "../../components/Buttons/RhinoButton";
import { UnStyledLink } from "../../components/Link";
import ProductCartDrawerCard from "apps/front-office/store/components/ProductCartDrawerCard";

type CartDrawerTypes = {
  isLoading: boolean;
  opened: boolean;
  close: () => void;
};

export default function CartDrawer({
  isLoading,
  opened,
  close,
}: CartDrawerTypes) {
  const totalCart = userAtom.use("totalCart");
  const items = cartAtom.use("items") || [];
  const totalsText = [...(cartAtom.use("totalsText") || [])];
  const finalPrice = totalsText.pop();

  return (
    <Drawer
      opened={opened}
      onClose={close}
      styles={{
        body: {
          padding: "0.5rem",
        },
      }}
      size="sm"
      title={
        <Text component="p" fz="1.5rm" fw={700} tt="uppercase">
          {trans("yourCart")} ({totalCart} {trans("items")})
        </Text>
      }
      position={current("locale") === "en" ? "left" : "right"}>
      {isLoading && <Loader />}
      {items.length === 0 && !isLoading && (
        <Center fz="xl">{trans("cartEmpty")}</Center>
      )}
      {items.length > 0 && (
        <>
          <Flex direction="column" gap="1.5rem" px="md">
            <Divider />
            {items.map(item => (
              <React.Fragment key={item.id}>
                <ProductCartDrawerCard key={item.id} item={item} />
              </React.Fragment>
            ))}
          </Flex>
          <Flex
            pos="sticky"
            bottom={0}
            p="md"
            bg="gray.0"
            direction="column"
            gap="1rem">
            <Flex justify="space-between" align="center">
              <Title order={4}>{trans("total")}:</Title>
              {finalPrice && (
                <Title order={4} c="rhino.9">
                  {price(finalPrice.value)}
                </Title>
              )}
            </Flex>
            <Flex justify="space-between" align="center" gap="0.5rem">
              <UnStyledLink to={URLS.checkout} w="50%">
                <RhinoButton size="md" w="100%">
                  {trans("checkout")}
                </RhinoButton>
              </UnStyledLink>
              <UnStyledLink to={URLS.cart} w="50%">
                <RhinoButton size="md" w="100%">
                  {trans("viewCart")}
                </RhinoButton>
              </UnStyledLink>
            </Flex>
          </Flex>
        </>
      )}
    </Drawer>
  );
}
