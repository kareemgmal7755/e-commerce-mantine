import { Avatar, Box, Flex, Grid, Text, UnstyledButton } from "@mantine/core";
import { trans } from "@mongez/localization";
import { currentRoute } from "@mongez/react-router";
import { useLogout } from "apps/front-office/account/hooks";
import user from "apps/front-office/account/user";
import Container from "../../components/Container";
import { UnStyledLink } from "../../components/Link";
import BaseLayout from "../BaseLayout";
import { sideLinksData } from "./data";

export default function AccountLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const logout = useLogout();

  return (
    <BaseLayout>
      <Container>
        <Grid py="4rem">
          <Grid.Col span={{ base: 12, md: 3 }}>
            <Box bg="aquaHaze.0" py="1rem">
              <Flex
                direction="column"
                gap="0.3rem"
                justify="center"
                align="center"
                ta="center"
                py="1.5rem">
                <Avatar radius="50%" size={110} color="rhino">
                  {user.get("name")[0]}
                </Avatar>
                <Text fw={700} component="p" fz="1rem">
                  {user.get("name")}
                </Text>
                <Text fw={600} span c="aquaHaze.4" fz="0.85rem">
                  {user.get("email")}
                </Text>
              </Flex>
              <Flex direction="column">
                {sideLinksData.map(link => (
                  <UnStyledLink
                    fw={500}
                    key={link.text}
                    to={link.route}
                    py="0.5rem"
                    px="1rem"
                    c="dark.9"
                    bg={currentRoute() === link.route ? "#fff" : ""}>
                    {trans(link.text)}
                  </UnStyledLink>
                ))}
                <UnstyledButton
                  fw={600}
                  ta="start"
                  onClick={logout}
                  py="0.5rem"
                  px="1rem"
                  c="red.9">
                  {trans("logout")}
                </UnstyledButton>
              </Flex>
            </Box>
          </Grid.Col>
          <Grid.Col span={{ base: 12, md: 9 }}>{children}</Grid.Col>
        </Grid>
      </Container>
    </BaseLayout>
  );
}
