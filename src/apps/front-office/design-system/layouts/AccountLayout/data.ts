import URLS from "apps/front-office/utils/urls";

export const sideLinksData = [
  {
    route: URLS.account.editProfile,
    text: "editProfile",
  },
  {
    route: URLS.account.updatePassword,
    text: "privacyPasswords",
  },
  {
    route: URLS.account.wishlist,
    text: "wishlist",
  },
  {
    route: URLS.account.orders,
    text: "orders",
  },
  {
    route: URLS.account.address,
    text: "addresses",
  },
];
