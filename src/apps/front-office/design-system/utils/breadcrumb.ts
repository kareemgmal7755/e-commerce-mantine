export function breadcrumb(text: string, url: string, title?: string) {
  return {
    text,
    url,
    title,
  };
}
