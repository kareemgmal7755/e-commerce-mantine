import { useMediaQuery } from "@mantine/hooks";

export type UseMediaType = {
  media: number;
};

export function useMedia(media = 768) {
  return useMediaQuery(`(max-width: ${media}px)`);
}
