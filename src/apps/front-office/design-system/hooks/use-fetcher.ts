import { useOnce } from "@mongez/react-hooks";
import { get, merge } from "@mongez/reinforcements";
import { AxiosResponse } from "axios";
import { useState } from "react";

export type ResponseCache = {
  expiresAt: number;
  response: any;
};

export type CachedResponses = Record<string, ResponseCache>;

export type FetcherOptions = {
  defaultParams: Record<string, any>;
  limit: number;
  /**
   * Cache Key
   */
  cacheKey: string;
  /**
   * Determine if the response should be cached or not.
   *
   * If set to zero (0), the response will never be cached.
   * If set to a positive number, the response will be cached for that amount of milliseconds.
   * If set to Infinity, the response will be cached forever
   *
   * Please note that the response is cached in memory, so it will be lost when the browser page is refreshed or closed.
   *
   * @default 5 minutes (60 * 5000).
   */
  expiresAfter: number;
  // the keys that will be taken from the `response.data` object and will be used as the output
  // it supports dot notation like `paginationInfo.currentPage` or `meta.totalRecords`
  keys: {
    records?: string;
    limit?: string;
    page?: string;
    pages?: string;
    total?: string;
    results?: string;
    pageNumber?: string;
  };
  onSuccess?: (response: AxiosResponse) => void;
};

export type FetcherOutput = {
  records: any[];
  error: null | any;
  load: (params?: Record<string, any>) => Promise<any>;
  reload: (params?: Record<string, any>) => Promise<any>;
  isLoading: boolean;
  loadMore: (params?: Record<string, any>) => Promise<any>;
  goToPage: (page: number) => Promise<any>;
  reset: () => Promise<any>;
  response?: AxiosResponse;
  data: AxiosResponse["data"];
  defaultParams: Record<string, any>;
  params: Record<string, any>;
  pagination: {
    page: number;
    pages: number;
    total: number;
    limit: number;
    results: number;

    isLastPage: boolean;
    isFirstPage: boolean;
    paginatable: boolean;
  };
};

const cachedResponses: CachedResponses = {};

const responseCacheManager = {
  cacheKey(fetcher: any, params: any) {
    return (
      JSON.stringify(fetcher) + JSON.stringify(params) + (params.cacheKey || "")
    );
  },
  canBeCached(params: any) {
    return params.expiresAfter > 0;
  },
  isExpired(cache: ResponseCache) {
    return cache.expiresAt < new Date().getTime();
  },
  set(cacheKey: string, response: any, params: any) {
    cachedResponses[cacheKey] = {
      response,
      expiresAt: new Date().getTime() + params.expiresAfter,
    };
  },
  get(cacheKey: string) {
    const cacheResponse = cachedResponses[cacheKey];

    if (!cacheResponse) return null;

    const isExpired = this.isExpired(cacheResponse);

    if (isExpired) {
      this.delete(cacheKey);
      return null;
    }

    return cacheResponse.response;
  },
  delete(cacheKey: string) {
    delete cachedResponses[cacheKey];
  },
  all() {
    return cachedResponses;
  },
};

const defaultOptions: Partial<FetcherOptions> = {
  defaultParams: {},
  expiresAfter: 0, // disabled
  cacheKey: "",
  keys: {
    records: "records",
    limit: "paginationInfo.limit",
    page: "paginationInfo.page",
    results: "paginationInfo.result",
    pages: "paginationInfo.pages",
    total: "paginationInfo.total",
    pageNumber: "page",
  },
};

let currentOptions: FetcherOptions = { ...defaultOptions } as FetcherOptions;

export function setFetchOptions(options: Partial<FetcherOptions> = {}) {
  currentOptions = { ...currentOptions, ...options };
}

export function getFetchOptions() {
  return currentOptions;
}

function getOptions(options: Partial<FetcherOptions>): FetcherOptions {
  return merge(currentOptions, options);
}

/**
 * Advanced hook to fetch data.
 */
export function useFetcher(
  fetcher: (params: any) => Promise<any>,
  options: Partial<FetcherOptions> = {},
): FetcherOutput {
  const fetchOptions = getOptions(options);

  const canBeCached = responseCacheManager.canBeCached(fetchOptions);

  const load = (params: any = {}) => {
    return new Promise(reject => {
      const fetcherParams = { ...fetchOptions.defaultParams, ...params };
      let cacheKey = "";

      if (canBeCached) {
        cacheKey = responseCacheManager.cacheKey(fetcher, fetcherParams);

        const response = responseCacheManager.get(cacheKey);

        if (response) {
          return updateSettings(response, fetcherParams);
        }
      }

      fetcher({ ...fetchOptions.defaultParams, ...params })
        .then(response => {
          if (fetchOptions.onSuccess) {
            fetchOptions.onSuccess(response);
          }

          updateSettings(response, fetcherParams);

          if (canBeCached) {
            responseCacheManager.set(cacheKey, response, fetcherParams);
          }
        })
        .catch(error => {
          setSettings({
            ...settings,
            error,
            response: error?.response,
            isLoading: false,
          });
          reject(error);
        });
    });
  };

  const goToPage = (pageNumber: number) => {
    return load({
      [fetchOptions.keys.pageNumber as string]: pageNumber,
      ...settings.params,
    });
  };

  const [settings, setSettings] = useState<any>(() => ({
    records: [],
    error: null,
    isLoading: true,
    currentPage: 1,
    totalPages: 0,
    totalRecords: 0,
    currentRecords: 0,
    defaultParams: fetchOptions.defaultParams || {},
    params: fetchOptions.defaultParams || {},
    keys: fetchOptions.keys,
  }));

  const updateSettings = (response: any, params: any) => {
    const responseData = response.data;

    setSettings({
      ...settings,
      error: null,
      isLoading: false,
      response,
      records: get(responseData, settings.keys.records || "records", []),
      pages: get(responseData, settings.keys.pages, 0),
      total: get(responseData, settings.keys.total, 0),
      results: get(responseData, settings.keys.results, 0),
      limit: get(responseData, settings.keys.limit, 0),
      page: get(responseData, settings.keys.page, 0),
      params: params,
    });
  };

  useOnce(() => {
    load();
  });

  return {
    load,
    reload: () => load(settings.params),
    isLoading: settings.isLoading,
    records: settings.records,
    error: settings.error,
    response: settings.response,
    data: settings.response?.data,
    loadMore: () => goToPage(settings.page + 1),
    goToPage,
    reset: () => load(fetchOptions.defaultParams),
    params: settings.params,
    defaultParams: fetchOptions.defaultParams,
    pagination: {
      page: settings.page,
      pages: settings.pages,
      total: settings.total,
      results: settings.results,
      limit: settings.limit,
      paginatable: settings.pages > 1,
      isLastPage: settings.page === settings.pages,
      isFirstPage: settings.page === 1,
    },
  };
}
