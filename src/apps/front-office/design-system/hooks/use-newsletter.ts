import { trans } from "@mongez/localization";
import { subscribeToNewsletter } from "../../common/services/newsletter";
import { toastError, toastSuccess } from "../utils/toast";
import parseError from "apps/front-office/utils/parse-error";

export function useNewsletterSubscription() {
  return ({ values, form }) => {
    subscribeToNewsletter(values)
      .then(() => {
        toastSuccess(trans("successfullySubscribed"));
        form.reset();
        form.submitting(false);
      })
      .catch(error => {
        toastError(parseError(error));
        form.submitting(false);
      });
  };
}
