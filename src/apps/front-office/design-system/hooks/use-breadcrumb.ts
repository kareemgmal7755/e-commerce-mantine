import { useEffect, useRef } from "react";
import { Breadcrumb, breadcrumbAtom } from "../atoms/breadcrumb-atom";

export default function useBreadcrumb(breadcrumbs: Breadcrumb[]) {
  const isLoadedRef = useRef(false);

  if (!isLoadedRef.current) {
    setTimeout(() => {
      isLoadedRef.current = true;
      breadcrumbAtom.update(breadcrumbs);
    }, 0);
  }

  useEffect(() => {
    isLoadedRef.current = false;
    breadcrumbAtom.update(breadcrumbs);
  }, [breadcrumbs]);
}
