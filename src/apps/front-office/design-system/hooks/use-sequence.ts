import { useEffect, useState } from "react";

export type SequenceOptions = {
  length: number;
  delay: number;
  increaseBy: number;
};

export function useSequence({
  length,
  delay,
  increaseBy = 1,
}: SequenceOptions) {
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    if (currentIndex < length) {
      const timer = setTimeout(() => {
        setCurrentIndex(currentIndex + increaseBy);
      }, delay);

      return () => clearTimeout(timer);
    }
  }, [currentIndex, length, delay, increaseBy]);

  return currentIndex;
}
