import { Box, Checkbox, Flex, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { useState } from "react";

import { HiddenInput } from "@mongez/react-form";
import { addressesAtom } from "apps/front-office/address-book/atom";
import AddAddress from "apps/front-office/address-book/components/AddAddress";
import { Address } from "apps/front-office/address-book/types";
import BillingAddressModal from "../BillingAddressModal";

export default function BillingAddress() {
  const [usedShippingAddress, setUsedShippingAddress] = useState(false);
  const addresses = addressesAtom.useValue();
  const [selectedAddress, setSelectedAddress] = useState<Address | undefined>(
    addresses.find(address => address.isPrimary),
  );

  return (
    <>
      <Box mt="xl">
        <Flex gap="0.5rem" direction="column">
          <Title order={4}>{trans("billingAddress")}</Title>
          {selectedAddress?.id && (
            <HiddenInput name="billingAddress" value={selectedAddress.id} />
          )}
          <Checkbox
            name="useShippingAddress"
            checked={!usedShippingAddress}
            color="rhino.9"
            onChange={() => setUsedShippingAddress(!usedShippingAddress)}
            label={trans("useShippingAddress")}
          />
        </Flex>
      </Box>
      {usedShippingAddress && (
        <>
          <AddAddress />
          <BillingAddressModal
            selectedAddress={selectedAddress}
            setSelectedAddress={setSelectedAddress}
          />
        </>
      )}
    </>
  );
}
