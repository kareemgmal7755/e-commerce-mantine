import { preload } from "@mongez/react-utils";
import { addressesAtom } from "apps/front-office/address-book/atom";
import { getAddressBook } from "apps/front-office/address-book/services/address-book-service";
import BillingAddress from "../BillingAddress";
import ShippingAddress from "../ShippingAddress";

function _CheckoutAddress() {
  return (
    <>
      <ShippingAddress />
      <BillingAddress />
    </>
  );
}

const CheckoutAddress = preload(_CheckoutAddress, getAddressBook, {
  onSuccess(response) {
    addressesAtom.update(response.data.addresses);
  },
});
export default CheckoutAddress;
