import { Flex, SimpleGrid, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import user from "apps/front-office/account/user";
import { price } from "apps/front-office/store/utils/price";
import { checkoutAtom } from "../../atom";
import { useMedia } from "apps/front-office/design-system/components";

export default function OrderDetails() {
  const media = useMedia(1000);
  const order = checkoutAtom.useValue();
  const primaryAddress = order.addresses.map(
    (address, index) =>
      address.isPrimary && (
        <Text key={index} fw={600}>
          {address.address}
        </Text>
      ),
  );

  return (
    <SimpleGrid
      cols={{base:1, sm:2}}
      w={media ? "100%" : "70%"}
      spacing={30}
      verticalSpacing={10}
      my="lg"
      bg="gray.1"
      p="1rem">
      <Flex justify="space-between">
        <Text>{trans("orderId")}:</Text>
        <Text fw={600}>{order.cart.id}</Text>
      </Flex>
      <Flex justify="space-between">
        <Text>{trans("name")}:</Text>
        <Text fw={600}>{user.get("name")}</Text>
      </Flex>
      <Flex justify="space-between">
        <Text>{trans("total")}:</Text>
        <Text fw={600}>{price(order.cart.totals.finalPrice)}</Text>
      </Flex>
      <Flex justify="space-between">
        <Text>{trans("address")}:</Text>
        {primaryAddress}
      </Flex>
      <Flex justify="space-between">
        <Text>{trans("paymentMethod")}:</Text>
        <Text fw={600}>{order.paymentMethods[0].label}</Text>
      </Flex>
      <Flex justify="space-between">
        <Text>{trans("shippingExpenses")}:</Text>
        <Text fw={600}>
          {order.cart.totals.shippingFees === 0
            ? trans("free")
            : order.cart.totals.shippingFees}
        </Text>
      </Flex>
    </SimpleGrid>
  );
}
