import { Box, Flex, Modal, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import { HiddenInput } from "@mongez/react-form";
import { useBooleanState } from "@mongez/react-hooks";
import { IconMapPinFilled } from "@tabler/icons-react";
import { addressesAtom } from "apps/front-office/address-book/atom";
import { Address } from "apps/front-office/address-book/types";
import { RhinoButton } from "apps/front-office/design-system/components/Buttons/RhinoButton";
import style from "../style.module.scss";

export default function BillingAddressModal({
  selectedAddress,
  setSelectedAddress,
}: {
  selectedAddress: Address | undefined;
  setSelectedAddress: React.Dispatch<React.SetStateAction<Address | undefined>>;
}) {
  const addresses = addressesAtom.useValue();
  const [
    selectingAddressesOpened,
    openSelectingAddresses,
    closeSelectingAddresses,
  ] = useBooleanState(false);
  return (
    <>
     
      <Box className={style.address_wrapper} p="0.5rem" c="gray.7">
        <Flex justify="space-between">
          <Flex gap="1rem" align="center">
            <Flex gap="0.1rem" align="center">
              <IconMapPinFilled size={18} />
              <Text span>{selectedAddress?.label}</Text>
            </Flex>
            {selectedAddress?.isPrimary && (
              <Text
                bg="rhino.9"
                c="gray.0"
                py={2.5}
                px={5}
                component="p"
                fz="0.8rem"
                fw={600}>
                {trans("default")}
              </Text>
            )}
          </Flex>
          <RhinoButton onClick={openSelectingAddresses}>
            {trans("change")}
          </RhinoButton>
        </Flex>
        <Text component="p" fw={600}>
          {selectedAddress?.name}
        </Text>
        <Text fw={700}>
          {selectedAddress?.address}, {selectedAddress?.district?.name},
          {selectedAddress?.district?.city?.name}
        </Text>
        <Text>{selectedAddress?.phoneNumber}</Text>
      </Box>
      <Modal
        size="xl"
        title={
          <Text c="gray.7" fw={700}>
            {trans("selectAddress")}
          </Text>
        }
        opened={selectingAddressesOpened}
        onClose={closeSelectingAddresses}>
        <Box>
          {addresses.map(address => (
            <Box
              p="1rem"
              bg={address.id === selectedAddress?.id ? "rhino.9" : "gray.0"}
              className={style.address_wrapper}
              c={address.id === selectedAddress?.id ? "gray.0" : "dark.9"}
              key={address.id}
              onClick={() => {
                setSelectedAddress(address);
                closeSelectingAddresses();
              }}
              style={{
                cursor: "pointer",
              }}>
              <Flex gap="1rem" align="center">
                <Flex gap="0.1rem" align="center">
                  <IconMapPinFilled size={18} />
                  <Text span>{selectedAddress?.label}</Text>
                </Flex>
                {address?.isPrimary && (
                  <Text
                    bg={
                      address.id !== selectedAddress?.id ? "rhino.9" : "gray.0"
                    }
                    c={address.id !== selectedAddress?.id ? "gray.0" : "dark.9"}
                    py={2.5}
                    px={5}
                    component="p"
                    fz="0.8rem"
                    fw={600}>
                    {trans("default")}
                  </Text>
                )}
              </Flex>
              <Text>{address.name}</Text>
              <Text fw={700}>
                {address?.address}, {address.district?.name},{" "}
                {address?.district?.city?.name}
              </Text>
              <Text>{address?.phoneNumber}</Text>
            </Box>
          ))}
        </Box>
      </Modal>
    </>
  );
}
