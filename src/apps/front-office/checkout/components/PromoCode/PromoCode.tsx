import { Box, Flex, Title, useMantineTheme } from "@mantine/core";
import { trans } from "@mongez/localization";
import { cartAtom } from "apps/front-office/cart/atoms";
import { RhinoButton } from "apps/front-office/design-system/components/Buttons/RhinoButton";
import TextInput from "apps/front-office/design-system/components/Form/TextInput";
import { toastError } from "apps/front-office/design-system/utils/toast";
import parseError from "apps/front-office/utils/parse-error";
import { useRef, useState } from "react";
import { applyCode, removeCode } from "../../services/services";
import style from "../style.module.scss";

export default function PromoCode() {
  const theme = useMantineTheme();
  const [codeSuccess, setCodeSuccess] = useState(false);
  const coupon = cartAtom.use("coupon");
  const inputValueRef = useRef<string>("");

  const submitCode = () => {
    if (!inputValueRef.current) return;

    const values = {
      code: inputValueRef.current,
    };

    applyCode(values)
      .then(() => {
        setCodeSuccess(true);
      })
      .catch(error => {
        toastError(parseError(error));
      });
  };

  const removePromoCode = () => {
    removeCode()
      .then(() => {
        setCodeSuccess(false);
      })
      .catch(error => {
        toastError(parseError(error));
      });
  };

  return (
    <Box className={style.promo_code_wrapper}>
      <Flex direction="column" gap="md" justify="center" align="center">
        <Title order={5}>{trans("haveAPromoCode")}</Title>
        <Flex gap="md" justify="space-between" align="unset">
          <TextInput
            key={coupon?.code}
            name="code"
            onChange={value => {
              inputValueRef.current = value;
            }}
            placeholder={trans("enterCoupon")}
            defaultValue={coupon?.code}
            readOnly={Boolean(coupon)}
            size="sm"
          />
          {codeSuccess ? (
            <RhinoButton onClick={removePromoCode} size="sm">
              {trans("remove")}
            </RhinoButton>
          ) : (
            <RhinoButton onClick={submitCode} size="sm">
              {trans("apply")}
            </RhinoButton>
          )}
        </Flex>
      </Flex>
    </Box>
  );
}
