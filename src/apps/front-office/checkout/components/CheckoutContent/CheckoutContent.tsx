import { Button, Divider, Flex, Grid, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { Form } from "@mongez/react-form";
import { IconArrowBack } from "@tabler/icons-react";
import { cartAtom } from "apps/front-office/cart/atoms";
import CartItemsTable from "apps/front-office/cart/components/CartItemsTable";
import { SubmitButton } from "apps/front-office/design-system/components/Buttons/SubmitButton";
import Container from "apps/front-office/design-system/components/Container";
import { RadioInput } from "apps/front-office/design-system/components/Form/RadioInput";
import { UnStyledLink } from "apps/front-office/design-system/components/Link";
import NoProductsFound from "apps/front-office/store/components/NoProductsFound";
import URLS from "apps/front-office/utils/urls";
import { checkoutAtom } from "../../atom";
import { useCheckout } from "../../hooks/use-checkout";
import BillingAddress from "../BillingAddress";
import CheckoutSummary from "../CheckoutSummary";
import PromoCode from "../PromoCode";
import ShippingAddress from "../ShippingAddress";

export default function CheckoutContent() {
  const { paymentMethods } = checkoutAtom.useValue();
  const items = cartAtom.use("items");
  const { submitCheckout } = useCheckout();

  if (!items) return <NoProductsFound text="noProductsInCheckout" />;

  return (
    <Container>
      <Form onSubmit={submitCheckout} id="checkout-form">
        <Grid justify="space-between" my="3rem">
          <Grid.Col span={{ base: 12, md: 8.5 }}>
            <Flex direction="column" gap="1.5rem">
              <CartItemsTable />
              <ShippingAddress />
              <BillingAddress />
            </Flex>
          </Grid.Col>
          <Grid.Col span={{ base: 12, md: 3.5 }} pos="relative">
            <Flex direction="column" gap="1.5rem">
              <PromoCode />
              <CheckoutSummary />
              <Flex gap="0.5rem" direction="column">
                <Title order={4}>{trans("paymentMethods")}</Title>
                {paymentMethods.map(method => (
                  <RadioInput
                    name="paymentMethod"
                    defaultValue={method.name}
                    label={method.label}
                    fw={600}
                    key={method.name}
                  />
                ))}
              </Flex>
              <Divider />
              <Flex justify="space-between" align="center" mt="sm">
                <UnStyledLink to={URLS.cart}>
                  <Button size="md" color="rhino.9" variant="light" radius={0}>
                    <IconArrowBack />
                    {trans("backToCart")}
                  </Button>
                </UnStyledLink>
                <SubmitButton size="md">{trans("proceed")}</SubmitButton>
              </Flex>
            </Flex>
          </Grid.Col>
        </Grid>
      </Form>
    </Container>
  );
}
