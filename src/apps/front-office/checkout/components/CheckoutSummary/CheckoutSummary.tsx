import { Box, Divider, Flex, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { IconTruck } from "@tabler/icons-react";
import { cartAtom } from "apps/front-office/cart/atoms";
import { checkoutAtom } from "../../atom";

export default function CheckoutSummary() {
  const totalsText = [...(checkoutAtom.use("cart").totalsText || [])];
  const finalPrice = totalsText.pop();

  return (
    <Flex p="1rem" bg="romance.0" direction="column" gap="0.7rem"  w="100%">
      {totalsText.map(total => (
        <Box key={total.type}>
          <Flex justify="space-between" align="center" key={total.label}>
            <Title order={6} tt="uppercase">
              {total.label}
            </Title>
            <Title order={6} fw={500}>
              {total.valueText}
            </Title>
          </Flex>
          <Divider mt="0.7rem" />
        </Box>
      ))}
      <Flex justify="space-between" align="center">
        <Title order={4} tt="uppercase">
          {trans("total")}
        </Title>
        {finalPrice && (
          <Title order={3} c="rhino.9">
            {finalPrice.valueText}
          </Title>
        )}
      </Flex>
    
    </Flex>
  );
}
