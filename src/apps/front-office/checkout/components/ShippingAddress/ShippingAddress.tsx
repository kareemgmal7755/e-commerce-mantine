import { Flex, SimpleGrid, Switch, Text, Title } from "@mantine/core";
import { trans } from "@mongez/localization";
import { HiddenInput } from "@mongez/react-form";
import { addressesAtom } from "apps/front-office/address-book/atom";
import AddAddress from "apps/front-office/address-book/components/AddAddress";
import { useAddresses } from "apps/front-office/address-book/hooks";
import { useState } from "react";
import style from "../style.module.scss";

export default function ShippingAddress() {
  const { markAsPrimary } = useAddresses();
  const addresses = addressesAtom.useValue();
  const [selectedAddress, setAddress] = useState(
    addresses.find(address => address.isPrimary)?.id,
  );

  return (
    <Flex direction="column" gap="0.5rem">
      <Flex justify="space-between" align="center">
        <Title order={4}>{trans("shippingAddress")}</Title>
        <AddAddress />
      </Flex>
      <HiddenInput name="shippingAddress" value={selectedAddress} />
      <SimpleGrid cols={{ base: 1, md: 2 }}>
        {addresses.map(address => (
          <Flex
            p="1rem"
            w="100%"
            direction="column"
            gap="0.5rem"
            key={address.id}
            className={style.address_wrapper}>
            <Flex justify="space-between" align="center">
              <Text component="p" fw={600}>
                {address.name}
              </Text>
              <Switch
                name="useShippingAddress"
                color="rhino.9"
                label={trans(
                  address.isPrimary ? "primaryAddress" : "makePrimary",
                )}
                checked={selectedAddress === address.id}
                onChange={() => {
                  markAsPrimary(address);
                  setAddress(address.id);
                }}
              />
            </Flex>
            <Text mt="xs" c="dimmed" size="sm" component="p">
              {address.address}, {address.district?.name},
              {address.district?.city?.name}
            </Text>
            <Text component="p" fz="0.9rem">
              {trans("phoneNumber")}: {address.phoneNumber}
            </Text>
          </Flex>
        ))}
      </SimpleGrid>
    </Flex>
  );
}
