import endpoint from "shared/endpoint";

export function getCheckOut() {
  return endpoint.get("/checkout");
}

export function confirmCheckout(data: any) {
  return endpoint.post("/checkout", data);
}

export function applyCode(data: any) {
  return endpoint.patch("/checkout/apply-coupon", data);
}

export function removeCode() {
  return endpoint.delete("/checkout/remove-coupon");
}