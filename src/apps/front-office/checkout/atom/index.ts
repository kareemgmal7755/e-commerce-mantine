import { atom } from "@mongez/react-atom";
import { CartType } from "apps/front-office/cart/atoms";

export type CheckoutData = {
  shippingMethod: string;
  coupon?: string;
  id: number;
  cart: CartType;
  paymentMethod?: string;
  shippingAddress?: any;
  paymentUrl?: string;
  billingAddress?: any;
  addresses: {
    address: string;
    isPrimary: boolean;
  }[];
  useShippingAddressForBilling?: boolean;
  paymentMethods: {
    name: string;
    label: string;
  }[];
  shippingMethods: {
    fees: number;
    text: string;
    value: string;
  }[];
};

export const checkoutAtom = atom<CheckoutData>({
  key: "checkout",
  default: {},
});
