import { guardedRoutes } from "../utils/router";
import URLS from "../utils/urls";
import CheckoutPage from "./pages/CheckoutPage";
import OrderCompletedPage from "./pages/OrderCompletedPage/OrderCompletedPage";

guardedRoutes([
  {
    path: URLS.checkout,
    component: CheckoutPage,
  },
  {
    path: URLS.successCheckoutRoute,
    component: OrderCompletedPage,
  },
]);
