import { trans } from "@mongez/localization";
import { FormSubmitOptions } from "@mongez/react-form";
import { navigateTo } from "@mongez/react-router";
import { toastError } from "apps/front-office/design-system/utils/toast";
import parseError from "apps/front-office/utils/parse-error";
import URLS from "apps/front-office/utils/urls";
import { confirmCheckout } from "../services/services";

function submitCheckout({ values }: FormSubmitOptions) {
  let { billingAddress } = values;

  const { paymentMethod, useShippingAddress, shippingAddress } = values;

  if (!paymentMethod) {
    return toastError(trans("paymentMethodRequired"));
  }

  if (!shippingAddress) {
    return toastError(trans("shippingAddressRequired"));
  }

  if (useShippingAddress && !billingAddress) {
    billingAddress = shippingAddress;
  }

  if (!billingAddress) {
    return toastError(trans("billingAddressRequired"));
  }

  confirmCheckout({
    billingAddress,
    shippingAddress,
    paymentMethod,
  })
    .then(response => {
      if (response.data.order) {
        navigateTo(URLS.successCheckout(response.data.order));
      }
    })
    .catch(error => {
      toastError(parseError(error));
    });
}

export function useCheckout() {
  return {
    submitCheckout,
  };
}
