import { Flex, Text } from "@mantine/core";
import { trans } from "@mongez/localization";
import user from "apps/front-office/account/user";
import OrderDetails from "apps/front-office/checkout/components/OrderDetails";
import Container from "apps/front-office/design-system/components/Container";
import OrderTable from "apps/front-office/store/components/OrderTable";
import { checkoutAtom } from "../../atom";

export default function OrderCompletedPage() {
  const items = checkoutAtom.use("cart").items;

  return (
    <>
      <Container>
        <Flex gap="1rem" direction="column" my="4rem" align="center">
          <Flex
            justify="space-between"
            align="center"
            direction="column"
            gap="1.5rem">
            <img
              width={250}
              src="https://cdn-icons-png.flaticon.com/512/2921/2921112.png"
              alt="done"
            />
            <Text fz="2.5rem" fw={700} c="rhino.9">
              {trans("orderCompleted")}
            </Text>
          </Flex>
          <Flex
            direction="column"
            w="100%"
            align="center"
            justify="center"
            m="auto"
            gap="0.5rem">
            <Text fz="1.2rem" fw={600}>
              {trans("welcome")}, {user.get("name")}
            </Text>
            <Text fz="md" c="gray.6">
              {trans("usingWebsite")}
            </Text>
            <Text fz="sm" fw={600}>
              {trans("receivedOrder")}
            </Text>
          </Flex>
          <OrderDetails />
          <OrderTable  />
        </Flex>
      </Container>
    </>
  );
}
