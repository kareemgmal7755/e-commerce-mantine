import { trans } from "@mongez/localization";
import Helmet from "@mongez/react-helmet";
import { preload } from "@mongez/react-utils";
import { addressesAtom } from "apps/front-office/address-book/atom";
import Breadcrumb from "apps/front-office/design-system/components/Breadcrumb";
import { checkoutAtom } from "../../atom";
import CheckoutContent from "../../components/CheckoutContent";
import { getCheckOut } from "../../services/services";

function _CheckoutPage() {
  return (
    <>
      <Helmet title={trans("checkout")} />
      <Breadcrumb title={trans("checkout")} />;
      <CheckoutContent />
    </>
  );
}

const CheckoutPage = preload(_CheckoutPage, getCheckOut, {
  onSuccess(response) {
    checkoutAtom.update(response.data);
    addressesAtom.update(response.data.addresses);
  },
});
export default CheckoutPage;
